rascUIde
========

Work-in-progress (simple!) desktop environment, written using [rascUI](https://gitlab.com/wilsonco-moo/rascui),
which is the UI library I wrote for [rasc](https://gitlab.com/wilsonco-moo/rasc).

I am starting off by writing a simple window manager, using xcb and the rascUI
xcb binding: [rascUIxcb](https://gitlab.com/wilsonco-moo/rascuixcb).

Here's a screenshot of rascUIwm as it is at the moment - right now all you can really do is move
and resize windows.
![Image](rascUIwm/development/screenshots/rascUIwm-0.png)

I'll write a proper README if this project ever goes anywhere.
