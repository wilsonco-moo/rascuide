/*
 * CalcWindow.h
 *
 *  Created on: 5 Oct 2021
 *      Author: wilson
 */

#ifndef RASCUICALC_CALCWINDOW_H_
#define RASCUICALC_CALCWINDOW_H_

#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <rascUI/util/EmplaceArray.h>

#include "Common.h"

namespace rascUI {
    class WindowConfig;
}

namespace rascUIcalc {

    /**
     *
     */
    class CalcWindow : public platform::Window {
    private:
        // Button for numbers.
        class NumberButton : public rascUI::Button {
        private:
            CalcWindow & window;
            unsigned int number;
        public:
            NumberButton(const rascUI::Location & location, CalcWindow & window, unsigned int number);
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };
        
        // Button for other functions.
        class FunctionButton : public rascUI::Button {
        public:
            using FuncType = void (CalcWindow::*)(void);
        private:
            CalcWindow & window;
            FuncType func;
        public:
            FunctionButton(const rascUI::Location & location, CalcWindow & window, const char * text, FuncType func);
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };
    
        // Panels, etc.
        rascUI::BackPanel backPanel;
        rascUI::FrontPanel topPanel;
        rascUI::Label topLabel, operatorLabel;

        // Number and function buttons.
        rascUI::EmplaceArray<NumberButton, 10> numberButtons;
        FunctionButton dotButton, inverseButton, equalsButton, addButton, subtractButton,
            multiplyButton, divideButton, clearButton, powerButton, piButton;

        // Whether we should zero display next time we type.
        bool shouldZeroDisplay;
        
        // Background value, and whether it exists.
        long double backgroundValue;
        bool hasBackgroundValue;

    public:
        CalcWindow(platform::Context * context);
        virtual ~CalcWindow(void);
        
        // So we can press buttons in response to key presses.
        virtual void onUnusedKeyPress(int key, bool special) override;
        virtual void onUnusedKeyRelease(int key, bool special) override;
        
    private:
        // Helpers
        
        // Get location of button x/y. This is used for pretty much all layout, so might as well
        // be a function to avoid duplication.
        static rascUI::Location getLocation(rascUI::Theme * theme, unsigned int buttonX, unsigned int buttonY);
        // Sets display from a number, for results of calculations.
        void setDisplayNumber(long double number);
        // Gets the component relevant to a key press, or NULL if none exists. Used for keyboard typing.
        rascUI::Component * getComponentByKey(int key, bool special);
        
        // Organisational functions.
        void appendNumber(unsigned int value);
        void appendDot(void);
        void inverse(void);
        void clearDisplay(void);
        void addPi(void);
        
        // Evaluation and zeroing.
        void evaluate(void);
        void zeroDisplay(void);
        
        // Operators.
        void doAdd(void);
        void doSubtract(void);
        void doMultiply(void);
        void doDivide(void);
        void doPower(void);
    };
}

#endif
