/*
 * CalcWindow.cpp
 *
 *  Created on: 5 Oct 2021
 *      Author: wilson
 */

#include "CalcWindow.h"

#ifdef _WIN32
    #include <windows.h>
#else
    #define XK_LATIN1
    #define XK_MISCELLANY
    #include <X11/keysymdef.h>
#endif

#include <rascUI/platform/WindowConfig.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <algorithm>
#include <cstdlib>
#include <cstddef>
#include <GL/gl.h>
#include <cmath>

#define LAYOUT_THEME theme

#define BUTTON_COLUMNS 5
#define BUTTON_ROWS 4

#define TOP_PANEL_POS \
    PIN_NORMAL_T(     \
     GEN_BORDER       \
    )

#define OPERATOR_LABEL_WIDTH UI_BHEIGHT

#define TOP_LABEL_POS                        \
    SUB_BORDERMARGIN_R(OPERATOR_LABEL_WIDTH, \
     TOP_PANEL_POS                           \
    )

#define OPERATOR_LABEL_POS  \
    PIN_R(OPERATOR_LABEL_WIDTH, \
     TOP_PANEL_POS              \
    )

#define BUTTON_POS(x, y)                              \
    BORDERTABLE_XY(x, y, BUTTON_COLUMNS, BUTTON_ROWS, \
     SUB_BORDERMARGIN_T(UI_BHEIGHT,                   \
      GEN_BORDER                                      \
    ))

#define BUTTON_WIDTH 32.0f

namespace rascUIcalc {

    // Gets window config to initialise window.
    static rascUI::WindowConfig getCalcWindowConfig(rascUI::Theme * theme, GLfloat uiScale) {
        // Define button width as a multiple of height, but don't go below minimum button width.
        const GLfloat buttonHeight = UI_BHEIGHT,
                       buttonWidth = std::max(buttonHeight * 1.4f, UI_BWIDTH);
        
        // Work out window size from button size and row/column count.
        // Note: add extra row for top labels.
        rascUI::WindowConfig config;
        config.screenWidth = (BUTTON_COLUMNS * (buttonWidth + UI_BORDER) + UI_BORDER) * uiScale;
        config.screenHeight = ((BUTTON_ROWS + 1) * (buttonHeight + UI_BORDER) + UI_BORDER) * uiScale;
        config.title = "rascUIcalc";
        return config;
    }

    CalcWindow::NumberButton::NumberButton(const rascUI::Location & location, CalcWindow & window, unsigned int number) :
        rascUI::Button(location, std::to_string(number)),
        window(window),
        number(number) {
    }
    
    void CalcWindow::NumberButton::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getMb()) {
            window.appendNumber(number);
        }
    }
    
    CalcWindow::FunctionButton::FunctionButton(const rascUI::Location & location, CalcWindow & window, const char * text, FuncType func) :
        rascUI::Button(location, text),
        window(window),
        func(func) {
    }
    
    void CalcWindow::FunctionButton::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getMb()) {
            (window.*func)();
        }
    }

    CalcWindow::CalcWindow(platform::Context * context) :
        platform::Window(context, getCalcWindowConfig(context->defaultTheme, *context->defaultXScalePtr)),
        
        backPanel(),
        topPanel(rascUI::Location(TOP_PANEL_POS)),
        topLabel(rascUI::Location(TOP_LABEL_POS), "0"),
        operatorLabel(rascUI::Location(OPERATOR_LABEL_POS)),
        numberButtons(),
        dotButton(getLocation(theme, 1, 3), *this, ".", &CalcWindow::appendDot),
        inverseButton(getLocation(theme, 4, 1), *this, "+/-", &CalcWindow::inverse),
        equalsButton(getLocation(theme, 2, 3), *this, "=", &CalcWindow::evaluate),
        addButton(getLocation(theme, 3, 3), *this, "+", &CalcWindow::doAdd),
        subtractButton(getLocation(theme, 3, 2), *this, "-", &CalcWindow::doSubtract),
        multiplyButton(getLocation(theme, 3, 1), *this, "*", &CalcWindow::doMultiply),
        divideButton(getLocation(theme, 3, 0), *this, "/", &CalcWindow::doDivide),
        clearButton(getLocation(theme, 4, 0), *this, "AC", &CalcWindow::clearDisplay),
        powerButton(getLocation(theme, 4, 2), *this, "^", &CalcWindow::doPower),
        piButton(getLocation(theme, 4, 3), *this, "pi", &CalcWindow::addPi),
        shouldZeroDisplay(false),
        hasBackgroundValue(false) {
        
        numberButtons.emplace(getLocation(theme, 0, 0), *this, 7);
        numberButtons.emplace(getLocation(theme, 1, 0), *this, 8);
        numberButtons.emplace(getLocation(theme, 2, 0), *this, 9);
        numberButtons.emplace(getLocation(theme, 0, 1), *this, 4);
        numberButtons.emplace(getLocation(theme, 1, 1), *this, 5);
        numberButtons.emplace(getLocation(theme, 2, 1), *this, 6);
        numberButtons.emplace(getLocation(theme, 0, 2), *this, 1);
        numberButtons.emplace(getLocation(theme, 1, 2), *this, 2);
        numberButtons.emplace(getLocation(theme, 2, 2), *this, 3);
        numberButtons.emplace(getLocation(theme, 0, 3), *this, 0);
        
        add(&backPanel);
        add(&topPanel);
        add(&topLabel);
        add(&operatorLabel);
        
        for (unsigned int buttonId = 0; buttonId < 10; buttonId++) {
            add(&numberButtons[buttonId]);
        }
        
        addAfter(&numberButtons[2], &divideButton);
        addAfter(&divideButton, &clearButton);
        
        addAfter(&numberButtons[5], &multiplyButton);
        addAfter(&multiplyButton, &inverseButton);
        
        addAfter(&numberButtons[8], &subtractButton);
        addAfter(&subtractButton, &powerButton);
        
        
        add(&dotButton);
        add(&equalsButton);
        add(&addButton);
        add(&piButton);
    }
    
    CalcWindow::~CalcWindow(void) {
    }
    
    void CalcWindow::onUnusedKeyPress(int key, bool special) {
        rascUI::Component * component = getComponentByKey(key, special);
        if (component != NULL) {
            // Note: Always send a release first, if there is keyboard selected,
            // to ensure any keys we have pressed but not released run their actions.
            if (getKeyboardSelected() != NULL) {
                runNavigationActionRelease(rascUI::NavigationAction::enter);
            }
            
            // If we find relevant component, and successfully set keyboard
            // selected, then send the press event.
            setKeyboardSelected(component);
            if (getKeyboardSelected() == component) {
                runNavigationActionPress(rascUI::NavigationAction::enter);
            }
        }
    }
    
    void CalcWindow::onUnusedKeyRelease(int key, bool special) {
        // If we find relevant component, send press event.
        // (Don't check selected status, that might have changed since press).
        if (getComponentByKey(key, special) != NULL) {
            runNavigationActionRelease(rascUI::NavigationAction::enter);
        }
    }
    
    rascUI::Location CalcWindow::getLocation(rascUI::Theme * theme, unsigned int buttonX, unsigned int buttonY) {
        return rascUI::Location(BUTTON_POS(buttonX, buttonY));
    }
    
    void CalcWindow::setDisplayNumber(long double number) {
        std::string strNumber = std::to_string(number);
        // Erase unnecessary trailing zeroes.
        while(!strNumber.empty() && strNumber.back() == '0') {
            strNumber.pop_back();
        }
        // Erase unnecessary trailing dot.
        if (!strNumber.empty() && strNumber.back() == '.') {
            strNumber.pop_back();
        }        
        topLabel.setText(strNumber);
        topPanel.repaint();
    }
    
    rascUI::Component * CalcWindow::getComponentByKey(int key, bool special) {
        #ifdef _WIN32
            // Key symbols for windows (note: some are special keys).
            if (special) {
                switch(key) {
                    case VK_NEXT: return &equalsButton;
                    case VK_DELETE: return &clearButton;
                    default: return NULL;
                }
            } else {
                switch(key) {
                    case '0': return &numberButtons[9];
                    case '1': return &numberButtons[6];
                    case '2': return &numberButtons[7];
                    case '3': return &numberButtons[8];
                    case '4': return &numberButtons[3];
                    case '5': return &numberButtons[4];
                    case '6': return &numberButtons[5];
                    case '7': return &numberButtons[0];
                    case '8': return &numberButtons[1];
                    case '9': return &numberButtons[2];
                    case '.': return &dotButton;
                    case '=': return &equalsButton;
                    case '+': return &addButton;
                    case '-': return &subtractButton;
                    case '*': return &multiplyButton;
                    case '/': return &divideButton;
                    case '_': return &inverseButton;
                    case '^': return &powerButton;
                    case 'p': return &piButton;
                    default: return NULL;
                }
            }
        #else
            // Key symbols for linux.
            switch(key) {
                case XK_0: case XK_KP_0: return &numberButtons[9];
                case XK_1: case XK_KP_1: return &numberButtons[6];
                case XK_2: case XK_KP_2: return &numberButtons[7];
                case XK_3: case XK_KP_3: return &numberButtons[8];
                case XK_4: case XK_KP_4: return &numberButtons[3];
                case XK_5: case XK_KP_5: return &numberButtons[4];
                case XK_6: case XK_KP_6: return &numberButtons[5];
                case XK_7: case XK_KP_7: return &numberButtons[0];
                case XK_8: case XK_KP_8: return &numberButtons[1];
                case XK_9: case XK_KP_9: return &numberButtons[2];
                case XK_period: case XK_KP_Decimal: return &dotButton;
                case XK_equal: case XK_Page_Down: return &equalsButton;
                case XK_plus: case XK_KP_Add: return &addButton;
                case XK_minus: case XK_KP_Subtract: return &subtractButton;
                case XK_asterisk: case XK_KP_Multiply: return &multiplyButton;
                case XK_slash: case XK_KP_Divide: return &divideButton;
                case XK_Delete: case XK_KP_Delete: return &clearButton;
                case XK_underscore: return &inverseButton;
                case XK_asciicircum: return &powerButton;
                case XK_p: return &piButton;
                default: return NULL;
            }
        #endif
    }
    
    void CalcWindow::appendNumber(unsigned int value) {
        // If we're supposed to zero display, do it, since we're typing.
        if (shouldZeroDisplay) {
            zeroDisplay();
        }
        
        std::string newText = topLabel.getText();
        
        // If existing text is a zero (possibly preceeded by negative), remove it before addding number.
        // Do nothing if adding zero to a zero.
        if (newText == "0" || newText == "-0") {
            if (value == 0) return;
            newText.pop_back();
        }
        
        // Add new character to the end.
        newText += std::to_string(value);
        topLabel.setText(newText);
        topPanel.repaint();
    }
    
    void CalcWindow::appendDot(void) {
        // If we're supposed to zero display, do it, since we're typing.
        if (shouldZeroDisplay) {
            zeroDisplay();
        }
        
        const std::string & labelText = topLabel.getText();
        
        // Add dot to label text, set dot button to inactive so it can't be run again.
        topLabel.setText(labelText + '.');
        topPanel.repaint();
        dotButton.setActive(false);
        dotButton.repaint();
    }
    
    void CalcWindow::inverse(void) {
        std::string newText = topLabel.getText();
        
        // Add or remove a minus, depending on whether there already is one.
        if (newText[0] == '-') {
            newText.erase(0, 1);
        } else {
            newText.insert(0, 1, '-');
        }
        topLabel.setText(newText);
        topPanel.repaint();
    }
    
    void CalcWindow::clearDisplay(void) {
        // Clear foreground by zeroing display, set flag to discard background value,
        // discard operator, set buttons active again.
        zeroDisplay();
        hasBackgroundValue = false;
        operatorLabel.setText(std::string());
        dotButton.setActive(true);
    }
    
    void CalcWindow::addPi(void) {
        // Set display number to pi, de-activate dot button since pi has a dot in it.
        setDisplayNumber(M_PI);
        dotButton.setActive(false);
        // This isn't a zeroable value, so make sure display isn't zeroed next time
        // user types, make sure equals button is active.
        shouldZeroDisplay = false;
        equalsButton.setActive(true);
    }
    
    void CalcWindow::evaluate(void) {
        if (!shouldZeroDisplay) {
            // Get foreground value from current display.
            long double foregroundValue = std::strtold(topLabel.getText().c_str(), NULL);
            
            // If there is an operator, and we have a background value, do some maths.
            if (!operatorLabel.getText().empty() && hasBackgroundValue) {
                switch(operatorLabel.getText()[0]) {
                case '+':
                    foregroundValue = backgroundValue + foregroundValue;
                    break;
                case '-':
                    foregroundValue = backgroundValue - foregroundValue;
                    break;
                case '*':
                    foregroundValue = backgroundValue * foregroundValue;
                    break;
                case '/':
                    foregroundValue = backgroundValue / foregroundValue;
                    break;
                case '^':
                    foregroundValue = std::pow(backgroundValue, foregroundValue);
                    break;
                }
            }
            
            // Set display from foreground value, save to background value for use next time.
            setDisplayNumber(foregroundValue);
            backgroundValue = foregroundValue;
            hasBackgroundValue = true;
            
            // Clear operator label, make it so screen is zeroed next time we type, re-enable dot button, disable equals button since we can't equals twice.
            operatorLabel.setText(std::string());
            shouldZeroDisplay = true;
            dotButton.setActive(true);
            equalsButton.setActive(false);
        }
    }
    
    void CalcWindow::zeroDisplay(void) {
        // Reset text to zero, repaint top panel.
        topLabel.setText("0");
        topPanel.repaint();
        // Make sure equals button is active again, and ensure this function isn't called next time.
        equalsButton.setActive(true);
        shouldZeroDisplay = false;
    }
    
    void CalcWindow::doAdd(void) {
        evaluate();
        operatorLabel.setText("+");
        topPanel.repaint();
    }
    
    void CalcWindow::doSubtract(void) {
        evaluate();
        operatorLabel.setText("-");
        topPanel.repaint();
    }
    
    void CalcWindow::doMultiply(void) {
        evaluate();
        operatorLabel.setText("*");
        topPanel.repaint();
    }
    
    void CalcWindow::doDivide(void) {
        evaluate();
        operatorLabel.setText("/");
        topPanel.repaint();
    }
    
    void CalcWindow::doPower(void) {
        evaluate();
        operatorLabel.setText("^");
        topPanel.repaint();
    }
}
