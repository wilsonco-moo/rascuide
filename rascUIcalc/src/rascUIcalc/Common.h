/*
 * Common.h
 *
 *  Created on: 5 Oct 2021
 *      Author: wilson
 */

#ifndef RASCUICALC_COMMON_H_
#define RASCUICALC_COMMON_H_

#ifdef _WIN32
    #include <rascUIwin/platform/Context.h>
    #include <rascUIwin/platform/Window.h>
    namespace platform = rascUIwin;
#else
    #include <rascUIxcb/platform/Context.h>
    #include <rascUIxcb/platform/Window.h>
    namespace platform = rascUIxcb;
#endif

#endif
