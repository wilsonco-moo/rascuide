/*
 * main.cpp
 *
 *  Created on: 5 Oct 2021
 *      Author: wilson
 */

#include <rascUIcalc/CalcWindow.h>
#include <rascUIcalc/Common.h>
#include <iostream>
#include <cstdlib>

#ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
    #include <rascUItheme/themes/win/scalableTheme/ScalableTheme.h>
#endif

int main(void) {
    
    #ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
        // TODO: this should be handled by rascUIwin, so platform-specific code isn't here.
        winScalableTheme::ScalableTheme scalableTheme;
        rascUI::Theme * theme = &scalableTheme;
        GLfloat scale = 1.0f;
        platform::Context context(theme, &scale, &scale);
    #else
        platform::Context context;
        if (!context.wasSuccessful()) {
            std::cerr << "Failed to load platform context, exiting.\n";
            return EXIT_FAILURE;
        }
    #endif
    
    rascUIcalc::CalcWindow window(&context);
    window.setMapped(true);
    context.mainLoop();
    
    return EXIT_SUCCESS;
}
