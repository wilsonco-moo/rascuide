/*
 * CustomContext.cpp
 *
 *  Created on: 14 Nov 2021
 *      Author: wilson
 */

#include "CustomContext.h"

#define XK_LATIN1
#define XK_MISCELLANY
// Before including Layout.h, define normal width/height as the ones for window
// decorations.
#define UI_BWIDTH (LAYOUT_THEME)->windowDecorationNormalComponentWidth
#define UI_BHEIGHT (LAYOUT_THEME)->windowDecorationNormalComponentHeight

#include <rascUItheme/utils/config/ConfigFile.h>
#include <rascUItheme/utils/config/PathUtils.h>
#include <rascUI/platform/WindowConfig.h>
#include <rascUIxcb/util/Misc.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <X11/keysymdef.h>
#include <xcb/xcb_util.h>
#include <xcb/dpms.h>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include <limits>

#include "utils/ProcessManager.h"
#include "windows/SwitchWindow.h"
#include "windows/RunWindow.h"

namespace rascUIwm {

    const char* const CustomContext::CONFIG_FILENAME = "rascUIwm.cfg";

    CustomContext::CustomContext(ProcessManager & processManager) :
        rascUIxcb::Context(false, NULL), // Note: Don't make Context initialise in constructor, we implement setRootCursor!
        processManager(processManager),
        workspaceSwitchState(*this),
        runWindow(NULL),
        switchWindow(NULL),
        
        altTab(
            xcbGetConnection(),         // Connection
            xcbGetKeySymbols(),         // Key symbol table
            0,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_MOD_MASK_1,             // Keyboard modifier mask
            { XK_Tab },                 // Key symbols
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC),       // Keyboard mode
            
        altShiftTab(
            xcbGetConnection(),         // Connection
            xcbGetKeySymbols(),         // Key symbol table
            0,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_MOD_MASK_1 | XCB_MOD_MASK_SHIFT, // Keyboard modifier mask
            { XK_Tab },                 // Key symbols
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC),       // Keyboard mode
    
        ctrlAltAll(
            xcbGetConnection(),         // Connection
            xcbGetKeySymbols(),         // Key symbol table
            0,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_MOD_MASK_CONTROL | XCB_MOD_MASK_1, // Keyboard modifier mask
            { XK_Delete, XK_Left, XK_Right, // Key symbols
              XK_0, XK_1, XK_2, XK_3, XK_4, XK_5, XK_6, XK_7, XK_8, XK_8, XK_9,
              XK_KP_0, XK_KP_1, XK_KP_2, XK_KP_3, XK_KP_4, XK_KP_5, XK_KP_6, XK_KP_7, XK_KP_8, XK_KP_8, XK_KP_9 },
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC),       // Keyboard mode

        superR(
            xcbGetConnection(),         // Connection
            xcbGetKeySymbols(),         // Key symbol table
            0,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_MOD_MASK_4,             // Keyboard modifier mask
            { XK_r },                   // Key symbols
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC),       // Keyboard mode

        alt(
            xcbGetConnection(),         // Connection
            xcbGetKeySymbols(),         // Key symbol table
            1,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            0,                          // Keyboard modifier mask
            { XK_Alt_L },               // Key symbols
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC),       // Keyboard mode
        
        ctrlAlt(
            xcbGetConnection(),         // Connection
            xcbGetKeySymbols(),         // Key symbol table
            1,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_MOD_MASK_CONTROL,       // Keyboard modifier mask
            { XK_Alt_L },               // Key symbols
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC),       // Keyboard mode

        altLeft(
            xcbGetConnection(),         // Connection
            0,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_BUTTON_1_MOTION, // Event mask
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC,        // Keyboard mode
            XCB_CURSOR_NONE,            // Cursor type
            XCB_BUTTON_INDEX_1,         // Mouse buttons
            XCB_MOD_MASK_1),            // Keyboard modifier mask
            
        altRight(
            xcbGetConnection(),         // Connection
            0,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_BUTTON_3_MOTION, // Event mask
            XCB_GRAB_MODE_ASYNC,        // Pointer mode
            XCB_GRAB_MODE_ASYNC,        // Keyboard mode
            XCB_CURSOR_NONE,            // Cursor type
            XCB_BUTTON_INDEX_3,         // Mouse buttons
            XCB_MOD_MASK_1),            // Keyboard modifier mask
        
        // Note:
        // To pass button grab event through to other windows, it must be a synchronous
        // grab. A synchronous grab requires us to manually allow the event to be
        // propogated through, as otherwise the X event queue becomes frozen.
        // Source:
        //   https://github.com/c00kiemon5ter/monsterwm/issues/12
        //   https://unix.stackexchange.com/a/397466
        // Archive:
        //   https://web.archive.org/web/20210506181134/https://github.com/c00kiemon5ter/monsterwm/issues/12
        //   https://web.archive.org/web/20210929073632/https://unix.stackexchange.com/questions/397339/xcb-asynchronous-pointer-grab-not-propagating-events
        anyOtherClick(
            xcbGetConnection(),         // Connection
            1,                          // Owner events (0 = eat event, 1 = allow passing on event afterwards)
            XCB_EVENT_MASK_BUTTON_PRESS, // Event mask
            XCB_GRAB_MODE_SYNC,         // Pointer mode
            XCB_GRAB_MODE_ASYNC,        // Keyboard mode
            XCB_CURSOR_NONE,            // Cursor type
            XCB_BUTTON_INDEX_ANY,       // Mouse buttons
            0),                         // Keyboard modifier mask
        
        moveResizeState(*this),
        currentWorkspace(0),
        workspaceCount(0),
        registeredRedirect(false),
        ignoreOverrideRedirect(false),
        focusFollowsMouse(false),
        autoPlaceNewWindows(false),
        clipWindowSize(true),
        windowSnapDistance(10) {
        
        // Run the parent Context's initialise method, since we passed false
        // into its constructor to stop it doing this automatically. Needed
        // since we override setRootCursor.
        xcbInitialise();
        
        // If parent rascUIxcb context initialised sucessfully, do our extra
        // initialisation steps.
        if (rascUIxcb::Context::wasSuccessful()) {
            xcb_screen_t * const screen = xcbGetScreen();
            
            // Try to enable substructure notify and redirect (to us) for the root window.
            const uint32_t mask = XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT;
            const xcb_void_cookie_t substructureCookie = xcb_change_window_attributes_checked(xcbGetConnection(), screen->root, XCB_CW_EVENT_MASK, &mask);
            
            // Send out queries for other things: DPMS extension and current screen saver.
            const xcb_query_extension_cookie_t dpmsCookie = xcb_query_extension(xcbGetConnection(), 4, "DPMS"); // Name len, name.
            const xcb_get_screen_saver_cookie_t screenSaverCookie = xcb_get_screen_saver(xcbGetConnection());
            
            // Before we block waiting for a config file to load, flush the requests
            // above. We want to wait for the x server to respond while we're loading
            // the config file, not flush afterwards!
            xcb_flush(xcbGetConnection());
            
            // In the meantime (while waiting for replies on the above), load
            // from the config file. If config fails to load, set default config
            // and save.
            rascUItheme::ConfigFile config((rascUItheme::PathUtils::getRascUIConfigDirectory() + CONFIG_FILENAME).c_str());
            if (!config.loadedSuccessfully()) {
                config.setValue("ignoreOverrideRedirect", "0");
                config.setValue("focusFollowsMouse", "0");
                config.setValue("workspaceCount", "256");
                config.setValue("forceDisableDPMS", "1");
                config.setValue("forceDisableScreenSaver", "1");
                config.setValue("showRunWindowByDefault", "1");
                config.setValue("autoPlaceNewWindows", "0");
                config.setValue("clipWindowSize", "1");
                config.setValue("windowSnapDistance", "10");
                config.save();
            }
            
            // Read members from the config file (not all of these need storing
            // as members).
            ignoreOverrideRedirect = (std::strcmp(config.getValue("ignoreOverrideRedirect"), "1") == 0);
            focusFollowsMouse = (std::strcmp(config.getValue("focusFollowsMouse"), "1") == 0);
            workspaceCount = std::strtoull(config.getValue("workspaceCount"), NULL, 0);
            const bool forceDisableDPMS = (std::strcmp(config.getValue("forceDisableDPMS"), "1") == 0);
            const bool forceDisableScreenSaver = (std::strcmp(config.getValue("forceDisableScreenSaver"), "1") == 0);
            const bool showRunWindowByDefault = (std::strcmp(config.getValue("showRunWindowByDefault"), "1") == 0);
            autoPlaceNewWindows = (std::strcmp(config.getValue("autoPlaceNewWindows"), "1") == 0);
            clipWindowSize = (std::strcmp(config.getValue("clipWindowSize"), "1") == 0);
            windowSnapDistance = std::min<unsigned long>(std::strtoul(config.getValue("windowSnapDistance"), NULL, 0), std::numeric_limits<unsigned int>::max());
            
            // Wait for a reply about enabling substructure notify/redirect, so
            // we can decide whether we're able to continue. Just give up if
            // another WM is running, (and discard replies we won't ever read).
            xcb_generic_error_t * const substructureError = xcb_request_check(xcbGetConnection(), substructureCookie);
            if (substructureError) {
                std::cerr << "WARNING: rascUIwm::CustomContext: " << "Failed to register substructure notify/redirect on root window: It is likely that another window manager is running.\n";
                free(substructureError);
                xcb_discard_reply(xcbGetConnection(), dpmsCookie.sequence);
                xcb_discard_reply(xcbGetConnection(), screenSaverCookie.sequence);
            } else {
                // If we're the only one running (successful):
                registeredRedirect = true;
                
                // Process manager can use our connection's pipe.
                processManager.setPipe(xcbGetXcbConnection().getWritePipe());
                
                // Initialise move resize state now we've loaded config etc.
                moveResizeState.initialise();
                
                // Always append this colour customisation XResources line. That means
                // appending it to the resource manager property.
                // This gets x11 applications to show colours (xterm isn't white,
                // xcalc shows colours and so on).
                // It is really hard to find documentation about these things,
                // See: https://www.x.org/releases/X11R6.7.0/doc/X.7.html#sect13
                // http://web.archive.org/web/20240801075719/https://www.x.org/releases/X11R6.7.0/doc/X.7.html#sect13
                xcb_change_property(
                    xcbGetConnection(), // Connection.
                    XCB_PROP_MODE_APPEND, // Append mode: Append our string to any existing X resources which may have been set before we started.
                    screen->root, // Window: Root window.
                    XCB_ATOM_RESOURCE_MANAGER, // Property to set: X resource manager properties.
                    XCB_ATOM_STRING, // Data type: String.
                    8, // Data format: 8-bit bytes.
                    23, // Data length.
                    "*customization: -color\n"); // Data.
                
                // If not using "focus follows mouse", set focus initially to none, (else
                // focus will start following mouse until user clicks on something).
                if (!focusFollowsMouse) {
                    xcb_set_input_focus(xcbGetConnection(), XCB_INPUT_FOCUS_POINTER_ROOT, XCB_NONE, XCB_CURRENT_TIME);
                }
                
                // Stop screen turning off when idle (annoying), part 1:
                // Get DPMS extension query reply: If config wants us to force disable it, disable it if the dpms
                // extension is present (using libxcb-dpms0 - xcb equivalent of libxext's dpms stuff used by xset).
                // Somebody auto-generated the xcb dpms implementation from xml, see xcb_dpms_disable in: https://github.com/StarchLinux/libxcb/blob/master/dpms.c
                // Archive: http://web.archive.org/web/20240825110554/https://raw.githubusercontent.com/StarchLinux/libxcb/master/dpms.c
                // Also see: https://gitlab.freedesktop.org/xorg/lib/libxext/-/blob/master/src/DPMS.c?ref_type=heads
                // Archive: http://web.archive.org/web/20240825121928/https://gitlab.freedesktop.org/xorg/lib/libxext/-/raw/master/src/DPMS.c?ref_type=heads
                xcb_query_extension_reply_t * const dpmsReply = xcb_query_extension_reply(xcbGetConnection(), dpmsCookie, NULL);
                if (dpmsReply != NULL) {
                    if (forceDisableDPMS) {
                        if (dpmsReply->present) {
                            std::cout << "Force disable DPMS: Extension is " << "present, disabling it.\n";
                            xcb_dpms_disable(xcbGetConnection());
                        } else {
                            std::cout << "Force disable DPMS: Extension is " << "not present on the X server.\n";
                        }
                    }
                    free(dpmsReply);
                }
                
                // Stop screen turning off when idle (annoying), part 2:
                // Get screen saver reply: If force disable flag is set, set
                // screen saver keeping all params the same but set "timeout"
                // to zero. This stops screen saver from ever showing.
                // See xset source: https://gitlab.freedesktop.org/xorg/app/xset/-/blob/master/xset.c?ref_type=heads
                // Archive: http://web.archive.org/web/20240825094658/https://gitlab.freedesktop.org/xorg/app/xset/-/raw/master/xset.c?ref_type=heads
                xcb_get_screen_saver_reply_t * const screenSaverReply = xcb_get_screen_saver_reply(xcbGetConnection(), screenSaverCookie, NULL);
                if (screenSaverReply != NULL) {
                    if (forceDisableScreenSaver) {
                        std::cout << "Force disable screen saver: Changing screen saver timeout from " << screenSaverReply->timeout << " seconds to 0 seconds (disabled).\n";
                        xcb_set_screen_saver(xcbGetConnection(), 0, screenSaverReply->interval, screenSaverReply->prefer_blanking, screenSaverReply->allow_exposures);
                    }
                    free(screenSaverReply);
                }
                
                // Set up the buckets.
                const Bucket * previousBucket = NULL;
                for (unsigned int index = 0; index < (unsigned int)Buckets::COUNT; index++) {
                    buckets.emplace(*this, previousBucket);
                    previousBucket = &buckets[index];
                }
                
                // Now we're done setting everything else up, show any popups
                // required by default.
                if (showRunWindowByDefault) {
                    showRunWindow();
                }
            }
        }
    }
    
    CustomContext::~CustomContext(void) {
        // Delete any child windows we created.
        delete runWindow;
        delete switchWindow;
        
        // Clear any remaining managed windows before destroying:
        // In *their* destructor, they'll remove themself from windowsByParent
        // - don't want them doing that after we've been destroyed.
        windows.clear();
        
        // Clear process manager's pipe (connection will be destroyed).
        processManager.clearPipe();
    }
    
    bool CustomContext::handleEventInternal(xcb_generic_event_t * event, bool * repaintNeeded) {
        switch(XCB_EVENT_RESPONSE_TYPE(event)) {
            case XCB_KEY_PRESS: {
                xcb_key_press_event_t * pressEvent = (xcb_key_press_event_t *)event;
                xcb_keysym_t keySym;
                if (altTab.matchesEvent(keySym, pressEvent)) {
                    showSwitchWindow();
                    switchWindow->switchForward();
                    std::cout << "Alt+tab key press\n";
                    return false;
                } else if (altShiftTab.matchesEvent(keySym, pressEvent)) {
                    showSwitchWindow();
                    switchWindow->switchBackward();
                    std::cout << "Alt+shift+tab key press\n";
                    return false;
                } else if (ctrlAltAll.matchesEvent(keySym, pressEvent)) {
                    switch(keySym) {
                    case XK_Delete:
                        std::cout << "Ctrl+alt+delete key press\n";
                        return false;
                    case XK_Left:
                        std::cout << "Ctrl+alt+left key press\n";
                        switchWorkspaceLeft();
                        return false;
                    case XK_Right:
                        std::cout << "Ctrl+alt+right key press\n";
                        switchWorkspaceRight();
                        return false;
                    case XK_0: case XK_KP_0: std::cout << "Ctrl+alt+0 key press\n"; return false;
                    case XK_1: case XK_KP_1: std::cout << "Ctrl+alt+1 key press\n"; return false;
                    case XK_2: case XK_KP_2: std::cout << "Ctrl+alt+2 key press\n"; return false;
                    case XK_3: case XK_KP_3: std::cout << "Ctrl+alt+3 key press\n"; return false;
                    case XK_4: case XK_KP_4: std::cout << "Ctrl+alt+4 key press\n"; return false;
                    case XK_5: case XK_KP_5: std::cout << "Ctrl+alt+5 key press\n"; return false;
                    case XK_6: case XK_KP_6: std::cout << "Ctrl+alt+6 key press\n"; return false;
                    case XK_7: case XK_KP_7: std::cout << "Ctrl+alt+7 key press\n"; return false;
                    case XK_8: case XK_KP_8: std::cout << "Ctrl+alt+8 key press\n"; return false;
                    case XK_9: case XK_KP_9: std::cout << "Ctrl+alt+9 key press\n"; return false;
                    }
                } else if (superR.matchesEvent(keySym, pressEvent)) {
                    std::cout << "Super+r key press\n";
                    showRunWindow();
                    return false;
                } else if (alt.matchesEvent(keySym, pressEvent)) {
                    std::cout << "Alt key press\n";
                    return false;
                } else if (ctrlAlt.matchesEvent(keySym, pressEvent)) {
                    std::cout << "Ctrl+alt key press\n";
                    return false;
                } else {
                    std::cout << "Other key press\n";
                }
                break;
            }
            
            case XCB_KEY_RELEASE: {
                xcb_key_release_event_t * releaseEvent = (xcb_key_release_event_t *)event;
                xcb_keysym_t keySym;
                if (altTab.matchesEvent(keySym, releaseEvent)) {
                    std::cout << "Alt+tab key release\n";
                    return false;
                } else if (altShiftTab.matchesEvent(keySym, releaseEvent)) {
                    std::cout << "Alt+shift+tab key release\n";
                    return false;
                } else if (ctrlAltAll.matchesEvent(keySym, releaseEvent)) {
                    switch(keySym) {
                    case XK_Delete:
                        std::cout << "Ctrl+alt+delete key release\n";
                        endMainLoop();
                        return false;
                    case XK_Left:
                        std::cout << "Ctrl+alt+left key release\n";
                        return false;
                    case XK_Right:
                        std::cout << "Ctrl+alt+right key release\n";
                        return false;
                    case XK_0: case XK_KP_0: std::cout << "Ctrl+alt+0 key release\n"; workspaceSwitchState.onCharacter('0'); return false;
                    case XK_1: case XK_KP_1: std::cout << "Ctrl+alt+1 key release\n"; workspaceSwitchState.onCharacter('1'); return false;
                    case XK_2: case XK_KP_2: std::cout << "Ctrl+alt+2 key release\n"; workspaceSwitchState.onCharacter('2'); return false;
                    case XK_3: case XK_KP_3: std::cout << "Ctrl+alt+3 key release\n"; workspaceSwitchState.onCharacter('3'); return false;
                    case XK_4: case XK_KP_4: std::cout << "Ctrl+alt+4 key release\n"; workspaceSwitchState.onCharacter('4'); return false;
                    case XK_5: case XK_KP_5: std::cout << "Ctrl+alt+5 key release\n"; workspaceSwitchState.onCharacter('5'); return false;
                    case XK_6: case XK_KP_6: std::cout << "Ctrl+alt+6 key release\n"; workspaceSwitchState.onCharacter('6'); return false;
                    case XK_7: case XK_KP_7: std::cout << "Ctrl+alt+7 key release\n"; workspaceSwitchState.onCharacter('7'); return false;
                    case XK_8: case XK_KP_8: std::cout << "Ctrl+alt+8 key release\n"; workspaceSwitchState.onCharacter('8'); return false;
                    case XK_9: case XK_KP_9: std::cout << "Ctrl+alt+9 key release\n"; workspaceSwitchState.onCharacter('9'); return false;
                    }
                } else if (superR.matchesEvent(keySym, releaseEvent)) {
                    std::cout << "Super+r key release\n";
                    return false;
                } else if (alt.matchesEvent(keySym, releaseEvent, XCB_MOD_MASK_1)) {
                    std::cout << "Alt key release\n";
                    if (switchWindow != NULL) {
                        switchWindow->onAltRelease();
                    }
                    workspaceSwitchState.onFinish();
                    return false;
                } else if (ctrlAlt.matchesEvent(keySym, releaseEvent, XCB_MOD_MASK_1)) {
                    std::cout << "Ctrl+alt key release\n";
                    workspaceSwitchState.onFinish();
                    return false;
                } else {
                    std::cout << "Other key release\n";
                }
                break;
            }
            
            case XCB_BUTTON_PRESS: {
                xcb_button_press_event_t * pressEvent = (xcb_button_press_event_t *)event;
                
                if (altLeft.matchesEvent(pressEvent)) {
                    std::cout << "Alt+left button press\n";
                    moveResizeState.start(pressEvent, true);
                    return false;
                } else if (altRight.matchesEvent(pressEvent)) {
                    std::cout << "Alt+right button press\n";
                    moveResizeState.start(pressEvent, false);
                    return false;
                } else if (anyOtherClick.matchesEvent(pressEvent)) {
                    if (pressEvent->child != XCB_NONE) {
                        // Note: Since we're subscribed to events on the root window, the event's "event"
                        // window is always root, and the event's "child" is always the top-level one,
                        // i.e: for reparented windows, the frame not the child!
                        
                        // For input focus, for reparented windows we always want the CHILD, (else
                        // xcb_set_input_focus does nothing). So search the parent windows, and use the
                        // attached child window if it exists. Otherwise, it is probably an
                        // override_redirect window we've not reparented, so focus the original one
                        // specified by the event instead.
                        xcb_window_t windowToFocus;
                        const std::unordered_map<xcb_window_t, CustomWindow *>::const_iterator iter = windowsByParent.find(pressEvent->child);
                        if (iter == windowsByParent.end()) {
                            windowToFocus = pressEvent->child;
                            // A window we've not reparented: Send to the top of "normal" bucket.
                            buckets[(size_t)Buckets::normal].sendToTop(pressEvent->child);
                        } else {
                            windowToFocus = iter->second->getChildWindow();
                            // If this is a window we've reparented, send it to the top of its current bucket.
                            iter->second->sendToTop();
                        }
                        
                        // Focus it.
                        focusWindow(windowToFocus);
                    }
                    // Allow events since this is a synchronous grab, see comment above.
                    xcb_allow_events(xcbGetConnection(), XCB_ALLOW_REPLAY_POINTER, XCB_CURRENT_TIME);
                } else {
                    std::cout << "Other button press\n";
                }
                // Note: Pass on presses except those generated by our key
                // grab (which are for the root window, rascUIxcb doesn't care).
                return pressEvent->event != xcbGetScreen()->root;
            }
            
            case XCB_BUTTON_RELEASE: {
                xcb_button_release_event_t * releaseEvent = (xcb_button_release_event_t *)event;
                if (altLeft.matchesEventNoModifiers(releaseEvent)) {
                    std::cout << "Alt+left button release\n";
                    moveResizeState.end(releaseEvent);
                    // Note: Pass on releases except those generated by our key
                    // grab (which are for the root window, rascUIxcb doesn't care).
                    return releaseEvent->event != xcbGetScreen()->root;
                } else if (altRight.matchesEventNoModifiers(releaseEvent)) {
                    std::cout << "Alt+right button release\n";
                    moveResizeState.end(releaseEvent);
                    return releaseEvent->event != xcbGetScreen()->root;
                } else {
                    std::cout << "Other button release\n";
                }
                break;
            }
                
            case XCB_MOTION_NOTIFY: {
                xcb_motion_notify_event_t * motionEvent = (xcb_motion_notify_event_t *)event;
                if (RASCUIXCB_NEXT_EVENT_IS_NOT(xcbGetXcbConnection(), XCB_MOTION_NOTIFY)) {
                    moveResizeState.motionNotify(motionEvent);
                }
                // Note: Don't pass on motion events for the root window to
                // rascUIxcb - they're generated by our key grab, and rascUIxcb
                // only cares about windows it is managing.
                return motionEvent->event != xcbGetScreen()->root;
            }
            
            // Events we pick up from substructure redirect on root window:
            // Must create/destroy custom windows for these.
            case XCB_CREATE_NOTIFY: {
                onCreateNotify(*(xcb_create_notify_event_t *)event);
                return false;
            }
            case XCB_DESTROY_NOTIFY: {
                onDestroyNotify(*(xcb_destroy_notify_event_t *)event);
                return false;
            }
            
            // Pass these events on to the relevant window, if we can't find
            // the window, pass on to rascUIxcb, (might be an event for the
            // parent window, which is managed by rascUIxcb).
            case XCB_CONFIGURE_REQUEST: {
                return onWindowEvent(((xcb_configure_request_event_t *)event)->window, event, repaintNeeded);
            }
            case XCB_MAP_REQUEST: {
                return onWindowEvent(((xcb_map_request_event_t *)event)->window, event, repaintNeeded);
            }
            case XCB_UNMAP_NOTIFY: {
                return onWindowEvent(((xcb_unmap_notify_event_t *)event)->window, event, repaintNeeded);
            }
            case XCB_PROPERTY_NOTIFY: {
                return onWindowEvent(((xcb_property_notify_event_t *)event)->window, event, repaintNeeded);
            }
            
            // Don't care about these events: Ignore them, and pass them on to
            // rascUIxcb's event loop.
            case XCB_CONFIGURE_NOTIFY:
            case XCB_CLIENT_MESSAGE:
            case XCB_MAP_NOTIFY:
            case XCB_REPARENT_NOTIFY:
            case XCB_EXPOSE:
            case XCB_ENTER_NOTIFY:
            case XCB_LEAVE_NOTIFY: {
                break;
            }
            
            // Xcb doesn't have a name for the id of error messages for whatever
            // reason, it is just zero.
            case 0: {
                // https://www.x.org/releases/current/doc/man/man3/xcb-requests.3.xhtml
                // https://x.org/releases/X11R7.7/doc/xproto/x11protocol.html#Encoding::Errors
                // http://web.archive.org/web/20101224073645/http://xcb.freedesktop.org/XcbUtil/api/group__xcb____event__t.html#g29a8acab86cd6f92c66efb4f2ab4eb0c
                // Some rough error handling: We can at least print its label
                // with xcb-util.
                const char * const errorName = xcb_event_get_error_label(event->pad0);
                std::cout << "Error: \""<< (errorName == NULL ? "null" : errorName) << "\" (" << (unsigned int)event->pad0 << ").\n";
                break;
            }
            
            default: {
                const char * const eventName = xcb_event_get_label(XCB_EVENT_RESPONSE_TYPE(event));
                std::cout << "WARNING: rascUIwm::CustomContext: " << "Unknown event \"" << (eventName == NULL ? "null" : eventName) << "\" (" << (unsigned int)XCB_EVENT_RESPONSE_TYPE(event) << ")\n";
                break;
            }
        }
        return true;
    }
    
    void CustomContext::onCreateNotify(const xcb_create_notify_event_t & event) {
        // First look and see if this window already exists. Ignore anyone who
        // tries to create the same window twice.
        const std::unordered_map<xcb_window_t, CustomWindow>::iterator iter = windows.find(event.window);
        if (iter != windows.end()) {
            std::cout << "WARNING: rascUIwm::CustomContext: " << "Somebody tried to create window id \"" << (unsigned int)event.window << "\" twice, ignoring that.\n";
            return;
        }
        
        // Ignore the (single) create notification from excluded windows, (i.e:
        // those we have created as parent/frame windows - don't want to create
        // frames for frames!). After receiving the (single) create
        // notification, remove them from the list. NOTE: Must be done like this
        // ("consume" from the list after create notify), instead of
        // CustomWindow removing itself when destroyed, as client could send us
        // a create, then immediately a destroy. Then, we'd receive the create
        // notify for the parent window AFTERWARDS, and it wouldn't be in the
        // list!
        const std::unordered_set<xcb_window_t>::iterator excludedIter = excludedWindows.find(event.window);
        if (excludedIter != excludedWindows.end()) {
            excludedWindows.erase(excludedIter);
            return;
        }
        
        // If ignoreOverrideRedirect flag is NOT set, for windows with the
        // override_redirect flag set, leave them be and don't try to muck about
        // with them.
        if (!ignoreOverrideRedirect && event.override_redirect) {
            return;
        }
        
        // Apply "inverse" window location to the specified size, to work out
        // required dimensions of parent window.
        rascUI::Location inverseWindowLocation = getInverseWindowLocation();
        inverseWindowLocation.fitCacheInto(rascUI::Rectangle(event.x, event.y, event.width, event.height), *defaultXScalePtr, *defaultYScalePtr);
        
        // If auto-place is disabled, we must try to honour the position specified
        // by the client, rather than letting rascUIxcb place the window itself.
        rascUI::WindowConfig config;
        if (!autoPlaceNewWindows) {
            // Tell rascUIxcb not to auto-place the window.
            config.enableScreenPosition = true;
            if (event.override_redirect) {
                // If the client window has the override redirect flag set, (popup etc),
                // keep the original x and y (even if below zero). If we offset it back
                // (like for normal windows), tooltips (etc) end up higher up than they
                // should be, and lots of things break.
                config.screenX = event.x;
                config.screenY = event.y;
                
            } else {
                // Otherwise, make sure placement is sensible, then use it.
                clipWindowPlacement(inverseWindowLocation.cache);
                config.screenX = inverseWindowLocation.cache.x;
                config.screenY = inverseWindowLocation.cache.y;
            }
        }
        
        // Clamp sizes for sanity-check.
        config.screenWidth = std::max<GLfloat>(rascUIxcb::Misc::MIN_CLAMP_SIZE, inverseWindowLocation.cache.width);
        config.screenHeight = std::max<GLfloat>(rascUIxcb::Misc::MIN_CLAMP_SIZE, inverseWindowLocation.cache.height);
        
        // Create and store the custom window.
        windows.emplace(std::piecewise_construct,
            std::forward_as_tuple(event.window),
            std::forward_as_tuple(this, config, event));
    }
    
    void CustomContext::onDestroyNotify(const xcb_destroy_notify_event_t & event) {
        // If this is a notification about a child window we're managing being
        // destroyed, then just get rid of the parent window. Everything will
        // be cleaned up nicely, and parent window will go away too.
        const std::unordered_map<xcb_window_t, CustomWindow>::iterator iter = windows.find(event.window);
        if (iter != windows.end()) {
            const xcb_window_t parentId = iter->second.xcbGetWindow();
            windows.erase(iter);
            std::cout << "Destroyed: Parent: " << parentId << ", child: " << event.window << ", (managing " << windows.size() << " window(s) now).\n";
        }
    }
    
    bool CustomContext::onWindowEvent(const xcb_window_t window, xcb_generic_event_t * event, bool * repaintNeeded) {
        // Find the window, give up if it doesn't exist.
        const std::unordered_map<xcb_window_t, CustomWindow>::iterator iter = windows.find(window);
        if (iter == windows.end()) {
            return true;
        }
        
        // Pass it on to the window.
        return iter->second.handleWindowEvent(event, repaintNeeded);
    }
    
    void CustomContext::clipWindowPlacement(rascUI::Rectangle & rectangle) {
        // Grab active crtcs, (can't do anything if there aren't any!).
        const std::vector<rascUIxcb::ActiveCrtc> & activeCrtcs = xcbGetScreenConfig().getActiveCrtcs();
        if (!activeCrtcs.empty()) {
            
            // Work out which CRTC to place into.
            // Pick the first one if that's the only choice.
            const rascUIxcb::ActiveCrtc * activeCrtc = &activeCrtcs.front();
            
            // If there are more than one, find the one which overlaps most.
            // If none overlap (all return 0), just keep the first one as above.
            if (activeCrtcs.size() > 1) {
                GLfloat largestOverlap = 0.0f;
                for (const rascUIxcb::ActiveCrtc & otherCrtc : activeCrtcs) {
                    const GLfloat overlap = rascUIxcb::Misc::xcbToRascUIRect(otherCrtc.dimensions).overlapArea(rectangle);
                    if (overlap > largestOverlap) {
                        largestOverlap = overlap;
                        activeCrtc = &otherCrtc;
                    }
                }
            }
            
            // Make sure at least window decoration component size is shown bottom/right.
            const GLfloat xBorder = *defaultXScalePtr * defaultTheme->windowDecorationNormalComponentWidth;
            const GLfloat yBorder = *defaultYScalePtr * defaultTheme->windowDecorationNormalComponentHeight;
            
            // Make sure rectangle isn't pushed too far off the bottom/right.
            rectangle.x = std::min<GLfloat>(rectangle.x, activeCrtc->borderedDimensions.x + activeCrtc->borderedDimensions.width - xBorder);
            rectangle.y = std::min<GLfloat>(rectangle.y, activeCrtc->borderedDimensions.y + activeCrtc->borderedDimensions.height - yBorder);
            
            // Make sure top-left corner is visible.
            rectangle.x = std::max<GLfloat>(rectangle.x, activeCrtc->borderedDimensions.x);
            rectangle.y = std::max<GLfloat>(rectangle.y, activeCrtc->borderedDimensions.y);
            
            // If clip window size is enabled, make sure width/height aren't too large.
            if (clipWindowSize) {
                rectangle.width = std::min<GLfloat>(rectangle.width, activeCrtc->borderedDimensions.width);
                rectangle.height = std::min<GLfloat>(rectangle.height, activeCrtc->borderedDimensions.height);
            }
        }
    }
    
    void CustomContext::switchWorkspaceLeft(void) {
        switchWorkspace((currentWorkspace == 0u ? std::max(1u, workspaceCount) : currentWorkspace) - 1u);
    }
    
    void CustomContext::switchWorkspaceRight(void) {
        switchWorkspace((currentWorkspace + 1u) % std::max(1u, workspaceCount));
    }
    
    void CustomContext::switchWorkspace(const unsigned int newWorkspace) {
        if (newWorkspace != currentWorkspace) {
            // Store new workspace value.
            std::cout << "Switched to workspace " << newWorkspace << ".\n";
            currentWorkspace = newWorkspace;
            // Each window now needs its map status updated, (they need to know
            // whether to show on the current workspace now).
            for (std::pair<const xcb_window_t, CustomWindow> & windowPair : windows) {
                windowPair.second.updateMapStatus();
            }
        }
    }
    
    void CustomContext::showRunWindow(void) {
        if (runWindow == NULL) {
            runWindow = new RunWindow(*this, defaultTheme);
        }
        runWindow->setMapped(true);
    }
    
    void CustomContext::showSwitchWindow(void) {
        if (switchWindow == NULL) {
            switchWindow = new SwitchWindow(*this, defaultTheme);
        }
        switchWindow->setMapped(true);
    }
    
    void CustomContext::xcbOnMainLoopDraw(void) {
        // Give process manager an update each draw pass.
        processManager.update();
        // Notify move/resize state that we're at a draw pass, so it can do resizing etc.
        moveResizeState.onDraw();
    }
    
    bool CustomContext::wasSuccessful(void) const {
        return registeredRedirect && processManager.hasPipe() && rascUIxcb::Context::wasSuccessful();
    }
    
    unsigned long long CustomContext::mainLoop(void) {
        // Create resources at the start of the loop, start counting events.
        xcbMainLoopInit();
        unsigned long long eventCount = 0;
        
        // Loop until it is requested that we stop (from endMainLoop).
        // Don't necessarily have to quit out when we run out of mapped windows,
        // since that's not the only purpose of our context.
        while(continueMainLoop /*&& xcbCountMappedWindows() > 0*/) {
            
            // Get event (note that we must free this).
            xcb_generic_event_t * event;
            bool error;
            xcbMainLoopGetEvent(&event, &error);
            
            // If there was an error in getting the event, end the main loop (but free the event anyway!). Otherwise, increment the event count.
            if (error) {
                free(event);
                break;
            }
            eventCount++;
            
            // If an event was created handle the event.
            bool repaintNeeded = true;
            if (event != NULL) {
                if (handleEventInternal(event, &repaintNeeded)) {
                    xcbMainLoopHandleEvent(event, &repaintNeeded);
                }
            }
            
            // Repaint and sync if needed, even if there was no event (we still need to update function queues in that case).
            if (repaintNeeded) {
                xcbMainLoopRepaintAndSync();
            }
            
            // Free the event since it is dynamically allocated. When we have no event, it will be NULL (but freeing NULL is fine).
            free(event);
        }
        
        // Clean up at end, return event count.
        xcbMainLoopCleanUp();
        return eventCount;
    }
    
    void CustomContext::setRootCursor(xcb_cursor_t rootCursor) {
        // All we need to do is set the root window's cursor attribute.
        xcb_change_window_attributes(xcbGetConnection(), xcbGetScreen()->root, XCB_CW_CURSOR, &rootCursor);
    }
    
    MoveResizeState & CustomContext::getMoveResizeState(void) {
        return moveResizeState;
    }
    
    ProcessManager & CustomContext::getProcessManager(void) {
        return processManager;
    }
    
    void CustomContext::focusWindow(const xcb_window_t childWindowId) const {
        // Leave focus up to the X server in focus follows mouse mouse.
        if (!focusFollowsMouse) {
            xcb_set_input_focus(xcbGetConnection(), XCB_INPUT_FOCUS_POINTER_ROOT, childWindowId, XCB_CURRENT_TIME);
        }
    }
    
    void CustomContext::sendToTopOfBucket(const xcb_window_t childWindowId, const Buckets bucket) {
        const std::unordered_map<xcb_window_t, CustomWindow>::iterator iter = windows.find(childWindowId);
        if (iter != windows.end()) {
            iter->second.sendToTopOfBucket(bucket);
        }
    }
    
    CustomWindow * CustomContext::getWindowByParent(const xcb_window_t parentId) const {
        const std::unordered_map<xcb_window_t, CustomWindow *>::const_iterator iter = windowsByParent.find(parentId);
        if (iter != windowsByParent.end()) {
            return iter->second;
        }
        return NULL;
    }
    
    CustomWindow * CustomContext::getWindowByChild(const xcb_window_t childId) {
        const std::unordered_map<xcb_window_t, CustomWindow>::iterator iter = windows.find(childId);
        if (iter != windows.end()) {
            return &iter->second;
        }
        return NULL;
    }
    
    Buckets CustomContext::getBucketByPlaceholderWindowId(const xcb_window_t windowId) const {
        for (size_t index = 0; index < buckets.size(); index++) {
            if (buckets[index].getPlaceholderWindowId() == windowId) {
                return (Buckets)index;
            }
        }
        return Buckets::INVALID;
    }
    
    unsigned int CustomContext::getWorkspaceCount(void) const {
        return workspaceCount;
    }
    
    int CustomContext::getWindowSnapDistance(void) const {
        // Multiply snap distance by scale, clamp to range of 16 bit int for
        // usage.
        return rascUIxcb::Misc::clampToPureInt16(*defaultXScalePtr * windowSnapDistance);
    }
        
    rascUI::Location CustomContext::getInverseWindowLocation(void) const {
        #define LAYOUT_THEME defaultTheme
        return rascUI::Location(
            SUB_MARGIN(-LAYOUT_THEME->windowDecorationUiBorder,
             SUB_MARGIN_T(-UI_BORDER - UI_BHEIGHT,
              GEN_FILL
            ))
        );
        #undef LAYOUT_THEME
    }
}
