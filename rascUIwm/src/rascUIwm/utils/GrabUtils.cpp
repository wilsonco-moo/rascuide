/*
 * GrabUtils.cpp
 *
 *  Created on: 24 Sep 2021
 *      Author: wilson
 */

#include "GrabUtils.h"

#include <rascUIxcb/util/KeySymbols.h>
#include <algorithm>
#include <cstdlib>
#include <array>

namespace rascUIwm {
    
    // Utility: ORs values in an array.
    template<typename Type, size_t Count>
    constexpr static Type arrayOr(const std::array<Type, Count> arr) {
        Type result = 0;
        for (size_t index = 0; index < Count; index++) {
            result |= arr[index];
        }
        return result;
    }
    
    // Array of extra mod masks (for iterating through), which we add to
    // each grab, (see documentation in header).
    constexpr static std::array<uint16_t, 4> EXTRA_MOD_MASK_ARR({
        0,
        XCB_MOD_MASK_2,
        XCB_MOD_MASK_LOCK,
        XCB_MOD_MASK_2 | XCB_MOD_MASK_LOCK
    });
    
    // Mask of modifiers which we ignore, (note: this has the limitation
    // that we can't include button mod masks in key grabs, but that is
    // probably alright, as doing that doesn't make much sense anyway).
    // Doing this is required as these show up in the modifier state for
    // mouse release events.
    constexpr static uint16_t IGNORED_MOD_MASK =
        XCB_BUTTON_MASK_1 |
        XCB_BUTTON_MASK_2 |
        XCB_BUTTON_MASK_3 |
        XCB_BUTTON_MASK_4 |
        XCB_BUTTON_MASK_5;
    
    // Mask which excludes the "extra" and "ignored" mod masks.
    constexpr static uint16_t REGULAR_MOD_MASK = ~(arrayOr(EXTRA_MOD_MASK_ARR) | IGNORED_MOD_MASK);
    
    
    // -------------------------- KeyGrab ------------------------------
    
    bool KeyGrab::KeyCodeMapping::operator < (const KeyCodeMapping & other) const {
        return keyCode < other.keyCode;
    }
    
    bool KeyGrab::KeyCodeMapping::operator < (const xcb_keycode_t other) const {
        return keyCode < other;
    }
    
    KeyGrab::KeyGrab(xcb_connection_t * connection, const rascUIxcb::KeySymbols & keySymbols, uint8_t ownerEvents, uint16_t modifiers, const std::initializer_list<xcb_keysym_t> & keySyms, uint8_t pointerMode, uint8_t keyboardMode) :
        connection(connection),
        keyCodes(),
        modifiers(modifiers & REGULAR_MOD_MASK) { // Note: remove "extra" modifiers from provided mod mask.
        
        if (connection != NULL) {
            // For each of the specified keysyms, get the key codes. If valid:
            for (const xcb_keysym_t keySym : keySyms) {
                xcb_keycode_t * const keyCodesArray = keySymbols.keySymToKeyCodes(keySym);
                if (keyCodesArray != NULL) {
                    // For each one, add to our key codes vector.
                    for (const xcb_keycode_t * keyCodeUpto = keyCodesArray; *keyCodeUpto != XCB_NO_SYMBOL; keyCodeUpto++) {
                        keyCodes.push_back({ *keyCodeUpto, keySym });
                    }
                    free(keyCodesArray);
                }
            }
            
            // Shrink the vector now we know its size. Also, sort the list of
            // key codes, so we can use lower_bound to search them later.
            keyCodes.shrink_to_fit();
            std::sort(keyCodes.begin(), keyCodes.end());
        
            // Iterate through each key code.
            const xcb_setup_t * setup = xcb_get_setup(connection);
            for (const KeyCodeMapping & keyCodeMapping : keyCodes) {
                // Iterate through each screen, run grabKeyAllModifiers on root window of each.
                for (xcb_screen_iterator_t screenIter = xcb_setup_roots_iterator(setup); screenIter.rem > 0; xcb_screen_next(&screenIter)) {
                    grabKeyAllModifiers(connection, ownerEvents, screenIter.data->root, this->modifiers, keyCodeMapping.keyCode, pointerMode, keyboardMode);
                }
            }
        }
    }
    
    KeyGrab::~KeyGrab(void) {
        if (connection != NULL) {
            // Iterate through each key code.
            const xcb_setup_t * setup = xcb_get_setup(connection);
            for (const KeyCodeMapping & keyCodeMapping : keyCodes) {
                // Iterate through each screen, run ungrabKeyAllModifiers on root window of each.
                for (xcb_screen_iterator_t screenIter = xcb_setup_roots_iterator(setup); screenIter.rem > 0; xcb_screen_next(&screenIter)) {
                    ungrabKeyAllModifiers(connection, keyCodeMapping.keyCode, screenIter.data->root, modifiers);
                }
            }
        }
    }
    
    bool KeyGrab::matchesEvent(xcb_keysym_t & keySym, const xcb_key_press_event_t * event, uint16_t extraIgnoreModifiers) const {
        // If we are not set to ANY event mask, return false if (regular) modifiers don't match,
        // (ignore modifiers are filtered out).
        if (modifiers != XCB_MOD_MASK_ANY && (event->state & REGULAR_MOD_MASK & (~extraIgnoreModifiers)) != modifiers) {
            return false;
        }
        
        // Return true if any of our key codes match the event detail. Also,
        // return translated key sym on a positive match.
        const std::vector<KeyCodeMapping>::const_iterator iter = std::lower_bound(keyCodes.begin(), keyCodes.end(), event->detail);
        if (iter != keyCodes.end() && iter->keyCode == event->detail) {
            keySym = iter->keySym;
            return true;
        }
        return false;
    }
    
    void KeyGrab::grabKeyAllModifiers(xcb_connection_t * connection, uint8_t ownerEvents, xcb_window_t grabWindow, uint16_t modifiers, xcb_keycode_t key, uint8_t pointerMode, uint8_t keyboardMode) {
        for (uint16_t extraModMask : EXTRA_MOD_MASK_ARR) {
            xcb_grab_key(connection, ownerEvents, grabWindow, modifiers | extraModMask, key, pointerMode, keyboardMode);
        }
    }
    
    void KeyGrab::ungrabKeyAllModifiers(xcb_connection_t * connection, xcb_keycode_t key, xcb_window_t grabWindow, uint16_t modifiers) {
        for (uint16_t extraModMask : EXTRA_MOD_MASK_ARR) {
            xcb_ungrab_key(connection, key, grabWindow, modifiers | extraModMask);
        }
    }
    
    // ------------------------- ButtonGrab ----------------------------
    
    ButtonGrab::ButtonGrab(xcb_connection_t * connection, uint8_t ownerEvents, uint16_t eventMask, uint8_t pointerMode, uint8_t keyboardMode, xcb_cursor_t cursor, xcb_button_t button, uint16_t modifiers) :
        connection(connection),
        modifiers(modifiers & REGULAR_MOD_MASK), // Note: remove "extra" modifiers from provided mod mask.
        button(button) {
        
        if (connection != NULL) {
            const xcb_setup_t * setup = xcb_get_setup(connection);
                
            // Iterate through each screen, run grabButtonAllModifiers on root window of each.
            for (xcb_screen_iterator_t screenIter = xcb_setup_roots_iterator(setup); screenIter.rem > 0; xcb_screen_next(&screenIter)) {
                grabButtonAllModifiers(connection, ownerEvents, screenIter.data->root, eventMask, pointerMode, keyboardMode, XCB_NONE, cursor, button, this->modifiers);
            }
        }
    }
    
    ButtonGrab::~ButtonGrab(void) {
        if (connection != NULL) {
            const xcb_setup_t * setup = xcb_get_setup(connection);
            
            // Iterate through each screen, run ungrabButtonAllModifiers on root window of each.
            for (xcb_screen_iterator_t screenIter = xcb_setup_roots_iterator(setup); screenIter.rem > 0; xcb_screen_next(&screenIter)) {
                ungrabButtonAllModifiers(connection, button, screenIter.data->root, modifiers);
            }
        }
    }
    
    bool ButtonGrab::matchesEvent(const xcb_button_press_event_t * event) const {
        // We match only if (regular) modifiers match, and event detail matches our button,
        // (unless modifiers/button are set to any, then we always match).
        return (modifiers == XCB_MOD_MASK_ANY || (event->state & REGULAR_MOD_MASK) == modifiers) &&
               (button == XCB_BUTTON_INDEX_ANY || event->detail == button);
    }
    
    bool ButtonGrab::matchesEventNoModifiers(const xcb_button_release_event_t * event) const {
        return button == XCB_BUTTON_INDEX_ANY || event->detail == button;
    }
    
    void ButtonGrab::grabButtonAllModifiers(xcb_connection_t * connection, uint8_t ownerEvents, xcb_window_t grabWindow, uint16_t eventMask, uint8_t pointerMode, uint8_t keyboardMode, xcb_window_t confineTo, xcb_cursor_t cursor, xcb_button_t button, uint16_t modifiers) {
        for (uint16_t extraModMask : EXTRA_MOD_MASK_ARR) {
            xcb_grab_button(connection, ownerEvents, grabWindow, eventMask, pointerMode, keyboardMode, confineTo, cursor, button, modifiers | extraModMask);
        }
    }
    
    void ButtonGrab::ungrabButtonAllModifiers(xcb_connection_t * connection, xcb_button_t button, xcb_window_t grabWindow, uint16_t modifiers) {
        for (uint16_t extraModMask : EXTRA_MOD_MASK_ARR) {
            xcb_ungrab_button(connection, button, grabWindow, modifiers | extraModMask);
        }
    }
}
