/*
 * Bucket.h
 *
 *  Created on: 1 Sep 2024
 *      Author: wilson
 */

#ifndef RASCUIWM_UTILS_BUCKET_H_
#define RASCUIWM_UTILS_BUCKET_H_

#include <xcb/xcb.h>

namespace rascUIwm {
    class CustomContext;

    /**
     * Windows are stacked in "buckets", each with a (never mapped) placeholder
     * window at the top. Moving to the top of a bucket means putting a window
     * directly behind the placeholder.
     */
    class Bucket final {
    private:
        CustomContext & context;
        xcb_window_t placeholderWindow;

    public:
        /**
         * Must provide context (for connection and so we don't try to add
         * frames to bucket placeholders!), and bucket above (or NULL), so the
         * initial order of the buckets can be set.
         */
        Bucket(CustomContext & context, const Bucket * const bucketAboveMe);
        ~Bucket(void);
        
        /**
         * Sends the specified window to the top of the bucket.
         */
        void sendToTop(const xcb_window_t window) const;
        
        /**
         * Gets xcb id of our placeholder window.
         */
        xcb_window_t getPlaceholderWindowId(void) const;
    };
}

#endif
