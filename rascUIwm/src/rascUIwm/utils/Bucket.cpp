/*
 * Bucket.cpp
 *
 *  Created on: 1 Sep 2024
 *      Author: wilson
 */

#include "Bucket.h"

#include "../CustomContext.h"

namespace rascUIwm {

    Bucket::Bucket(CustomContext & context, const Bucket * const bucketAboveMe) :
        context(context) {
        
        xcb_connection_t * const connection = context.xcbGetConnection();
        const xcb_screen_t * const screen = context.xcbGetScreen();
        
        // Generate id for placeholder window.
        placeholderWindow = xcb_generate_id(connection);
        
        // Create the window (never map it).
        xcb_create_window(
            context.xcbGetConnection(),                       // The connection.
            XCB_COPY_FROM_PARENT,                             // The depth.
            placeholderWindow,                                // The window id.
            screen->root,                                     // The parent window (the root window).
            0, 0, 1, 1,                                       // x, y, width, height
            0,                                                // border width.
            XCB_WINDOW_CLASS_INPUT_OUTPUT,                    // The window class (type).
            screen->root_visual,                              // Visual for the window.
            0, NULL);                                         // The mask and values to enable events.
        
        if (bucketAboveMe == NULL) {
            // First bucket: Put at the top.
            const uint32_t stackMode = XCB_STACK_MODE_ABOVE;
            xcb_configure_window(connection, placeholderWindow, XCB_CONFIG_WINDOW_STACK_MODE, &stackMode);
        } else {
            // Else, put immediately below previous bucket.
            const uint32_t values[] = {
                bucketAboveMe->placeholderWindow,
                XCB_STACK_MODE_BELOW
            };
            xcb_configure_window(connection, placeholderWindow, XCB_CONFIG_WINDOW_SIBLING | XCB_CONFIG_WINDOW_STACK_MODE, values);
        }
        
        // Add to excluded list, so we never try to create a frame around bucket
        // placeholder windows.
        context.excludedWindows.insert(placeholderWindow);
    }
    
    Bucket::~Bucket(void) {
        // Destroy the window now we're done.
        xcb_destroy_window(context.xcbGetConnection(), placeholderWindow);
    }
    
    void Bucket::sendToTop(const xcb_window_t window) const {
        // Put the window immediately below the bucket placeholder.
        const uint32_t values[] = {
            placeholderWindow,
            XCB_STACK_MODE_BELOW
        };
        xcb_configure_window(context.xcbGetConnection(), window, XCB_CONFIG_WINDOW_SIBLING | XCB_CONFIG_WINDOW_STACK_MODE, values);
    }
    
    xcb_window_t Bucket::getPlaceholderWindowId(void) const {
        return placeholderWindow;
    }
}
