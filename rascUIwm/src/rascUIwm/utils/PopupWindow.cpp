/*
 * PopupWindow.cpp
 *
 *  Created on: 31 Aug 2024
 *      Author: wilson
 */

#include "PopupWindow.h"

#include <rascUIxcb/util/ScreenConfig.h>
#include <rascUIxcb/platform/Context.h>
#include <rascUIxcb/util/Misc.h>
#include <xcb/xcb.h>
#include <cstddef>

namespace rascUIwm {

    PopupWindow::PopupWindow(rascUIxcb::Context & context, const rascUI::WindowConfig & config) :
        rascUIxcb::Window(&context, config),
        beenMappedBefore(false) {
    }
    
    PopupWindow::~PopupWindow(void) {
    }

    void PopupWindow::xcbRequestMapWindow(void) {
        rascUIxcb::Context & context = (rascUIxcb::Context &)*getContext();
        
        // If this isn't the first time we're mapped, place the window.
        // We want popups to always appear in a sensible place,
        // (configure is allowed while not mapped).
        if (beenMappedBefore) {
            const rascUIxcb::ActiveCrtc * const activeCrtc = context.xcbGetScreenConfig().getActiveCrtcUnderPointer();
            if (activeCrtc != NULL) {
                xcb_rectangle_t geometry = rascUIxcb::Misc::rascUIToXcbRect(xcbGetWindowGeometryArea());
                activeCrtc->place(geometry);
                
                // Do this by sending ourself a configure request event:
                // Doing it directly would configure the child window,
                // which is pointless as it is attached to parent.
                xcb_configure_request_event_t configureRequest;
                configureRequest.response_type = XCB_CONFIGURE_REQUEST;
                configureRequest.stack_mode = XCB_STACK_MODE_ABOVE; // Doesn't matter.
                configureRequest.parent = XCB_WINDOW_NONE; // Doesn't matter.
                configureRequest.window = xcbGetWindow();
                configureRequest.sibling = XCB_WINDOW_NONE; // Doesn't matter.
                configureRequest.x = geometry.x;
                configureRequest.y = geometry.y;
                configureRequest.width = geometry.width;
                configureRequest.height = geometry.height;
                configureRequest.border_width = 0; // Doesn't matter.
                configureRequest.value_mask = XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT;
                xcb_send_event(context.xcbGetConnection(), false, xcbGetWindow(), 0, (const char *)&configureRequest);
            }
        }
        beenMappedBefore = true;
        
        // Send map request event to map ourself.
        xcb_map_request_event_t mapRequestEvent;
        mapRequestEvent.response_type = XCB_MAP_REQUEST;
        mapRequestEvent.parent = XCB_WINDOW_NONE; // Doesn't matter.
        mapRequestEvent.window = xcbGetWindow();
        xcb_send_event(context.xcbGetConnection(), false, xcbGetWindow(), 0, (const char *)&mapRequestEvent);
    }
}
