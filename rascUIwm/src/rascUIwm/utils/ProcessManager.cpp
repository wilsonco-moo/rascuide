/*
 * ProcessManager.cpp
 *
 *  Created on: 26 Aug 2024
 *      Author: wilson
 */

#include "ProcessManager.h"

#include <rascUIxcb/util/Connection.h>
#include <sys/wait.h>
#include <signal.h>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstddef>
#include <thread>
#include <chrono>
#include <mutex>

namespace rascUIwm {

    namespace processManagerHelpers {
        static std::mutex dataMutex;
        static bool isRegistered = false;
        static int writePipe;
    }
    
    ProcessManager::ProcessManager(void) :
        hasSetPipe(false) {
    }
    
    ProcessManager::ProcessManager(const int writePipe) :
        ProcessManager() {
        setPipe(writePipe);
    }
    
    ProcessManager::~ProcessManager(void) {
        // Clear pipe and signal handler if we have one.
        clearPipe();

        // Can early out if no processes to end.
        if (processes.empty()) return;

        // Send all remaining processes a teminate signal.
        for (const std::pair<const pid_t, bool> & pidPair : processes) {
            kill(pidPair.first, SIGTERM);
        }

        // Repeat until no processes are left, or the timeout is up.
        const std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();
        const std::chrono::steady_clock::time_point endTime = startTime + std::chrono::seconds(1);
        while(!processes.empty() && std::chrono::steady_clock::now() < endTime) {
            // Wait a bit, then reap any processes who managed to end.
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            update();
        }

        // For any remaining (stubborn) ones, send them a kill signal
        // too if we're allowed.
        for (const std::pair<const pid_t, bool> & pidPair : processes) {
            if (pidPair.second) {
                kill(pidPair.first, SIGKILL);
            }
        }

        // Do a blocking wait for each of the processes we just killed,
        // to make sure they've actually ended. This may block for a while
        // if there are any we're not allowed to kill (above).
        for (const std::pair<const pid_t, bool> & pidPair : processes) {
            waitpid(pidPair.first, NULL, 0);
        }
    }
    
    void ProcessManager::signalHandler(int signal) {
        // See: https://stackoverflow.com/a/25341698
        // Archive: http://web.archive.org/web/20151122162939/https://stackoverflow.com/questions/25339266/cleaning-child-processes-with-handler-with-waitpid-pselect-fork-sigaction
        
        if (signal == SIGCHLD) {
            // When we're notified that a child process has ended, just write
            // to the connection's pipe! That'll guarantee an interruption to
            // rascUIxcb's event loop (interrupt pselect), and make sure we get
            // an update call "soon".
            // We can't rely on the signal itself interrupting pselect, as then
            // there's a race condition: If the signal shows up before we call
            // pselect (not while it is blocking), it wouldn't interrupt
            // anything!
            // Writing to the pipe guarantees that next time pselect is run by
            // rascUIxcb, it'll immediately return as there's data to read from
            // the pipe.
            // And as it turns out, we're allowed to call write from a signal
            // handler, see "man signal-safety",
            // Also: https://www.man7.org/linux/man-pages/man7/signal-safety.7.html
            // Archive: http://web.archive.org/web/20240813203016/https://www.man7.org/linux/man-pages/man7/signal-safety.7.html
            const char toWrite = 64; // Can be any arbitrary value.
            write(processManagerHelpers::writePipe, &toWrite, 1);
        }
    }
    
    bool ProcessManager::setPipe(const int writePipe) {
        // Clear any existing pipe first (hasSetPipe is now always false).
        if (hasSetPipe) {
            clearPipe();
        }
        
        // If the write pipe is valid:
        if (writePipe != -1) {
            
            // Try to register and set pipe in static data. If successful, set
            // hasSetPipe.
            const std::lock_guard<std::mutex> lock(processManagerHelpers::dataMutex);
            if (!processManagerHelpers::isRegistered) {
                processManagerHelpers::isRegistered = true;
                processManagerHelpers::writePipe = writePipe;
                hasSetPipe = true;
            }
            
            // If successuful, register our signal handler now pipe is set in
            // static data.
            if (hasSetPipe) {
                signal(SIGCHLD, &ProcessManager::signalHandler);
            }
        }
        
        return hasSetPipe;
    }

    void ProcessManager::clearPipe(void) {
        if (hasSetPipe) {
            // Clear our signal handler (set to default): We don't care about
            // notifications from child processes ending any more.
            signal(SIGCHLD, SIG_DFL);
            
            // Unregister static data.
            {
                const std::lock_guard<std::mutex> lock(processManagerHelpers::dataMutex);
                processManagerHelpers::isRegistered = false;
            }
            
            hasSetPipe = false;
        }
    }
    
    bool ProcessManager::hasPipe(void) const {
        return hasSetPipe;
    }
    
    void ProcessManager::update(void) {
        // Keep waiting for (any) child process to end.
        while(true) {
            const pid_t pid = waitpid(-1, NULL, WNOHANG);
            if (pid > 0) {
                // A child process has ended, remove it from our list of
                // processes.
                processes.erase(pid);
            } else {
                // No children have ended: We're done.
                break;
            }
        }
    }
    
    pid_t ProcessManager::start(const char * path, bool canBeKilled, const char * const * args) {
        const pid_t pid = fork();
        if (pid == 0) {
            // Child process: do exec and replace process. Need a const cast
            // because for some reason exec wants non-const strings.
            execvp(path, const_cast<char * const *>(args));
            // Execv will only return if it fails, if so, just complain and end
            // the child process.
            const int execError = errno;
            std::cerr << "Cannot execute \"" << path << "\", ";
            for (const char * const * argsUpto = args; *argsUpto != NULL; argsUpto++) {
                std::cerr << '"' << *argsUpto << "\", ";
            }
            std::cerr << "error: \"" << std::strerror(execError) << "\".\n";
            std::exit(EXIT_FAILURE);
        } else {
            // Parent process: If fork was sucessful (didn't return -1), then
            // add to the list of processes.
            if (pid > 0) {
                processes.emplace(pid, canBeKilled);
            }
        }
        return pid;
    }
    
    bool ProcessManager::isAlive(pid_t pid) const {
        return processes.find(pid) != processes.end();
    }
}
