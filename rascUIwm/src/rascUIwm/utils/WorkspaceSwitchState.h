/*
 * WorkspaceSwitchState.h
 *
 *  Created on: 3 Sep 2024
 *      Author: wilson
 */

#ifndef RASCUIWM_UTILS_WORKSPACESWITCHSTATE_H_
#define RASCUIWM_UTILS_WORKSPACESWITCHSTATE_H_

#include <xcb/xcb.h>
#include <vector>

namespace rascUIwm {
    class CustomContext;

    /**
     *
     */
    class WorkspaceSwitchState final {
    private:
        CustomContext & context;
        std::vector<char> characters;
        xcb_get_input_focus_cookie_t inputFocusCookie;

    public:
        WorkspaceSwitchState(CustomContext & context);
        ~WorkspaceSwitchState(void);

        void onCharacter(const char character);
        void onFinish(void);
    };
}

#endif
