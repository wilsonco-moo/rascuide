/*
 * GrabUtils.h
 *
 *  Created on: 24 Sep 2021
 *      Author: wilson
 */

#ifndef RASCUIWM_UTILS_GRABUTILS_H_
#define RASCUIWM_UTILS_GRABUTILS_H_

#include <xcb/xcb_keysyms.h>
#include <initializer_list>
#include <xcb/xcb.h>
#include <cstdint>
#include <vector>

namespace rascUIxcb {
    class KeySymbols;
}

namespace rascUIwm {
    
    /**
     * Utility class encapsulating a key grab - the key is grabbed
     * when we're created, and ungrabbed when we're destroyed.
     * The grab is established on all screen root windows, using all key
     * codes which match the key symbol, using all 4 combinations of
     * num lock and caps lock.
     * 
     * All grabs in KeyGrab and ButtonGrab use all four combinations of
     * num lock and caps lock, because num lock and caps lock are
     * included in the modifier mask (so treated as part of the
     * button/key combination).
     * 
     * See the following comment:
     * Original: https://stackoverflow.com/questions/35906243/no-error-on-xcb-grab-key-but-event-loop-not-catching-global-hotkey#comment87969165_37062492
     * Archive:  http://web.archive.org/web/20210725135120/https://stackoverflow.com/questions/35906243/no-error-on-xcb-grab-key-but-event-loop-not-catching-global-hotkey
     * 
     * Also see:
     * Original: https://stackoverflow.com/questions/19376338/xcb-keyboard-button-masks-meaning
     * Archive:  http://web.archive.org/web/20210725134625/https://stackoverflow.com/questions/19376338/xcb-keyboard-button-masks-meaning
     */
    class KeyGrab {
    private:
        class KeyCodeMapping {
        public:
            xcb_keycode_t keyCode;
            xcb_keysym_t keySym;
            bool operator < (const KeyCodeMapping & other) const;
            bool operator < (const xcb_keycode_t other) const;
        };
    
        xcb_connection_t * connection;
        std::vector<KeyCodeMapping> keyCodes;
        uint16_t modifiers;
        
        // Don't allow copying, we hold raw resources and X resources.
        KeyGrab(const KeyGrab & other);
        KeyGrab & operator = (const KeyGrab & other);
        
    public:
        /**
         * Constructor: Here we take a connection on which to establish
         * from, but no window-related parameters as we grab always from
         * root window.
         * Instead of key codes, for convenience here we accept a
         * rascUIxcb::KeySymbols table, and a list of xcb_keysym_t key symbols.
         * The grab is established for all key codes related to all specified
         * key symbols.
         * This means, a single key grab can conveniently listen out for
         * arbitrarily many key symbols, and match any of them. Matching uses
         * a binary search over key codes, so is better than a long chain of
         * ifs. (A single matchesEvent call can be followed with a switch-case
         * for the key symbol).
         * 
         * Note: Don't pass in duplicate key symbols: We make no attempt to
         * de-duplicate key codes!
         */
        KeyGrab(xcb_connection_t * connection, const rascUIxcb::KeySymbols & keySymbols, uint8_t ownerEvents,
            uint16_t modifiers, const std::initializer_list<xcb_keysym_t> & keySyms, uint8_t pointerMode, uint8_t keyboardMode);
        ~KeyGrab(void);
        
        /**
         * Returns true if the specified key press/release event matches
         * this key grab. Optionally, additional ignore modifiers can be
         * provided: (Helpful when trying to listen for the release of a
         * modifier key, as the modifier key itself is *sometimes* included in
         * the release!).
         * Note that this matches either press OR release, since they
         * use the same underlying type.
         * On a sucessful match, the associated keysym is also returned.
         */
        bool matchesEvent(xcb_keysym_t & keySym, const xcb_key_press_event_t * event, uint16_t extraIgnoreModifiers = 0) const;
        
        /**
         * Wrapper for xcb_grab_key:
         * Grabs the specified key combination, using for all four
         * possible combinations of num lock and caps lock.
         * Matches ungrabKeyAllModifiers.
         */
        static void grabKeyAllModifiers(xcb_connection_t * connection, uint8_t ownerEvents,
            xcb_window_t grabWindow, uint16_t modifiers, xcb_keycode_t key, uint8_t pointerMode, uint8_t keyboardMode);
        
        /**
         * Wrapper for xcb_ungrab_key:
         * Ungrabs a key combination, using all four possible
         * combinations of num lock and caps lock.
         * Matches grabKeyAllModifiers.
         */
        static void ungrabKeyAllModifiers(xcb_connection_t * connection, xcb_keycode_t key, xcb_window_t grabWindow, uint16_t modifiers);
    };
    
    /**
     * Utility class encapsulating a button grab - the button is grabbed
     * when we're created, and ungrabbed when we're destroyed.
     * The grab is established on all screens, using all 4 combinations
     * of num lock and caps lock modifiers.
     */
    class ButtonGrab {
    private:
        xcb_connection_t * connection;
        uint16_t modifiers;
        xcb_button_t button;
        
        // Don't allow copying, we hold X resources.
        ButtonGrab(const ButtonGrab & other);
        ButtonGrab & operator = (const ButtonGrab & other);
        
    public:
        /**
         * Constructor: Here we take a connection on which to establish
         * from, but no window-related parameters as we grab always from
         * root window.
         */
        ButtonGrab(xcb_connection_t * connection, uint8_t ownerEvents, uint16_t eventMask,
            uint8_t pointerMode, uint8_t keyboardMode, xcb_cursor_t cursor, xcb_button_t button, uint16_t modifiers);
        ~ButtonGrab(void);
        
        /**
         * Returns true if the specified button press/release event
         * matches this button grab.
         * Note that this matches either press OR release, since they
         * use the same underlying type.
         */
        bool matchesEvent(const xcb_button_press_event_t * event) const;
        
        /**
         * Returns true if the specified button press/release event
         * matches this button grab, ignoring any modifiers. This should be used
         * for spotting the button being released: The user could've released
         * the modifier key by the time they release the mouse button.
         * Note that this matches either press OR release, since they
         * use the same underlying type.
         */
        bool matchesEventNoModifiers(const xcb_button_release_event_t * event) const;
        
        /**
         * Wrapper for xcb_grab_button:
         * Grabs a button, using all four possible combinations of num
         * lock and caps lock.
         * Matches ungrabButtonAllModifiers.
         */
        static void grabButtonAllModifiers(xcb_connection_t * connection, uint8_t ownerEvents, xcb_window_t grabWindow, uint16_t eventMask,
            uint8_t pointerMode, uint8_t keyboardMode, xcb_window_t confineTo, xcb_cursor_t cursor, xcb_button_t button, uint16_t modifiers);
        
        /**
         * Wrapper for xcb_ungrab_button:
         * Ungrabs a button, using all four possible combinations of num
         * lock and caps lock.
         * Matches grabButtonAllModifiers.
         */
        static void ungrabButtonAllModifiers(xcb_connection_t * connection, xcb_button_t button, xcb_window_t grabWindow, uint16_t modifiers);
    };
}

#endif
