/*
 * WorkspaceSwitchState.cpp
 *
 *  Created on: 3 Sep 2024
 *      Author: wilson
 */

#include "WorkspaceSwitchState.h"

#include "../CustomContext.h"
#include "../CustomWindow.h"

#include <algorithm>
#include <cstdlib>

namespace rascUIwm {

    WorkspaceSwitchState::WorkspaceSwitchState(CustomContext & context) :
        context(context),
        characters(),
        inputFocusCookie({ 0 }) {
    }
    
    WorkspaceSwitchState::~WorkspaceSwitchState(void) {
        // Discard any pending replies.
        if (inputFocusCookie.sequence != 0) {
            xcb_discard_reply(context.xcbGetConnection(), inputFocusCookie.sequence);
        }
    }
    
    void WorkspaceSwitchState::onCharacter(const char character) {
        // Query for input focus if not already done that (if not started yet).
        if (inputFocusCookie.sequence == 0) {
            inputFocusCookie = xcb_get_input_focus(context.xcbGetConnection());
        }
        
        // Append the character.
        characters.push_back(character);
    }
    
    void WorkspaceSwitchState::onFinish(void) {
        // Get reply to find the CustomWindow associated with input focus
        // (if any).
        xcb_get_input_focus_reply_t * const inputFocusReply = xcb_get_input_focus_reply(context.xcbGetConnection(), inputFocusCookie, NULL);
        if (inputFocusReply != NULL) {
            CustomWindow * const customWindow = context.getWindowByChild(inputFocusReply->focus);
            if (customWindow != NULL) {
                // Convert characters to the workspace number. Append a '\0' so
                // characters vector is a string.
                characters.push_back('\0');
                const unsigned int workspaceIndex = std::strtoul(&characters[0], NULL, 10) % std::max(1u, context.getWorkspaceCount());
                // Move window to that workspace.
                customWindow->setWorkspace(workspaceIndex);
            }
            free(inputFocusReply);
        }
        
        // Clear character list and cookie for next time.
        characters.clear();
        inputFocusCookie = xcb_get_input_focus_cookie_t({ 0 });
    }
}
