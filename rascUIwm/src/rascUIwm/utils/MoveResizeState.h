/*
 * MoveResizeState.h
 *
 *  Created on: 25 Sep 2021
 *      Author: wilson
 */

#ifndef RASCUIWM_UTILS_MOVERESIZESTATE_H_
#define RASCUIWM_UTILS_MOVERESIZESTATE_H_

#include <xcb/xcb.h>
#include <cstdint>
#include <GL/gl.h>
#include <vector>

namespace rascUIxcb {
    class ActiveCrtc;
}

namespace rascUIwm {
    class CustomContext;

    enum class ResizeMode : uint8_t {
        backward, none, forward
    };

    /**
     * Stores relevant state, and contains the logic for moving and
     * resizing windows.
     */
    class MoveResizeState {
    private:
        bool active, isMove, hasReadGeometryCookie, startedFromRascUI, hasPendingMouseMove;
        ResizeMode resizeModeX, resizeModeY;
        
        int16_t initialMouseX, initialMouseY, initialWindowX, initialWindowY, pendingMouseX, pendingMouseY;
        uint16_t initialWindowWidth, initialWindowHeight;
        unsigned int minWindowWidth, minWindowHeight;
        
        xcb_get_geometry_cookie_t geometryCookie;
        xcb_window_t window;
        
        CustomContext & context;
        
        // Store these so we can re-use the same allocations each time.
        std::vector<int32_t> xBoundaries, yBoundaries;
        
    public:
        MoveResizeState(CustomContext & context);
        ~MoveResizeState(void);
        
    private:
        // Snaps the specified value to the specified boundaries.
        static int32_t snap(const int32_t value, const std::vector<int32_t> & boundaries, const int32_t snapDistance);
    
        // Populates xBoundaries and yBoundaries from overlapping active CRTCs.
        // x,y,width,height should be the window's intended dimensions,
        // if "considerSize" is true we add additional boundaries for both
        // window edges (i.e: for moving).
        void getBoundaries(const int32_t x, const int32_t y, const int32_t width, const int32_t height, const bool considerSize);
        // Adds the specified CRTC as boundaries, used by getBoundaries.
        void addBoundaries(const rascUIxcb::ActiveCrtc & activeCrtc, const int32_t width, const int32_t height, const bool considerSize);
    
        // If we have not yet read geometry cookie, reads windowGeometryCookie
        // and sets initialWindowX/initialWindowY.
        // If we're in resize mode, also works out the X and Y resize modes.
        void getGeometry(void);
        
        // Called internally by onDraw.
        void onMouseMove(int32_t mouseX, int32_t mouseY);
        
        // Gets resize mode for the axis, considering the size on the axis and opposite
        // axis, and position (relative to window) on axis and opposite axis.
        static ResizeMode getResizeMode(int32_t relativePos, int32_t opRelativePos, uint32_t size, uint32_t opSize);
        
        // Writes position and size, using the specified resize mode, according to the specified
        // initial pos, size and mouse diff.
        static void getResizeOutput(int32_t & outputPos, int32_t & outputSize, ResizeMode resizeMode, int32_t initialPos, int32_t initialSize, int32_t mouseDiff, int32_t minSize);
        // Snaps resize output according to specified boundaries.
        static void snapResizeOutput(int32_t & outputPos, int32_t & outputSize, const ResizeMode resizeMode, const std::vector<int32_t> & boundaries, const int32_t snapDistance);
        
        // Internal logic for start, used by both overloads.
        void start(int16_t initialMouseX, int16_t initialMouseY, xcb_window_t window, bool isMove, bool startedFromRascUI, ResizeMode resizeModeX, ResizeMode resizeModeY);
    public:
        /**
         * Called at the end of initialisation. Calculates min window size.
         */
        void initialise(void);
    
        /**
         * Starts moving or resizing, from a relevant button press
         * event. The isMove parameter should specify whether this is
         * a move or resize event.
         */
        void start(const xcb_button_press_event_t * pressEvent, bool isMove);
        
        /**
         * Starts moving or resizing, from a rascUI event. The isMove parameter
         * should specify whether this is a move or resize event, and resize
         * modes must be specified. Initial mouse position must be within the
         * specified window.
         */
        void start(GLfloat viewX, GLfloat viewY, xcb_window_t window, bool isMove, ResizeMode resizeModeX, ResizeMode resizeModeY);
        
        /**
         * Updates the move/resize, on motion notify.
         * This will do nothing if we're not active.
         * All this does is stores the pending mouse move: It gets actually
         * applied in onDraw.
         */
        void motionNotify(const xcb_motion_notify_event_t * motionEvent);
        
        /**
         * Updates the move/resize on a draw pass (xcbOnMainLoopDraw): Applies
         * any pending mouse moves.
         * This way, we don't try to configure windows more than once per
         * "draw pass" (per xcb_aux_sync). Even if we're not falling behind,
         * we can still end up sending out lots of configures, especially in
         * cases where motion notifies are interleaved with expose events:
         * Then even the event skipping won't stop things falling behind.
         */
        void onDraw(void);
        
        /**
         * Should be called when we encounter a matching release event.
         * Stops moving/resizing.
         * This will do nothing if we're not active.
         */
        void end(const xcb_button_release_event_t * releaseEvent);
    };
}

#endif
