/*
 * PopupWindow.h
 *
 *  Created on: 31 Aug 2024
 *      Author: wilson
 */

#ifndef RASCUIWM_UTILS_POPUPWINDOW_H_
#define RASCUIWM_UTILS_POPUPWINDOW_H_

#include <rascUIxcb/platform/Window.h>

namespace rascUIwm {

    /**
     * Simple extension to rascUIxcb window: When mapped, we send a map request
     * to ourself instead of directly mapping the window. This allows the
     * window manager to create a window which it manages like a window from
     * any other client.
     */
    class PopupWindow : public rascUIxcb::Window {
    private:
        bool beenMappedBefore;
        
    public:
        PopupWindow(rascUIxcb::Context & context, const rascUI::WindowConfig & config);
        virtual ~PopupWindow(void);
        
    protected:
        // Override to send a map request to ourself instead.
        virtual void xcbRequestMapWindow(void) override;
    };
}

#endif
