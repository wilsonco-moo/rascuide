/*
 * ProcessManager.h
 *
 *  Created on: 26 Aug 2024
 *      Author: wilson
 */

#ifndef RASCUIWM_UTILS_PROCESSMANAGER_H_
#define RASCUIWM_UTILS_PROCESSMANAGER_H_

#include <unordered_map>
#include <unistd.h>

namespace rascUIxcb { class Connection; }

namespace rascUIwm {

    /**
     * Simple manager for processes.
     */
    class ProcessManager final {
    private:
        // Map of processes, with bool flag for whether they can be killed.
        std::unordered_map<pid_t, bool> processes;
        // Whether we've got a pipe set.
        bool hasSetPipe;

        // Don't allow copying.
        ProcessManager(const ProcessManager & other);
        ProcessManager & operator = (const ProcessManager & other);

    public:
        /**
         * Default constructor: Can set a pipe later.
         */
        ProcessManager(void);
    
        /**
         * Auto-set a pipe.
         */
        ProcessManager(const int writePipe);
                
        ~ProcessManager(void);
        
    private:
        static void signalHandler(int signal);
        
    public:
        /**
         * Sets our pipe, returns true if successful. Only one ProcessManager
         * is allowed to have a pipe set!
         * Assigns a signal handler to write to this pipe when processes end.
         */
        bool setPipe(const int writePipe);
        
        /**
         * Clears the pipe and signal handler (call if you're destroying the
         * pipe before ProcessManager).
         */
        void clearPipe(void);
        
        /**
         * Returns whether we successfully set the pipe (only one process
         * manager can do this per process).
         */
        bool hasPipe(void) const;
        
        /**
         * Update/cleanup child processes to avoid zombies: Called each draw
         * pass.
         */
        void update(void);
        
        /**
         * Start a new process, giving a null terminated list of args.
         * If "canBeKilled" is true, when ProcessManager is destroyed, after
         * giving it a SIGTERM, we'll kill the process after a timeout.
         * Else, we'll block and wait for the process to die.
         */
        pid_t start(const char * path, bool canBeKilled, const char * const * args);
        
        /**
         * Queries if the specified process is still alive.
         */
        bool isAlive(pid_t pid) const;
    };
}

#endif
