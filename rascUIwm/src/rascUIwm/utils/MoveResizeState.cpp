/*
 * MoveResizeState.cpp
 *
 *  Created on: 25 Sep 2021
 *      Author: wilson
 */

#include "MoveResizeState.h"

#include <rascUIxcb/util/ScreenConfig.h>
#include <rascUI/util/Rectangle.h>
#include <rascUIxcb/util/Misc.h>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <limits>
#include <cmath>

#include "../CustomContext.h"

namespace rascUIwm {

    MoveResizeState::MoveResizeState(CustomContext & context) :
        active(false),
        context(context) {
    }
    
    MoveResizeState::~MoveResizeState(void) {
        // If we're destroyed while active, make sure we read the
        // geometry cookie as to not leave behind and unread cookie.
        if (active) {
            getGeometry();
        }
    }
    
    int32_t MoveResizeState::snap(const int32_t value, const std::vector<int32_t> & boundaries, const int32_t snapDistance) {
        // Find boundary which is closest to value (if any).
        int32_t smallestDistance = std::numeric_limits<int32_t>::max();
        int32_t smallestBoundary = value;
        for (const int32_t boundary : boundaries) {
            int32_t distance = std::abs(value - boundary);
            if (distance < smallestDistance) {
                smallestDistance = distance;
                smallestBoundary = boundary;
            }
        }
        
        // If smallest found distance is within snap distance, return relevant
        // boundary. Otherwise return value unchanged.
        return smallestDistance <= snapDistance ? smallestBoundary : value;
    }
    
    void MoveResizeState::getBoundaries(const int32_t x, const int32_t y, const int32_t width, const int32_t height, const bool considerSize) {
        // Clear any previous data.
        xBoundaries.clear();
        yBoundaries.clear();
        
        // Get CRTCs, give up if there aren't any.
        const std::vector<rascUIxcb::ActiveCrtc> & activeCrtcs = context.xcbGetScreenConfig().getActiveCrtcs();
        if (activeCrtcs.empty()) {
            return;
        }
        
        if (activeCrtcs.size() == 1) {
            // Only one: Add it as boundaries (saves the bother of overlap checks).
            addBoundaries(activeCrtcs.front(), width, height, considerSize);
        
        } else {
            // Otherwise, add any which overlap (or none!).
            const rascUI::Rectangle rectangle(x, y, width, height);
            for (const rascUIxcb::ActiveCrtc & activeCrtc : activeCrtcs) {
                if (rascUIxcb::Misc::xcbToRascUIRect(activeCrtc.borderedDimensions).overlaps(rectangle)) {
                    addBoundaries(activeCrtc, width, height, considerSize);
                }
            }
        }
    }
    
    void MoveResizeState::addBoundaries(const rascUIxcb::ActiveCrtc & activeCrtc, const int32_t width, const int32_t height, const bool considerSize) {
        // Add normal boundaries.
        xBoundaries.push_back(activeCrtc.borderedDimensions.x);
        xBoundaries.push_back(activeCrtc.borderedDimensions.x + activeCrtc.borderedDimensions.width);
        yBoundaries.push_back(activeCrtc.borderedDimensions.y);
        yBoundaries.push_back(activeCrtc.borderedDimensions.y + activeCrtc.borderedDimensions.height);
        
        // If considering size too, add boundaries for opposing edge (subtract
        // window width/height).
        if (considerSize) {
            xBoundaries.push_back(activeCrtc.borderedDimensions.x - width);
            xBoundaries.push_back(activeCrtc.borderedDimensions.x + activeCrtc.borderedDimensions.width - width);
            yBoundaries.push_back(activeCrtc.borderedDimensions.y - height);
            yBoundaries.push_back(activeCrtc.borderedDimensions.y + activeCrtc.borderedDimensions.height - height);
        }
    }
    
    void MoveResizeState::getGeometry(void) {
        if (!hasReadGeometryCookie) {
            xcb_get_geometry_reply_t * geometryReply = xcb_get_geometry_reply(context.xcbGetConnection(), geometryCookie, NULL);
            // If we get geometry use it, otherwise (if it failed
            // somehow) pick some placeholder values.
            if (geometryReply != NULL) {
                initialWindowX = geometryReply->x;
                initialWindowY = geometryReply->y;
                initialWindowWidth = geometryReply->width;
                initialWindowHeight = geometryReply->height;
                std::cout << "Geometry: x: " << initialWindowX << ", y: " << initialWindowY << "\n";
            } else {
                initialWindowX = 0;
                initialWindowY = 0;
                initialWindowWidth = 32;
                initialWindowHeight = 32;
            }
            hasReadGeometryCookie = true;
            free(geometryReply);
            
            if (startedFromRascUI) {
                // If started from rascUI, we already know resize modes, but
                // the initial mouse positions were relative to the window. So
                // convert them to root coordinates.
                initialMouseX = rascUIxcb::Misc::clampToPureInt16(initialWindowX + initialMouseX);
                initialMouseY = rascUIxcb::Misc::clampToPureInt16(initialWindowY + initialMouseY);
            } else {
                // Otherwise, we've got to work out the resize mode, which we
                // can do now we have the window's geometry.
                const int32_t mouseRelX = initialMouseX - initialWindowX,
                              mouseRelY = initialMouseY - initialWindowY;
                resizeModeX = getResizeMode(mouseRelX, mouseRelY, initialWindowWidth, initialWindowHeight);
                resizeModeY = getResizeMode(mouseRelY, mouseRelX, initialWindowHeight, initialWindowWidth);
            }
        }
    }
    
    void MoveResizeState::onMouseMove(int32_t mouseX, int32_t mouseY) {
        xcb_connection_t * const connection = context.xcbGetConnection();
        
        // Read window geometry cookie if not already read.
        getGeometry();
        
        // Get snap distance.
        const int32_t snapDistance = context.getWindowSnapDistance();
        
        // Work out how far the mouse has moved.
        const int32_t mouseDiffX = mouseX - initialMouseX,
                      mouseDiffY = mouseY - initialMouseY;
        
        
        // If moving, move window according to mouse diff.
        if (isMove) {
            // Find new intended position, use to grab boundaries, (pass true
            // as we want both edges snapping since this is a move).
            const int32_t newX = initialWindowX + mouseDiffX,
                          newY = initialWindowY + mouseDiffY;
            getBoundaries(newX, newY, initialWindowWidth, initialWindowHeight, true);
            
            // X stores these as 16 bit, so clamp to 16 bit range (as we're
            // doing calculations as 32-bit).
            const uint32_t moveOutput[2] = {
                rascUIxcb::Misc::clampToInt16(snap(newX, xBoundaries, snapDistance)),
                rascUIxcb::Misc::clampToInt16(snap(newY, yBoundaries, snapDistance))
            };
            xcb_configure_window(connection, window, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, moveOutput);
            
        // Otherwise, if resizing:
        } else {
            // Find new intended dimensions, use to grab boundaries.
            int32_t resizeOutput[4];
            getResizeOutput(resizeOutput[0], resizeOutput[2], resizeModeX, initialWindowX, initialWindowWidth, mouseDiffX, minWindowWidth);
            getResizeOutput(resizeOutput[1], resizeOutput[3], resizeModeY, initialWindowY, initialWindowHeight, mouseDiffY, minWindowHeight);
            getBoundaries(resizeOutput[0], resizeOutput[1], resizeOutput[2], resizeOutput[3], false);
            
            // Snap resize output now we have boundaries.
            snapResizeOutput(resizeOutput[0], resizeOutput[2], resizeModeX, xBoundaries, snapDistance);
            snapResizeOutput(resizeOutput[1], resizeOutput[3], resizeModeY, yBoundaries, snapDistance);
            
            // Clamp output and configure.
            uint32_t clampedResizeOutput[4] = {
                rascUIxcb::Misc::clampToInt16(resizeOutput[0]),
                rascUIxcb::Misc::clampToInt16(resizeOutput[1]),
                rascUIxcb::Misc::clampToSize16(resizeOutput[2], minWindowWidth),
                rascUIxcb::Misc::clampToSize16(resizeOutput[3], minWindowHeight)
            };
            xcb_configure_window(connection, window, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, clampedResizeOutput);
        }
    }
    
    ResizeMode MoveResizeState::getResizeMode(int32_t relativePos, int32_t opRelativePos, uint32_t size, uint32_t opSize) {
        // Get positions relative to centre (multiply instead of
        // dividing as only ratio is important).
        const int32_t centreRelPos = (relativePos * 2) - size,
                      opCentreRelPos = (opRelativePos * 2) - opSize;
        
        // If 3 times abs(axis) is greater than abs(opposite axis),
        // we're in corner region. Normalise values to be "square" by
        // multiplying by opposite axis size.
        if ((3 * std::abs(centreRelPos) * (int32_t)opSize) > (std::abs(opCentreRelPos) * (int32_t)size)) {
            // If in corner region, pick either forward or backward
            // depending on whether position is in front or behind
            // centre.
            return centreRelPos >= 0 ? ResizeMode::forward : ResizeMode::backward;
        // Otherwise, we're in a centre region so don't resize on this
        // axis.
        } else {
            return ResizeMode::none;
        }
    }
    
    void MoveResizeState::getResizeOutput(int32_t & outputPos, int32_t & outputSize, ResizeMode resizeMode, int32_t initialPos, int32_t initialSize, int32_t mouseDiff, int32_t minSize) {
        switch(resizeMode) {
        case ResizeMode::backward:
            // When resizing backward (moving back edge), add mouse diff
            // to position (to move back edge), and subtract mouse diff
            // from size (to compensate width). Don't let size go below min,
            // or we'd push the front edge.
            outputSize = std::max(initialSize - mouseDiff, minSize);
            outputPos = initialPos - (outputSize - initialSize);
            break;
        
        case ResizeMode::none:
            // With none resize, just use initial position/size.
            outputPos = initialPos;
            outputSize = initialSize;
            break;
        
        case ResizeMode::forward:
            // When resizing forward (moving front edge), leave position
            // unchanged, adjust size for mouse diff. Keep size above min, or
            // we can send negative sizes into the snapping stuff.
            outputPos = initialPos;
            outputSize = std::max(initialSize + mouseDiff, minSize);
            break;
        }
    }
    
    void MoveResizeState::snapResizeOutput(int32_t & outputPos, int32_t & outputSize, const ResizeMode resizeMode, const std::vector<int32_t> & boundaries, const int32_t snapDistance) {
        switch(resizeMode) {
            case ResizeMode::backward: {
                // When resizing backward (moving back edge), snap back edge
                // then subtract change from size (to compensate width).
                const int32_t initialPos = outputPos;
                outputPos = snap(outputPos, boundaries, snapDistance);
                outputSize -= (outputPos - initialPos);
                break;
            }
            
            case ResizeMode::none: {
                // No snapping to do in none mode.
            }
        
            case ResizeMode::forward: {
                // When resizing forward (moving front edge), snap front edge
                // then add change to size (to update it).
                const int32_t initialFrontEdge = outputPos + outputSize;
                const int32_t newFrontEdge = snap(initialFrontEdge, boundaries, snapDistance);
                outputSize += (newFrontEdge - initialFrontEdge);
                break;
            }
        }
    }
    
    void MoveResizeState::start(int16_t initialMouseX, int16_t initialMouseY, xcb_window_t window, bool isMove, bool startedFromRascUI, ResizeMode resizeModeX, ResizeMode resizeModeY) {
        // Do nothing if already active (trying to resize while moving
        // etc).
        if (active) {
            return;
        }
        
        // If child window is none, user is trying to move the root
        // window, so ignore.
        if (window == XCB_NONE) {
            return;
        }
        
        // Trying to resize a sensible window, set active/move status,
        // take note of window, set initial mouse position, send out
        // request for window geometry.
        active = true;
        this->isMove = isMove;
        this->window = window;
        this->initialMouseX = initialMouseX;
        this->initialMouseY = initialMouseY;
        this->startedFromRascUI = startedFromRascUI;
        this->resizeModeX = resizeModeX;
        this->resizeModeY = resizeModeY;
        hasReadGeometryCookie = false;
        hasPendingMouseMove = false;
        geometryCookie = xcb_get_geometry(context.xcbGetConnection(), window);
        
        std::cout << "MOVE START\n";        
        std::cout << "Initial mouse: x: " << initialMouseX << ", y: " << initialMouseY << " (" << (startedFromRascUI ? "relative to window" : "relative to root") << ")\n";
    }
    
    void MoveResizeState::initialise(void) {
        // Calculate minimum window width/height. That is, the outer dimensions
        // of a child window where size is 1x1.
        rascUI::Location inverseWindowLocation = context.getInverseWindowLocation();
        inverseWindowLocation.fitCacheInto(rascUI::Rectangle(0, 0, 1, 1), *context.defaultXScalePtr, *context.defaultYScalePtr);
        minWindowWidth = inverseWindowLocation.cache.width;
        minWindowHeight = inverseWindowLocation.cache.height;
    }
    
    void MoveResizeState::start(const xcb_button_press_event_t * pressEvent, bool isMove) {
        start(pressEvent->root_x, pressEvent->root_y, pressEvent->child, isMove, false, ResizeMode::none, ResizeMode::none);
    }
    
    void MoveResizeState::start(GLfloat viewX, GLfloat viewY, xcb_window_t window, bool isMove, ResizeMode resizeModeX, ResizeMode resizeModeY) {
        start(rascUIxcb::Misc::clampToPureInt16(viewX), rascUIxcb::Misc::clampToPureInt16(viewY), window, isMove, true, resizeModeX, resizeModeY);
    }

    void MoveResizeState::motionNotify(const xcb_motion_notify_event_t * motionEvent) {
        // Do nothing if not active.
        if (!active) {
            return;
        }
        
        // Else store the pending mouse move (applied when we get onDraw).
        hasPendingMouseMove = true;
        pendingMouseX = motionEvent->root_x;
        pendingMouseY = motionEvent->root_y;
    }
    
    void MoveResizeState::onDraw(void) {
        // Do nothing if not active.
        if (!active) {
            return;
        }
        
        // Else process any pending mouse move.
        if (hasPendingMouseMove) {
            onMouseMove(pendingMouseX, pendingMouseY);
            hasPendingMouseMove = false;
        }
    }

    void MoveResizeState::end(const xcb_button_release_event_t * releaseEvent) {
        // Do nothing if not active.
        if (!active) {
            return;
        }
        
        onMouseMove(releaseEvent->root_x, releaseEvent->root_y);
        
        active = false;
        std::cout << "MOVE STOP\n";
    }
}
