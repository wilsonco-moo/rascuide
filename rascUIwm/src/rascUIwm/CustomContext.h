/*
 * CustomContext.h
 *
 *  Created on: 14 Nov 2021
 *      Author: wilson
 */

#ifndef RASCUIWM_CUSTOMCONTEXT_H_
#define RASCUIWM_CUSTOMCONTEXT_H_

#include <rascUIxcb/platform/Context.h>
#include <rascUI/util/EmplaceArray.h>
#include <rascUI/util/Location.h>
#include <unordered_map>
#include <unordered_set>
#include <xcb/xcb.h>

#include "utils/WorkspaceSwitchState.h"
#include "utils/MoveResizeState.h"
#include "utils/GrabUtils.h"
#include "utils/Bucket.h"
#include "CustomWindow.h"

namespace rascUIwm {
    class ProcessManager;
    class CustomWindow;
    class RunWindow;
    class SwitchWindow;

    /**
     * Enum of buckets, top-most is first.
     */
    enum class Buckets : unsigned char {
        switchWindow, specialPopup, above, normal, COUNT, INVALID = COUNT
    };

    /**
     * Context implementation for rascUIwm.
     * 
     * By implementing our main loop as an extension of a rascUIxcb context,
     * we can get rascUIxcb to do most of the effort for us (and share most of
     * the code used for regular rascUIxcb gui stuff).
     * 
     * Links:
     * https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-i/
     * https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-ii/
     * https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-iii/
     * https://xcb.freedesktop.org/manual/group__XCB____API.html
     * https://community.kde.org/Xcb
     * https://www.x.org/releases/current/doc/man/man3/xcb-requests.3.xhtml
     * 
     * Archive:
     * http://web.archive.org/web/20240529073714/https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-i/
     * http://web.archive.org/web/20240609113408/https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-ii/
     * http://web.archive.org/web/20240314104019/https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-iii/
     * http://web.archive.org/web/20240329000244/https://xcb.freedesktop.org/manual/group__XCB____API.html
     * http://web.archive.org/web/20240615201939/https://community.kde.org/Xcb
     */
    class CustomContext : public rascUIxcb::Context {
    public:
        /**
         * Filename of the config file used for rascUIwm.
         * This resides within the directory reported by
         * PathUtils::getRascUIConfigDirectory.
         */
        static const char * const CONFIG_FILENAME;
        
    private:
        // So custom window and bucket can add themself as an excluded windows.
        friend class CustomWindow;
        friend class Bucket;
        
        // Process manager for our use.
        ProcessManager & processManager;
        
        // For moving windows between workspaces.
        WorkspaceSwitchState workspaceSwitchState;
        
        // Child (popup) windows owned by the window manager: Created only when
        // needed.
        RunWindow * runWindow;
        SwitchWindow * switchWindow;
        
        // Our array of buckets, used when sending windows to front.
        rascUI::EmplaceArray<Bucket, (size_t)Buckets::COUNT> buckets;
        
        // Key binds, move/resize state, etc.
        KeyGrab altTab, altShiftTab, ctrlAltAll, superR, alt, ctrlAlt;
        ButtonGrab altLeft, altRight, anyOtherClick;
        MoveResizeState moveResizeState;
        
        // The windows which we're managing for clients connected to the window
        // manager. Map from child window to rascUIxcb window.
        std::unordered_map<xcb_window_t, CustomWindow> windows;
        
        // Same as above, but mapping from parent (frame) window id.
        // This list is managed by the constructor/destructor of the
        // CustomWindow itself.
        std::unordered_map<xcb_window_t, CustomWindow *> windowsByParent;
        
        // Windows for which we ignore the next (single) create notify event
        // for. CustomWindow adds the created parent window here in its
        // constructor, the window is removed when we get the create notify.
        std::unordered_set<xcb_window_t> excludedWindows;

        // Current workspace and max number of workspaces.
        unsigned int currentWorkspace, workspaceCount;

        // Set true once we successfully manage to register for substructure
        // redirect on the root window.
        bool registeredRedirect;
        
        // Whether we ignore the override_redirect flag, and put window borders
        // around popup windows anyway. Loaded from config file.
        bool ignoreOverrideRedirect;
        
        // If set, we don't bother changing input focus (ever), and leave it to
        // the default chosen by the X server (which is following the mouse).
        // Loaded from config file.
        bool focusFollowsMouse;
        
        // If true, we ignore the position specified by create notify event, and
        // let rascUIxcb place the window. That puts it in the centre of the
        // active CRTC containing the mouse cursor.
        // If false, we honour the client's position in create notify event, but
        // "clip" to make sure the top-left corner of the window is visible,
        // and not too close to the bottom/right.
        bool autoPlaceNewWindows;
        
        // If true, we clip window sizes to make sure they're not larger than
        // the active CRTC we're clipping them against. Only applies if auto
        // placement is disabled!
        bool clipWindowSize;
        
        // Distance to screen (active CRTC) edges used when considering window
        // snapping. Zero to disable snapping. Multiplied by scale.
        unsigned int windowSnapDistance;
        
    public:
        CustomContext(ProcessManager & processManager);
        virtual ~CustomContext(void);
        
    private:
        // Handles internal event, returns true if the event should be
        // then passed to xcbMainLoopHandleEvent.
        bool handleEventInternal(xcb_generic_event_t * event, bool * repaintNeeded);
        
        void onCreateNotify(const xcb_create_notify_event_t & event);
        void onDestroyNotify(const xcb_destroy_notify_event_t & event);
        bool onWindowEvent(const xcb_window_t window, xcb_generic_event_t * event, bool * repaintNeeded);
        
        // Adjusts the specified rectangle, to make sure its top-left corner is
        // visible on one of the active CRTCs.
        void clipWindowPlacement(rascUI::Rectangle & rectangle);
        
        // Switch workspace, left, right, to a specific one.
        void switchWorkspaceLeft(void);
        void switchWorkspaceRight(void);
        void switchWorkspace(const unsigned int newWorkspace);
        
        // Methods to show child windows and create them if needed.
        void showRunWindow(void);
        void showSwitchWindow(void);
        
    protected:
        virtual void xcbOnMainLoopDraw(void) override;
        
    public:
        /**
         * Here we additionally check if we've managed to register for
         * substructure redirect on the root window: I.e: no other window
         * manager is present.
         */
        virtual bool wasSuccessful(void) const override;
    
        /**
         * Custom main loop logic, extended from rascUIxcb::Context to include
         * the additional stuff for window management.
         */
        virtual unsigned long long mainLoop(void) override;
        
        /**
         * Here we allow the theme to set a root cursor.
         */
        virtual void setRootCursor(xcb_cursor_t rootCursor) override;
        
        /**
         * Allow access from outside, for custom window to allow move/resize.
         */
        MoveResizeState & getMoveResizeState(void);
        
        /**
         * Allow access from outside, for things like the run window.
         */
        ProcessManager & getProcessManager(void);
        
        /**
         * Focus a window (if not in focus follows mouse mode). For reparented
         * windows, specified window must be the child!
         * Public so popups (generated by US) can steal focus.
         */
        void focusWindow(const xcb_window_t childWindowId) const;
        
        /**
         * Send the specified child window to the top of the specified bucket.
         * Public so popups (generated by US) can steal focus.
         */
        void sendToTopOfBucket(const xcb_window_t childWindowId, const Buckets bucket);

        /**
         * Searches windows by parent id, to find the CustomWindow.
         */
        CustomWindow * getWindowByParent(const xcb_window_t parentId) const;
        
        /**
         * Searches windows by child id, to find the CustomWindow.
         */
        CustomWindow * getWindowByChild(const xcb_window_t childId);
        
        /**
         * Gets the bucket associated with the specified placeholder window id,
         * or INVALID.
         */
        Buckets getBucketByPlaceholderWindowId(const xcb_window_t windowId) const;
        
        /**
         * Gets workspace count set by config.
         */
        unsigned int getWorkspaceCount(void) const;
        
        /**
         * Gets scaled window snap distance.
         */
        int getWindowSnapDistance(void) const;
        
        /**
         * Get a location representing the parent window (window decoration)
         * relative to the child window, to work out the required dimensions of
         * the parent window. Used for initial window dimensions, and when
         * a resize of the child window is requested.
         */
        rascUI::Location getInverseWindowLocation(void) const;
    };
}

#endif
