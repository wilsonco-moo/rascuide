/*
 * CustomWindow.cpp
 *
 *  Created on: 14 Nov 2021
 *      Author: wilson
 */

#include "CustomWindow.h"

// Before including Layout.h, define normal width/height as the ones for window
// decorations.
#define UI_BWIDTH (LAYOUT_THEME)->windowDecorationNormalComponentWidth
#define UI_BHEIGHT (LAYOUT_THEME)->windowDecorationNormalComponentHeight

#include <rascUI/platform/WindowConfig.h>
#include <rascUIxcb/util/Misc.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <xcb/xcb_util.h>
#include <algorithm>
#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <limits>

#include "utils/MoveResizeState.h"
#include "CustomContext.h"

#define LAYOUT_THEME theme

#define WDEC_BORDER (LAYOUT_THEME)->windowDecorationUiBorder

#define INTERIOR_LOC        \
    SUB_MARGIN(WDEC_BORDER, \
     GEN_FILL               \
    )

#define TOP_PANEL_LOC \
    PIN_NORMAL_T(     \
     INTERIOR_LOC     \
    )

#define FRONT_PANEL_LOC           \
    SUB_BORDERMARGIN_R(UI_BWIDTH, \
     TOP_PANEL_LOC                \
    )

#define CLOSE_BUTTON_LOC \
    PIN_NORMAL_R(        \
     TOP_PANEL_LOC       \
    )

#define CHILD_WINDOW_LOC           \
    SUB_BORDERMARGIN_T(UI_BHEIGHT, \
     INTERIOR_LOC                  \
    )

namespace rascUIwm {

    CustomWindow::ChildWindow::ChildWindow(rascUI::Theme * theme, const CustomContext & context, const xcb_window_t window) :
        rascUI::Component(rascUI::Location(CHILD_WINDOW_LOC)),
        window(window) {

        // We want to enable property change event on the child window. But if
        // this child window happens to be a window managed by ourself (by
        // rascUIxcb), we don't want to erase the base event mask! So OR that in
        // to honour it.
        uint32_t eventMask = XCB_EVENT_MASK_PROPERTY_CHANGE;
        if (context.xcbGetWindowById(window) != NULL) {
            eventMask |= rascUIxcb::Window::xcbGetBaseEventMask();
        }
        
        // Make sure override redirect is disabled on the child window.
        // Otherwise substructure redirect is blocked, we're not told when the
        // client wants to map it, and we can't reparent it properly. Allows
        // forcing a window border even when the client doesn't want it.
        // Also, subscribe to property change events on the child window too, so
        // we can see when atoms (like window title) are changed.
        const uint32_t values[] = {
            0, // Override redirect
            eventMask // Event mask
        };
        xcb_change_window_attributes(context.xcbGetConnection(), window, XCB_CW_OVERRIDE_REDIRECT |  XCB_CW_EVENT_MASK, values);
    }
    
    CustomWindow::ChildWindow::~ChildWindow(void) {
    }
    
    void CustomWindow::ChildWindow::onDraw(void) {
    }
    
    bool CustomWindow::ChildWindow::shouldRespondToKeyboard(void) const {
        return false;
    }
    
    void CustomWindow::ChildWindow::recalculate(const rascUI::Rectangle & parentSize) {
        rascUI::Component::recalculate(parentSize);
        CustomWindow & parentWindow = (CustomWindow &)*getTopLevel();
        CustomContext & context = (CustomContext &)*parentWindow.getContext();
        
        // We'll be recalculated whenever the parent is resized. So convert our
        // rascUI location to xcb positions/sizes, and resize the child window.
        // This way the child window acts like a rascUI component.
        const uint32_t sizeValues[] = {
            rascUIxcb::Misc::clampToInt16(location.cache.x), rascUIxcb::Misc::clampToInt16(location.cache.y),
            rascUIxcb::Misc::clampToSize16(location.cache.width), rascUIxcb::Misc::clampToSize16(location.cache.height),
            0 // Border width
        };
        
        xcb_configure_window(context.xcbGetConnection(), window, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT | XCB_CONFIG_WINDOW_BORDER_WIDTH, sizeValues);
    }
    
    void CustomWindow::ChildWindow::onClientRequestMap(void) const {
        CustomWindow & parentWindow = (CustomWindow &)*getTopLevel();
        CustomContext & context = (CustomContext &)*parentWindow.getContext();
        
        // Reparent the child window: Attach it to the frame.
        // Do this only once the window is requested to be mapped, otherwise
        // some clients (notably gtk right-click menus), get confused about
        // the window's parent and give up (don't show the right click menu).
        xcb_reparent_window(context.xcbGetConnection(), window, parentWindow.xcbGetWindow(), location.cache.x, location.cache.y);
    }
    
    const CustomWindow::WindowDecorationBackPanel::ResizeData CustomWindow::WindowDecorationBackPanel::resizeData[ResizeDirection::COUNT] = {
        { rascUI::CursorType::inherit, ResizeMode::none, ResizeMode::none }, // none
        { rascUI::CursorType::moveRight, ResizeMode::forward, ResizeMode::none }, // right
        { rascUI::CursorType::moveUpRight, ResizeMode::forward, ResizeMode::backward }, // topRight
        { rascUI::CursorType::moveUp, ResizeMode::none, ResizeMode::backward }, // top
        { rascUI::CursorType::moveUpLeft, ResizeMode::backward, ResizeMode::backward }, // topLeft
        { rascUI::CursorType::moveLeft, ResizeMode::backward, ResizeMode::none }, // left
        { rascUI::CursorType::moveDownLeft, ResizeMode::backward, ResizeMode::forward }, // bottomLeft
        { rascUI::CursorType::moveDown, ResizeMode::none, ResizeMode::forward }, // bottom
        { rascUI::CursorType::moveDownRight, ResizeMode::forward, ResizeMode::forward } // bottomRight
    };
    CustomWindow::WindowDecorationBackPanel::WindowDecorationBackPanel() :
        rascUI::BackPanel(),
        currentDirection(ResizeDirection::none) {
    }
    CustomWindow::WindowDecorationBackPanel::~WindowDecorationBackPanel(void) {
    }
    unsigned int CustomWindow::WindowDecorationBackPanel::getResizeDirection(GLfloat viewX, GLfloat viewY) const {
        const rascUI::Theme * const theme = getTopLevel()->theme;
        
        // Ignore if outside boundary.
        rascUI::Rectangle boundary = location.cache;
        const float borderX = WDEC_BORDER * location.xScale,
            borderY = WDEC_BORDER * location.yScale;
        boundary.x += borderX;
        boundary.y += borderY;
        boundary.width -= (borderX * 2.0f);
        boundary.height -= (borderY * 2.0f);
        if (boundary.contains(viewX, viewY)) {
            return ResizeDirection::none;
        }
        
        // Else, find which area the mouse is in.
        const float bWidth = UI_BWIDTH * location.xScale,
            bHeight = UI_BHEIGHT * location.yScale;
        if (viewY < location.cache.y + bHeight) {
            // Top area
            if (viewX < location.cache.x + bWidth) return ResizeDirection::topLeft;
            if (viewX < location.cache.x + location.cache.width - bWidth) return ResizeDirection::top;
            return ResizeDirection::topRight;
        } else if (viewY < location.cache.y + location.cache.height - bHeight) {
            // Middle area
            if (viewX < location.cache.x + bWidth) return ResizeDirection::left;
            return ResizeDirection::right;
        } else {
            // Bottom area
            if (viewX < location.cache.x + bWidth) return ResizeDirection::bottomLeft;
            if (viewX < location.cache.x + location.cache.width - bWidth) return ResizeDirection::bottom;
            return ResizeDirection::bottomRight;
        }
    }
    void CustomWindow::WindowDecorationBackPanel::updateResizeDirection(GLfloat viewX, GLfloat viewY) {
        const unsigned int newDirection = getResizeDirection(viewX, viewY);
        if (newDirection != currentDirection) {
            currentDirection = newDirection;
            getTopLevel()->forceEmptyRepaint();
        }
    }
    void CustomWindow::WindowDecorationBackPanel::onDraw(void) {
        getTopLevel()->theme->drawWindowDecorationBackPanel(location);
    }
    void CustomWindow::WindowDecorationBackPanel::onMouseEnter(GLfloat viewX, GLfloat viewY) {
        updateResizeDirection(viewX, viewY);
    }
    void CustomWindow::WindowDecorationBackPanel::onMouseMove(GLfloat viewX, GLfloat viewY) {
        updateResizeDirection(viewX, viewY);
    }
    void CustomWindow::WindowDecorationBackPanel::onMousePress(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getMb()) {
            updateResizeDirection(viewX, viewY);
            CustomWindow & parentWindow = (CustomWindow &)*getTopLevel();
            CustomContext & context = (CustomContext &)*parentWindow.getContext();
            context.getMoveResizeState().start(viewX, viewY, parentWindow.xcbGetWindow(), false, resizeData[currentDirection].resizeModeX, resizeData[currentDirection].resizeModeY);
        }
    }
    void CustomWindow::WindowDecorationBackPanel::onMouseLeave(GLfloat viewX, GLfloat viewY) {
        if (currentDirection != ResizeDirection::none) {
            currentDirection = ResizeDirection::none;
            getTopLevel()->forceEmptyRepaint();
        }
    }
    rascUI::CursorType CustomWindow::WindowDecorationBackPanel::getCursorType(void) const {
        return resizeData[currentDirection].cursorType;
    }

    CustomWindow::WindowDecorationFrontPanelLabel::WindowDecorationFrontPanelLabel(const rascUI::Location & location) :
        rascUI::FrontPanel(location),
        rascUI::TextObject("Title") {
    }
    CustomWindow::WindowDecorationFrontPanelLabel::~WindowDecorationFrontPanelLabel(void) {
    }
    void CustomWindow::WindowDecorationFrontPanelLabel::onDraw(void) {
        getTopLevel()->theme->drawWindowDecorationFrontPanel(location);
        getTopLevel()->theme->drawWindowDecorationText(location, getText());
    }
    void CustomWindow::WindowDecorationFrontPanelLabel::onMousePress(GLfloat viewX, GLfloat viewY, int button) {
        // Start moving the window on mouse press: The rest (motion/release) is
        // taken care of by CustomContext.
        if (button == getMb()) {
            CustomWindow & parentWindow = (CustomWindow &)*getTopLevel();
            CustomContext & context = (CustomContext &)*parentWindow.getContext();
            context.getMoveResizeState().start(viewX, viewY, parentWindow.xcbGetWindow(), true, ResizeMode::none, ResizeMode::none);
        }
    }
    void CustomWindow::WindowDecorationFrontPanelLabel::onMouseEnter(GLfloat viewX, GLfloat viewY) {
        // Force an empty repaint on enter/exit, to allow cursor change.
        getTopLevel()->forceEmptyRepaint();
    }
    void CustomWindow::WindowDecorationFrontPanelLabel::onMouseLeave(GLfloat viewX, GLfloat viewY) {
        getTopLevel()->forceEmptyRepaint();
    }
    rascUI::CursorType CustomWindow::WindowDecorationFrontPanelLabel::getCursorType(void) const {
        return rascUI::CursorType::move;
    }
    
    CustomWindow::WindowDecorationButton::WindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) :
        rascUI::ResponsiveComponent(location),
        type(type) {
    }
    CustomWindow::WindowDecorationButton::~WindowDecorationButton(void) {
    }
    void CustomWindow::WindowDecorationButton::onDraw(void) {
        getTopLevel()->theme->drawWindowDecorationButton(location, type);
    }
    void CustomWindow::WindowDecorationButton::onMouseClick(GLfloat viewX, GLfloat viewY, int button) {
        if (button == getMb()) {
            CustomWindow & parentWindow = (CustomWindow &)*getTopLevel();
            CustomContext & context = (CustomContext &)*parentWindow.getContext();
            
            // Do appropriate action depending on type of window decoration button.
            switch(type) {
                case rascUI::WindowDecorationButtonType::close: {
                    // Don't actually need to do this until close button is
                    // pressed!
                    parentWindow.receiveWmProtocols();
                    
                    if (parentWindow.hasCloseBehaviour) {
                        // If they've got close behaviour set up, send them
                        // the relevent event.
                        xcb_client_message_event_t messageEvent;
                        messageEvent.response_type = XCB_CLIENT_MESSAGE;
                        messageEvent.format = 32; // Atoms are 32-bit
                        messageEvent.window = parentWindow.child.window;
                        messageEvent.type = context.xcbGetAtomWmProtocols().getAtom();
                        messageEvent.data.data32[0] = context.xcbGetAtomWmDeleteWindow().getAtom();
                        messageEvent.data.data32[1] = XCB_TIME_CURRENT_TIME;
                        xcb_send_event(context.xcbGetConnection(), false, parentWindow.child.window, 0, (const char *)&messageEvent);
                    } else {
                        // Else just kill their connection to the X server.
                        xcb_kill_client(context.xcbGetConnection(), parentWindow.child.window);
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    CustomWindow::CustomWindow(CustomContext * context, const rascUI::WindowConfig & config, const xcb_create_notify_event_t & createEvent) :
        rascUIxcb::Window(context, config),
        backPanel(),
        frontPanel(rascUI::Location(FRONT_PANEL_LOC)),
        closeButton(rascUI::Location(CLOSE_BUTTON_LOC), rascUI::WindowDecorationButtonType::close),
        child(theme, *context, createEvent.window),
        currentWindowWorkspace(0),
        wmNameCookie({ 0 }), // Note: Xcb uses 0 for invalid cookies, see xcb source (xcb_in.c / xcb_out.c).
        wmProtocolsCookie({ 0 }),
        currentBucket(Buckets::normal),
        isPopup(createEvent.override_redirect), // Note: Take note of popup flag, if client tried to create a window with override redirect set.
        hasCloseBehaviour(false),
        isWmShown(true),
        clientWantsUsMapped(false) {
        
        // Enable substructure notify/redirect on the parent window, so we get
        // notifications for operations on the child window. Also include base
        // event mask.
        const uint32_t mask = xcbGetBaseEventMask() | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT;
        xcb_change_window_attributes(context->xcbGetConnection(), xcbGetWindow(), XCB_CW_EVENT_MASK, &mask);
        
        // Always initially request properties: Client could've set them before
        // we managed to start listening for property notify events.
        requestWindowTitle();
        requestWmProtocols();
        
        // Always send the parent to the top of its default bucket.
        sendToTop();
        
        std::cout << "Created: Parent: " << xcbGetWindow() << " (" << config.screenX << ',' << config.screenY << ',' << config.screenWidth << ',' << config.screenHeight << "), child: " << child.window << " (" << createEvent.x << ',' << createEvent.y << ',' << createEvent.width << ',' << createEvent.height << ").\n";
        
        // Add the various components.
        add(&backPanel);
        add(&frontPanel);
        add(&closeButton);
        add(&child);
        
        // Add ourself to lists in the context.
        // Add the parent window to the list of excluded windows, or we'll
        // try to create a frame for the frame!
        context->excludedWindows.insert(xcbGetWindow());
        context->windowsByParent.emplace(xcbGetWindow(), this);
    }
    
    CustomWindow::~CustomWindow(void) {
        CustomContext & context = (CustomContext &)*getContext();
        
        // When destroying, remove ourself from the lists in the context.
        context.windowsByParent.erase(xcbGetWindow());
        
        // Discard cookies if we've not read them yet, (else the reply won't
        // ever get freed by xcb when it shows up).
        if (wmNameCookie.sequence != 0) {
            xcb_discard_reply(context.xcbGetConnection(), wmNameCookie.sequence);
        }
        if (wmProtocolsCookie.sequence != 0) {
            xcb_discard_reply(context.xcbGetConnection(), wmProtocolsCookie.sequence);
        }
    }
    
    void CustomWindow::handleConfigureRequest(const xcb_configure_request_event_t & event) {
        // Only allow the client to configure its windows if they're NOT mapped,
        // avoid clients causing chaos by moving their windows around or jumping
        // them to the front!
        if (isMapped()) {
            std::cout << "Blocking configure request for mapped window " << (unsigned int)child.window << ".\n";
        } else {
            CustomContext & context = (CustomContext &)*getContext();
            std::cout << "Allowing configure for " << (unsigned int)child.window << ".\n";
            
            // Can have up to 6 values, we'll add values and mask bits as we go.
            uint32_t values[6];
            size_t valueUpto = 0;
            uint16_t mask = 0;
            
            // Take a copy of the parent's geometry area, for the configure
            // notify later on, (since we know this already, don't need to query
            // for geometry).
            // We'll update this below if the client has requested a move/resize.
            rascUI::Rectangle parentGeometryArea = xcbGetWindowGeometryArea();
            
            // If modifying any of position or size:
            if ((event.value_mask & (XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT)) != 0) {
                
                // Find the child window's current dimensions from parent's
                // current geometry area. Use this as a starting point.
                rascUI::Location childWindowLocation = child.location;
                childWindowLocation.fitCacheInto(parentGeometryArea, *context.defaultXScalePtr, *context.defaultYScalePtr);
                rascUI::Rectangle & childWindowDimensions = childWindowLocation.cache;
                
                // Replace in each value specified by the client: Now we've got
                // the full intended new dimensions for the child window.
                if (event.value_mask & XCB_CONFIG_WINDOW_X) childWindowDimensions.x = event.x;
                if (event.value_mask & XCB_CONFIG_WINDOW_Y) childWindowDimensions.y = event.y;
                if (event.value_mask & XCB_CONFIG_WINDOW_WIDTH) childWindowDimensions.width = event.width;
                if (event.value_mask & XCB_CONFIG_WINDOW_HEIGHT) childWindowDimensions.height = event.height;
                
                // Apply "inverse" window location to the specified size, to
                // work out the full intended dimensions of the parent window.
                // Set parent geometry area to this.
                {
                    rascUI::Location inverseWindowLocation = context.getInverseWindowLocation();
                    inverseWindowLocation.fitCacheInto(childWindowDimensions, *context.defaultXScalePtr, *context.defaultYScalePtr);
                    parentGeometryArea = inverseWindowLocation.cache;
                }
                
                if (context.autoPlaceNewWindows) {
                    // According to config, we're supposed to auto-place windows. So even
                    // when client is trying to configure the window, ignore client's requested
                    // position and get rascUIxcb to re-place it.
                    const rascUIxcb::ActiveCrtc * const activeCrtc = context.xcbGetScreenConfig().getActiveCrtcUnderPointer();
                    if (activeCrtc != NULL) {
                        xcb_rectangle_t area = rascUIxcb::Misc::rascUIToXcbRect(parentGeometryArea);
                        activeCrtc->place(area);
                        parentGeometryArea = rascUIxcb::Misc::xcbToRascUIRect(area);
                    }
                    
                } else {
                    if (isPopup) {
                        // If we're a popup window, keep the original x and y (even if
                        // below zero). If we offset it back (like for normal windows),
                        // tooltips (etc) end up higher up than they should be, and lots
                        // of things break.
                        parentGeometryArea.x = event.x;
                        parentGeometryArea.y = event.y;

                    } else {
                        // Otherwise, we've got to try to honour the position and
                        // size requested by the client. We still don't want to
                        // allow clients to do stupid placement, so clip it.
                        context.clipWindowPlacement(parentGeometryArea);
                    }
                }
                
                // Clamp and store each into values. Set all in the mask (simplifies things).
                mask |= (XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT);
                values[valueUpto++] = rascUIxcb::Misc::clampToInt16(parentGeometryArea.x);
                values[valueUpto++] = rascUIxcb::Misc::clampToInt16(parentGeometryArea.y);
                values[valueUpto++] = rascUIxcb::Misc::clampToSize16(parentGeometryArea.width);
                values[valueUpto++] = rascUIxcb::Misc::clampToSize16(parentGeometryArea.height);
            }
            
            // Only allow stacking configuration if they're setting stack mode AND sibling.
            if ((event.value_mask | (XCB_CONFIG_WINDOW_SIBLING | XCB_CONFIG_WINDOW_STACK_MODE)) == event.value_mask) {
                // Always translate sibling to parent window (if possible).
                xcb_window_t translatedSibling;
                const std::unordered_map<xcb_window_t, CustomWindow>::const_iterator siblingIter = context.windows.find(event.sibling);
                if (siblingIter == context.windows.end()) {
                    translatedSibling = event.sibling;
                } else {
                    translatedSibling = siblingIter->second.xcbGetWindow();
                }
            
                // Work out whether window stacking is allowed.
                // Only if sibling is a window we're managing.
                bool allowWindowStacking = false;
                CustomWindow * const siblingWindow = context.getWindowByParent(translatedSibling);
                if (siblingWindow != NULL) {
                    // And only if it is in normal or above buckets.
                    const Buckets siblingBucket = siblingWindow->currentBucket;
                    if (siblingBucket == Buckets::normal || siblingBucket == Buckets::above) {
                        // Allow stacking, update our bucket to the same as sibling.
                        allowWindowStacking = true;
                        currentBucket = siblingBucket;
                    }
                }
                
                // Set sibling and stack mode if appropriate.
                if (allowWindowStacking) {
                    if (event.value_mask & XCB_CONFIG_WINDOW_SIBLING) {
                        mask |= XCB_CONFIG_WINDOW_SIBLING;
                        values[valueUpto++] = translatedSibling;
                    }
                    if (event.value_mask & XCB_CONFIG_WINDOW_STACK_MODE) {
                        mask |= XCB_CONFIG_WINDOW_STACK_MODE;
                        values[valueUpto++] = event.stack_mode;
                    }
                }
            }
            
            // Apply configuration to the PARENT window.
            xcb_configure_window(context.xcbGetConnection(), xcbGetWindow(), mask, values);
            
            // Now, while unmapped, we've configured the parent window not the
            // child. We won't configure the child until we repaint, (so until
            // we're mapped). But according to ICCCM, we've always got to send
            // child a configure notify - so build a synthetic one. Otherwise
            // programs like xterm block waiting for it, even though their
            // window isn't mapped yet!
            
            // Since we know the parent's geometry area, work out the intended
            // child window area (which will happen once we are mapped and we
            // recalculate).
            rascUI::Location childWindowLocation = child.location;
            childWindowLocation.fitCacheInto(parentGeometryArea, *context.defaultXScalePtr, *context.defaultYScalePtr);
            
            // And use it to build a configure notify. Note: Sibling is always
            // none, since we only allow configure when not mapped, (and this
            // would be a pain to work out anyway).
            xcb_configure_notify_event_t notifyEvent;
            notifyEvent.response_type = XCB_CONFIGURE_NOTIFY;
            notifyEvent.event = child.window;
            notifyEvent.window = child.window;
            notifyEvent.above_sibling = XCB_WINDOW_NONE;
            notifyEvent.x = rascUIxcb::Misc::clampToPureInt16(childWindowLocation.cache.x);
            notifyEvent.y = rascUIxcb::Misc::clampToPureInt16(childWindowLocation.cache.y);
            notifyEvent.width = rascUIxcb::Misc::clampToPureSize16(childWindowLocation.cache.width);
            notifyEvent.height = rascUIxcb::Misc::clampToPureSize16(childWindowLocation.cache.height);
            notifyEvent.border_width = 0;
            notifyEvent.override_redirect = 0;
            xcb_send_event(context.xcbGetConnection(), false, child.window, 0, (const char *)&notifyEvent);
        }
    }
    
    void CustomWindow::handlePropertyNotify(const xcb_property_notify_event_t & event) {
        CustomContext & context = (CustomContext &)*getContext();
        
        // If window title property has changed:
        if (event.atom == XCB_ATOM_WM_NAME) {
            // Always request window title.
            requestWindowTitle();
            // If already mapped, immediately receive window title. Else, we'll
            // do that once the window gets mapped to avoid waiting for reply
            // just now, (won't show the title until the window is mapped).
            if (isMapped()) {
                receiveWindowTitle();
            }
        // Re-request wm protocols whenever they change.
        } else if (event.atom == context.xcbGetAtomWmProtocols().getAtom()) {
            requestWmProtocols();
        }
    }
    
    void CustomWindow::requestWindowTitle(void) {
        CustomContext & context = (CustomContext &)*getContext();
        
        // Discard any previous requests for window title (else the reply won't
        // ever get freed by xcb).
        if (wmNameCookie.sequence != 0) {
            xcb_discard_reply(context.xcbGetConnection(), wmNameCookie.sequence);
        }
        
        // Send off request for window title. Last parameter seems to be max
        // length of data to be received (times 4 bytes). Set to reasonable
        // value.
        wmNameCookie = xcb_get_property(context.xcbGetConnection(), false, child.window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 0, 256);
    }
    
    void CustomWindow::receiveWindowTitle(void) {
        // If there's a pending property request, grab the reply (else there's
        // nothing to do).
        if (wmNameCookie.sequence != 0) {
            xcb_get_property_reply_t * const propertyReply = xcb_get_property_reply(((CustomContext *)getContext())->xcbGetConnection(), wmNameCookie, NULL);
            if (propertyReply != NULL) {
                // Read reply (string data) using the value/length functions,
                // repaint the panel and free the reply.
                frontPanel.setText((const char *)xcb_get_property_value(propertyReply), xcb_get_property_value_length(propertyReply));
                frontPanel.repaint();
                free(propertyReply);
            }
            
            // Set to zero to show that there's no pending request now.
            wmNameCookie.sequence = 0;
        }
    }
    
    void CustomWindow::requestWmProtocols(void) {
        CustomContext & context = (CustomContext &)*getContext();
        
        // Discard any previous requests.
        if (wmProtocolsCookie.sequence != 0) {
            xcb_discard_reply(context.xcbGetConnection(), wmProtocolsCookie.sequence);
        }
        
        // Send off a request for atoms associated with WM_PROTOCOLS. Must pick
        // a maximum number of things to return, so just use uint32_t max for
        // lack of any better ideas.
        wmProtocolsCookie = xcb_get_property(context.xcbGetConnection(), false, child.window, context.xcbGetAtomWmProtocols().getAtom(), XCB_ATOM_ATOM, 0, std::numeric_limits<uint32_t>::max());
    }
    
    void CustomWindow::receiveWmProtocols(void) {
        if (wmProtocolsCookie.sequence != 0) {
            CustomContext & context = (CustomContext &)*getContext();
            xcb_get_property_reply_t * const propertyReply = xcb_get_property_reply(context.xcbGetConnection(), wmProtocolsCookie, NULL);
            if (propertyReply != NULL) {
                // Grab list of atoms and get length.
                const xcb_atom_t * const atoms = (const xcb_atom_t *)xcb_get_property_value(propertyReply);
                const int count = xcb_get_property_value_length(propertyReply) / sizeof(xcb_atom_t);
                
                // Search for WM_DELETE_WINDOW atom, set flag if window has it.
                const xcb_atom_t wmDeleteWindow = context.xcbGetAtomWmDeleteWindow().getAtom();
                hasCloseBehaviour = false;
                for (int index = 0; index < count; index++) {
                    if (atoms[index] == wmDeleteWindow) {
                        hasCloseBehaviour = true;
                        break;
                    }
                }
                
                free(propertyReply);
            }
            wmProtocolsCookie.sequence = 0;
        }
    }
    
    void CustomWindow::onMapWindow(void) {
        // When we're mapped, receive any pending replies for the window title
        // (since now we need to know it).
        receiveWindowTitle();
        
        // Call parent rascUIxcb method then map the child too.
        rascUIxcb::Window::onMapWindow();
        xcb_map_window(((CustomContext *)getContext())->xcbGetConnection(), child.window);
    }
    
    bool CustomWindow::handleWindowEvent(xcb_generic_event_t * event, bool * repaintNeeded) {
        switch(XCB_EVENT_RESPONSE_TYPE(event)) {
            case XCB_CONFIGURE_REQUEST: {
                handleConfigureRequest((xcb_configure_request_event_t &)*event);
                break;
            }
            case XCB_MAP_REQUEST: {
                std::cout << "Map " << (unsigned int)child.window << "\n";
                // If changed, set flag and update map status.
                if (!clientWantsUsMapped) {
                    CustomContext & context = (CustomContext &)*getContext();
                    // Set ourself to current workspace when client requests us
                    // to be mapped.
                    currentWindowWorkspace = context.currentWorkspace;
                    // Also notify child that client has requested a map (so
                    // it can do reparenting).
                    child.onClientRequestMap();
                    clientWantsUsMapped = true;
                    updateMapStatus();
                }
                break;
            }
            case XCB_UNMAP_NOTIFY: {
                std::cout << "Unmap " << (unsigned int)child.window << "\n";
                // If changed, set flag and update map status.
                if (clientWantsUsMapped) {
                    clientWantsUsMapped = false;
                    updateMapStatus();
                }
                break;
            }
            case XCB_PROPERTY_NOTIFY: {
                handlePropertyNotify((xcb_property_notify_event_t &)*event);
                break;
            }
        }
        return true;
    }
    
    xcb_window_t CustomWindow::getChildWindow(void) const {
        return child.window;
    }
    
    void CustomWindow::sendToTopOfBucket(const Buckets bucket) {
        currentBucket = bucket;
        sendToTop();
    }

    void CustomWindow::sendToTop(void) const {
        // Find our current bucket, send us to the top of it.
        CustomContext & context = (CustomContext &)*getContext();
        context.buckets[(size_t)currentBucket].sendToTop(xcbGetWindow());
    }
    
    void CustomWindow::setWorkspace(const unsigned int newWorkspace) {
        if (newWorkspace != currentWindowWorkspace) {
            std::cout << "Moving window (parent: " << xcbGetWindow() << ", child: " << child.window << ") to workspace " << newWorkspace << ".\n";
            currentWindowWorkspace = newWorkspace;
            updateMapStatus();
        }
    }

    void CustomWindow::setWmShown(const bool newIsWmShown) {
        if (newIsWmShown != isWmShown) {
            isWmShown = newIsWmShown;
            updateMapStatus();
        }
    }

    void CustomWindow::updateMapStatus(void) {
        const CustomContext & context = (CustomContext &)*getContext();
        setMapped(isWmShown && clientWantsUsMapped && currentWindowWorkspace == context.currentWorkspace);
    }
    
    bool CustomWindow::isClientMappedOnCurrentWorkspace(void) const {
        const CustomContext & context = (CustomContext &)*getContext();
        return clientWantsUsMapped && currentWindowWorkspace == context.currentWorkspace;
    }
    
    const std::string & CustomWindow::getTitle(void) {
        // Wait for any pending replies for title, then get front panel text.
        receiveWindowTitle();
        return frontPanel.getText();
    }
}
