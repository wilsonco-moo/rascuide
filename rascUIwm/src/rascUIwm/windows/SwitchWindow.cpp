/*
 * SwitchWindow.cpp
 *
 *  Created on: 1 Sep 2024
 *      Author: wilson
 */

#include "SwitchWindow.h"

#include <rascUI/platform/WindowConfig.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <cstdlib>
#include <cstddef>
#include <vector>

#include "../CustomContext.h"

#define LAYOUT_THEME theme

#define SWITCH_BUTTON_LOC \
    SUB_BORDER_T( \
     SUB_BORDER_X( \
      GEN_FILL \
    ))

#define SCROLL_PANE_LOC \
    SUB_BORDER_R( \
     GEN_FILL \
    )

namespace rascUIwm {

    namespace switchWindowHelpers {
        static rascUI::WindowConfig getWindowConfig(CustomContext & context, rascUI::Theme * theme) {
            rascUI::WindowConfig config;
            config.screenWidth = 256;
            config.screenHeight = 256;
            config.title = "rascUIwm switch";
            return config;
        }
    }
    
    SwitchWindow::SwitchToggleButton::SwitchToggleButton(rascUI::Theme * theme, rascUI::ToggleButtonSeries & series, unsigned long toggleButtonId) :
        rascUI::ToggleButton(&series, toggleButtonId, rascUI::Location(SWITCH_BUTTON_LOC)) {
    }
    
    SwitchWindow::SwitchToggleButton::~SwitchToggleButton(void) {
    }
    
    void SwitchWindow::SwitchToggleButton::setWindow(CustomWindow & window, const int windowIndex) {
        windowId = window.xcbGetWindow();
        this->windowIndex = windowIndex;
        setText(window.getTitle());
    }
    
    void SwitchWindow::SwitchToggleButton::pushForward(const CustomContext & context) {
        // Get window, if it still exists, just send it to the top of its
        // bucket.
        CustomWindow * const customWindow = context.getWindowByParent(windowId);
        if (customWindow != NULL) {
            customWindow->sendToTop();
        }
    }
    
    void SwitchWindow::SwitchToggleButton::returnToPlace(const CustomContext & context, const xcb_query_tree_reply_t * const windowListReply) {
        // Get window, if it still exists:
        CustomWindow * const customWindow = context.getWindowByParent(windowId);
        if (customWindow != NULL) {
            // Get the window list from the specified reply.
            const xcb_window_t * const windowList = xcb_query_tree_children(windowListReply);
            const int windowLength = xcb_query_tree_children_length(windowListReply);
            
            // Iterate upwards, starting at our original sibling index, until
            // we find a window which still exists (can be a bucket
            // placeholder, those won't be destroyed). If we don't find a
            // suitable sibling, just do nothing.
            // (Note: Xcb window list is bottom-to-top!).
            for (int index = windowIndex + 1; index < windowLength; index++) {
                const xcb_window_t siblingId = windowList[index];
                if (context.getWindowByParent(siblingId) != NULL || context.getBucketByPlaceholderWindowId(siblingId) != Buckets::INVALID) {
                    // Return the window to below its original sibling.
                    const uint32_t values[] = {
                        siblingId,
                        XCB_STACK_MODE_BELOW
                    };
                    xcb_configure_window(context.xcbGetConnection(), windowId, XCB_CONFIG_WINDOW_SIBLING | XCB_CONFIG_WINDOW_STACK_MODE, values);
                    break;
                }
            }
        }
    }
    
    xcb_window_t SwitchWindow::SwitchToggleButton::getWindowId(void) const {
        return windowId;
    }

    SwitchWindow::SwitchSeries::SwitchSeries(SwitchWindow & switchWindow) :
        rascUI::ToggleButtonSeries(false),
        switchWindow(switchWindow) {
    }
    
    SwitchWindow::SwitchSeries::~SwitchSeries(void) {
    }
    
    void SwitchWindow::SwitchSeries::onChangeSelection(unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
        CustomContext & context = (CustomContext &)*switchWindow.getContext();
        
        // Send last window back to where it is supposed to be.
        if (switchWindow.lastSelectedButton != NULL) {
            switchWindow.lastSelectedButton->returnToPlace(context, switchWindow.windowListReply);
        }
        // Store new button, and bring window forward.
        switchWindow.lastSelectedButton = (SwitchToggleButton *)toggleButton;
        switchWindow.lastSelectedButton->pushForward(context);
    }

    SwitchWindow::SwitchWindow(CustomContext & context, rascUI::Theme * theme) :
        PopupWindow(context, switchWindowHelpers::getWindowConfig(context, theme)),
        backPanel(),
        scrollPane(theme, rascUI::Location(SCROLL_PANE_LOC)),
        series(*this),
        windowListReply(NULL) {
        
        // No animation on scroll pane.
        scrollPane.contents.smoothMoveSpeed = 0.0f;

        add(&backPanel);
        add(&scrollPane);
    }
    
    SwitchWindow::~SwitchWindow(void) {
        // Delete any window list reply we had stored.
        free(windowListReply);
    }
    
    void SwitchWindow::resizeButtons(const size_t newSize) {
        if (newSize <= buttons.size()) {
            // Got to resize smaller or not at all: Remove from the end (cheaper
            // to remove from segment container), until size matches.
            while (buttons.size() > newSize) {
                scrollPane.contents.remove(&buttons.back());
                buttons.pop_back();
            }
        } else {
            // New size is bigger than container's max size: Remove all the
            // existing ones (back first again), then resize the container.
            if (newSize > buttons.max_size()) {
                while (!buttons.empty()) {
                    scrollPane.contents.remove(&buttons.back());
                    buttons.pop_back();
                }
                buttons.resize(newSize);
            }
            // Add new buttons until large enough.
            // Note: Always match toggle button IDs with their actual ID within
            // the array! Makes setting/getting selection way easier!
            while (buttons.size() < newSize) {
                buttons.emplace(theme, series, buttons.size());
                scrollPane.contents.add(&buttons.back());
            }
        }
    }
    
    void SwitchWindow::xcbOnMapNotify(void) {
        PopupWindow::xcbOnMapNotify();
        
        CustomContext & context = (CustomContext &)*getContext();
        
        // Send to top of the special switch-window bucket: Sneaky! Don't steal
        // focus: We wait for user to focus us with mouse as a cue to know
        // whether to unmap when alt is released.
        // Note: We're in our own bucket, so switching between other special
        // popup windows doesn't put them in front of us!
        context.sendToTopOfBucket(xcbGetWindow(), Buckets::switchWindow);
    }
    
    void SwitchWindow::onMapWindow(void) {
        PopupWindow::onMapWindow();
        CustomContext & context = (CustomContext &)*getContext();
        
        // Query for list of windows and input focus.
        const xcb_query_tree_cookie_t treeCookie = xcb_query_tree(context.xcbGetConnection(), context.xcbGetScreen()->root);        
        const xcb_get_input_focus_cookie_t inputFocusCookie = xcb_get_input_focus(context.xcbGetConnection());
        
        // Free any existing window list reply we had.
        free(windowListReply);
        
        // Find the list of windows we are going to show, store their IDs within
        // the window list. Unfortunately, we've got to wait for a round trip. 
        std::vector<std::pair<CustomWindow *, int>> filteredWindowList;
        windowListReply = xcb_query_tree_reply(context.xcbGetConnection(), treeCookie, NULL);
        if (windowListReply != NULL) {
            const xcb_window_t * const windowList = xcb_query_tree_children(windowListReply);
            const int windowLength = xcb_query_tree_children_length(windowListReply);
            
            // Iterate backwards: Tree uses bottom-to-top order.
            for (int index = windowLength - 1; index >= 0; index--) {
                const xcb_window_t window = windowList[index];
                
                // Search by parent for windows we're managing:
                // Excludes managed windows which we've not mapped yet, bucket
                // placeholders and unmanaged popups (override redirect etc). 
                // Exclude the alt+tab window, also only show windows where
                // CLIENT wants them to be mapped, and are on current workspace.
                // Note: Includes windows which are unmapped due to being
                // minimised!
                CustomWindow * const customWindow = context.getWindowByParent(window);
                if (customWindow != NULL &&
                    customWindow->getChildWindow() != xcbGetWindow() &&
                    customWindow->isClientMappedOnCurrentWorkspace()) {
                    filteredWindowList.emplace_back(customWindow, index);
                }
            }
        }
        
        // Get input focus too.
        xcb_window_t inputFocus = XCB_WINDOW_NONE;
        xcb_get_input_focus_reply_t * const inputFocusReply = xcb_get_input_focus_reply(context.xcbGetConnection(), inputFocusCookie, NULL);
        if (inputFocusReply != NULL) {
            inputFocus = inputFocusReply->focus;
            free(inputFocusReply);
        }
        
        // Clear last selected button from last time.
        lastSelectedButton = NULL;
        
        // Allocate the buttons.
        const size_t windowCount = filteredWindowList.size();
        resizeButtons(windowCount);
        
        // Update the buttons to show each window. Set selected for input focus.
        bool foundInputFocus = false;
        for (size_t index = 0; index < windowCount; index++) {
            const std::pair<CustomWindow *, int> & windowPair = filteredWindowList[index];
            buttons[index].setWindow(*windowPair.first, windowPair.second);
            if (windowPair.first->getChildWindow() == inputFocus) {
                series.setSelectedId(index, true);
                setKeyboardSelected(&buttons[index]);
                foundInputFocus = true;
            }
        }
        
        // If not found input focus, set the first one to be selected if
        // possible.
        if (!foundInputFocus && windowCount != 0) {
            series.setSelectedId(0, true);
            setKeyboardSelected(&buttons[0]);
        }
    }
    
    void SwitchWindow::onUnmapWindow(void) {
        PopupWindow::onUnmapWindow();
        
        // When we're done, focus the window which we ended up selecting (if
        // any, if it exists. Note that we must translate to child window id).
        if (lastSelectedButton != NULL) {
            CustomContext & context = (CustomContext &)*getContext();
            CustomWindow * const customWindow = context.getWindowByParent(lastSelectedButton->getWindowId());
            if (customWindow != NULL) {
                context.focusWindow(customWindow->getChildWindow());
            }
        }
    }
    
    void SwitchWindow::switchForward(void) {
        if (!buttons.empty()) {
            const unsigned long newSelectedId = (series.getSelectedId() + 1u) % buttons.size();
            series.setSelectedId(newSelectedId, true);
            setKeyboardSelected(&buttons[newSelectedId]);
        }
    }
    
    void SwitchWindow::switchBackward(void) {
        if (!buttons.empty()) {
            const unsigned long selectedId = series.getSelectedId();
            const unsigned long newSelectedId = selectedId == 0u ? (buttons.size() - 1u) : (selectedId - 1u);
            series.setSelectedId(newSelectedId, true);
            setKeyboardSelected(&buttons[newSelectedId]);
        }
    }
    
    void SwitchWindow::onAltRelease(void) {
        setMapped(false);
    }
}
