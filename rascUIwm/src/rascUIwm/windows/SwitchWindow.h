/*
 * SwitchWindow.h
 *
 *  Created on: 1 Sep 2024
 *      Author: wilson
 */

#ifndef RASCUIWM_WINDOWS_SWITCHWINDOW_H_
#define RASCUIWM_WINDOWS_SWITCHWINDOW_H_

#include "../utils/PopupWindow.h"

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/util/EmplaceVector.h>

namespace rascUI {
    class Theme;
}

namespace rascUIwm {
    class CustomContext;
    class CustomWindow;

    /**
     * Switch window is for alt+tab.
     */
    class SwitchWindow : public PopupWindow  {
    private:
        class SwitchToggleButton : public rascUI::ToggleButton {
        private:
            xcb_window_t windowId;
            int windowIndex;
        public:
            SwitchToggleButton(rascUI::Theme * theme, rascUI::ToggleButtonSeries & series, unsigned long toggleButtonId);
            virtual ~SwitchToggleButton(void);
            
            void setWindow(CustomWindow & window, const int windowIndex);
            void pushForward(const CustomContext & context);
            void returnToPlace(const CustomContext & context, const xcb_query_tree_reply_t * const windowListReply);
            xcb_window_t getWindowId(void) const;
        };
        
        class SwitchSeries : public rascUI::ToggleButtonSeries {
        private:
            SwitchWindow & switchWindow;
        public:
            SwitchSeries(SwitchWindow & switchWindow);
            virtual ~SwitchSeries(void);
        protected:
            virtual void onChangeSelection(unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) override;
        };
    
        rascUI::BackPanel backPanel;
        rascUI::ScrollPane scrollPane;
        SwitchSeries series;
        rascUI::EmplaceVector<SwitchToggleButton> buttons;
        xcb_query_tree_reply_t * windowListReply;
        SwitchToggleButton * lastSelectedButton;

    public:
        SwitchWindow(CustomContext & context, rascUI::Theme * theme);
        virtual ~SwitchWindow(void);
        
    private:
        void resizeButtons(const size_t newSize);
        
    protected:
        virtual void xcbOnMapNotify(void) override;
        virtual void onMapWindow(void) override;
        virtual void onUnmapWindow(void) override;
        
    public:
        void switchForward(void);
        void switchBackward(void);
        void onAltRelease(void);
    };
}

#endif
