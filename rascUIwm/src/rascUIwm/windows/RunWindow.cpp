/*
 * RunWindow.cpp
 *
 *  Created on: 31 Aug 2024
 *      Author: wilson
 */

#include "RunWindow.h"

#include <rascUI/platform/WindowConfig.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <cstddef>

#include "../utils/ProcessManager.h"
#include "../CustomContext.h"

#define LAYOUT_THEME theme

#define RUN_OVERALL_WIDTH 256
#define RUN_OVERALL_HEIGHT (COUNT_ONEBORDER_Y(RUN_ROWS) + UI_BHEIGHT)

#define RUN_ROWS 2
#define RUN_ROW_LOC(row)               \
    BORDERTABLE_Y(row, RUN_ROWS,       \
     PIN_T(COUNT_INBORDER_Y(RUN_ROWS), \
      SUB_MARGIN_T(UI_BHEIGHT,         \
       SUB_BORDER_X(                   \
        GEN_FILL                       \
    ))))

#define RUN_LABEL_LOC \
    PIN_NORMAL_T(     \
     GEN_FILL         \
    )

#define RUN_BUTTON_WIDTH 64
#define CANCEL_BUTTON_LOC   \
    PIN_L(RUN_BUTTON_WIDTH, \
     RUN_ROW_LOC(1)         \
    )
#define RUN_BUTTON_LOC   \
    PIN_R(RUN_BUTTON_WIDTH, \
     RUN_ROW_LOC(1)         \
    )

namespace rascUIwm {

    namespace runWindowHelpers {
        static rascUI::WindowConfig getWindowConfig(CustomContext & context, rascUI::Theme * theme) {
            rascUI::WindowConfig config;
            config.screenWidth = RUN_OVERALL_WIDTH;
            config.screenHeight = RUN_OVERALL_HEIGHT;
            config.title = "rascUIwm run";
            return config;
        }
    }

    RunWindow::RunBox::RunBox(const rascUI::Location & location) :
        rascUI::BasicTextEntryBox(location) {
    }
    
    RunWindow::RunBox::~RunBox(void) {
    }
    
    rascUI::NavigationAction RunWindow::RunBox::onSelectionLockKeyRelease(rascUI::NavigationAction defaultAction, int key, bool special) {
        // Immediately run command when enter key is pressed.
        if (defaultAction == rascUI::NavigationAction::enter) {
            RunWindow & runWindow = (RunWindow &)*getTopLevel();
            runWindow.run();
        }
        return rascUI::BasicTextEntryBox::onSelectionLockKeyRelease(defaultAction, key, special);
    }

    RunWindow::RunWindow(CustomContext & context, rascUI::Theme * theme) :
        PopupWindow(context, runWindowHelpers::getWindowConfig(context, theme)),
        backPanel(),
        topLabel(rascUI::Location(RUN_LABEL_LOC), "Run using shell (Bash):"),
        box(rascUI::Location(RUN_ROW_LOC(0))),
        cancelButton(rascUI::Location(CANCEL_BUTTON_LOC), "Cancel",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) setMapped(false);
            }
        ),
        runButton(rascUI::Location(RUN_BUTTON_LOC), "Run",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) run();
            }
        ) {
        
        add(&backPanel);
        add(&topLabel);
        add(&box);
        add(&cancelButton);
        add(&runButton);
    }
    
    RunWindow::~RunWindow(void) {
    }
    
    void RunWindow::run(void) {
        CustomContext & context = (CustomContext &)*getContext();
        ProcessManager & processManager = context.getProcessManager();
        
        // No longer need the popup.
        setMapped(false);
        
        // Run user's input either "raw" or under a bash shell. For both, allow
        // killing the process when we exit.
        const std::string & text = box.getText();
        if (text.find(' ') == std::string::npos) {
            // No spaces in the string: Just run the string (presumably just
            // a filename of a program).
            const char * const args[] = { text.c_str(), NULL };
            processManager.start(text.c_str(), true, args);
        } else {
            // Spaces in the string: Make bash interpret it.
            const char * const args[] = { "bash", "-c", text.c_str(), NULL };
            processManager.start("bash", true, args);
        }
    }
    
    void RunWindow::xcbOnMapNotify(void) {
        PopupWindow::xcbOnMapNotify();
        
        CustomContext & context = (CustomContext &)*getContext();
        
        // Send to top of popup bucket and steal focus: Sneaky! Can't be in
        // onMapWindow - won't be mapped yet due to popup's map request).
        context.sendToTopOfBucket(xcbGetWindow(), Buckets::specialPopup);
        context.focusWindow(xcbGetWindow());
    }
    
    void RunWindow::onMapWindow(void) {
        PopupWindow::onMapWindow();
        
        // Clear box and lock selection to it when we open.
        box.setText("");
        afterEvent->addFunction([this](void) {
            setKeyboardSelected(&box);
            lockSelection();
        });
    }
}
