/*
 * RunWindow.h
 *
 *  Created on: 31 Aug 2024
 *      Author: wilson
 */

#ifndef RASCUIWM_WINDOWS_RUNWINDOW_H_
#define RASCUIWM_WINDOWS_RUNWINDOW_H_

#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>

#include "../utils/PopupWindow.h"

namespace rascUI {
    class Theme;
}

namespace rascUIwm {
    class CustomContext;

    /**
     * Popup to run shell commands.
     */
    class RunWindow : public PopupWindow {
    private:
        class RunBox : public rascUI::BasicTextEntryBox {
        public:
            RunBox(const rascUI::Location & location = rascUI::Location());
            virtual ~RunBox(void);
        protected:
            virtual rascUI::NavigationAction onSelectionLockKeyRelease(rascUI::NavigationAction defaultAction, int key, bool special) override;
        };
    
        rascUI::BackPanel backPanel;
        rascUI::Label topLabel;
        RunBox box;
        rascUI::Button cancelButton, runButton;

    public:
        RunWindow(CustomContext & context, rascUI::Theme * theme);
        virtual ~RunWindow(void);
        
    private:
        void run(void);
        
    protected:
        virtual void xcbOnMapNotify(void) override;
        virtual void onMapWindow(void) override;
    };
}

#endif
