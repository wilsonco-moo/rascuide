/*
 * CustomWindow.h
 *
 *  Created on: 14 Nov 2021
 *      Author: wilson
 */

#ifndef RASCUIWM_CUSTOMWINDOW_H_
#define RASCUIWM_CUSTOMWINDOW_H_

#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/abstract/TextObject.h>
#include <rascUI/components/abstract/ResponsiveComponent.h>
#include <rascUIxcb/platform/Window.h>
#include <rascUI/base/Component.h>
#include <xcb/xcb.h>

namespace rascUI {
    enum class WindowDecorationButtonType;
    enum class CursorType;
}

namespace rascUIwm {
    class CustomContext;
    enum class ResizeMode : uint8_t;
    enum class Buckets : unsigned char;

    /**
     * Helper used inside CustomContext, for managing a window (creating frame
     * window too).
     */
    class CustomWindow : public rascUIxcb::Window {
    private:
        // Child window stores the window created by the other application,
        // exists as a component under top level (as to be resized by the layout
        // system), and resizes the child window to match on recalculate.
        class ChildWindow : public rascUI::Component {
        public:
            xcb_window_t window;
            
            ChildWindow(rascUI::Theme * theme, const CustomContext & context, const xcb_window_t window);
            virtual ~ChildWindow(void);
            
        protected:
            virtual void onDraw(void) override;
            virtual bool shouldRespondToKeyboard(void) const override;
            virtual void recalculate(const rascUI::Rectangle & parentSize) override;
        
        public:
            // Called when client requests us to be mapped, so we can do reparenting.
            void onClientRequestMap(void) const;
        };
        
        // Custom back panel for window decoration background.
        class WindowDecorationBackPanel : public rascUI::BackPanel {
        private:
            class ResizeDirection { public: enum { none, right, topRight, top, topLeft, left, bottomLeft, bottom, bottomRight, COUNT }; };
            class ResizeData {
            public:
                rascUI::CursorType cursorType;
                ResizeMode resizeModeX, resizeModeY;
            };
            const static ResizeData resizeData[ResizeDirection::COUNT];
            unsigned int currentDirection;
        public:
            WindowDecorationBackPanel();
            virtual ~WindowDecorationBackPanel(void);
        private:
            unsigned int getResizeDirection(GLfloat viewX, GLfloat viewY) const;
            void updateResizeDirection(GLfloat viewX, GLfloat viewY);
        protected:
            virtual void onDraw(void) override;
            virtual void onMouseEnter(GLfloat viewX, GLfloat viewY) override;
            virtual void onMouseMove(GLfloat viewX, GLfloat viewY) override;
            virtual void onMousePress(GLfloat viewX, GLfloat viewY, int button) override;
            virtual void onMouseLeave(GLfloat viewX, GLfloat viewY) override;
        public:
            virtual rascUI::CursorType getCursorType(void) const override;
        };
        
        // Custom front panel and label for window decoration top.
        class WindowDecorationFrontPanelLabel : public rascUI::FrontPanel, public rascUI::TextObject {
        public:
            WindowDecorationFrontPanelLabel(const rascUI::Location & location);
            virtual ~WindowDecorationFrontPanelLabel(void);
        protected:
            virtual void onDraw(void) override;
            virtual void onMousePress(GLfloat viewX, GLfloat viewY, int button) override;
            virtual void onMouseEnter(GLfloat viewX, GLfloat viewY) override;
            virtual void onMouseLeave(GLfloat viewX, GLfloat viewY) override;
        public:
            virtual rascUI::CursorType getCursorType(void) const override;
        };
    
        // Custom button (close buttons etc).
        class WindowDecorationButton : public rascUI::ResponsiveComponent {
        private:
            rascUI::WindowDecorationButtonType type;
        public:
            WindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type);
            virtual ~WindowDecorationButton(void);
        protected:
            virtual void onDraw(void) override;
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };
    
        WindowDecorationBackPanel backPanel;
        WindowDecorationFrontPanelLabel frontPanel;
        WindowDecorationButton closeButton;
        ChildWindow child;
        
        // The workspace we're currently assigned to. Set to context's current
        // workspace when client requests us to be mapped.
        unsigned int currentWindowWorkspace;
        // Property cookie for requesting window title (WM_NAME). Stored between
        // property notify and map to avoid blocking.
        xcb_get_property_cookie_t wmNameCookie;
        // Property cookie for requesting WM_PROTOCOLS (so we know whether
        // window has WM_DELETE_WINDOW. Stored since we don't actually need to
        // know that until the close button is pressed!
        xcb_get_property_cookie_t wmProtocolsCookie;
        // Our current bucket: "Normal" by default.
        Buckets currentBucket;
        // Set if client originally tried to create the window with override
        // redirect set.
        bool isPopup;
        // Set if client has WM_DELETE_WINDOW, won't have a valid value until
        // receiveWmProtocols gets called!
        bool hasCloseBehaviour;
        // Whether window manager wants us shown, (true by default, false once
        // we're minimised).
        bool isWmShown;
        // Toggled true/false by map request and unmap notify. Keeps track of
        // whether client wants us to be mapped. False by default.
        bool clientWantsUsMapped;

    public:
        CustomWindow(CustomContext * context, const rascUI::WindowConfig & config, const xcb_create_notify_event_t & createEvent);
        virtual ~CustomWindow(void);
        
    private:
        void handleConfigureRequest(const xcb_configure_request_event_t & event);
        void handlePropertyNotify(const xcb_property_notify_event_t & event);
        
        void requestWindowTitle(void);
        void receiveWindowTitle(void);
        
        void requestWmProtocols(void);
        void receiveWmProtocols(void);
        
    protected:
        // Override to also map child window.
        // Note: No need to UNmap child window when told to, it's an unmap
        // NOTIFY not a REQUEST!
        virtual void onMapWindow(void) override;
    
    public:
        bool handleWindowEvent(xcb_generic_event_t * event, bool * repaintNeeded);
        
        /**
         * Gets the id of our associated child window, reparented within the
         * frame.
         */
        xcb_window_t getChildWindow(void) const;
        
        /**
         * Sends us to the top of the specified bucket (sets current bucket).
         */
        void sendToTopOfBucket(const Buckets bucket);
        
        /**
         * Sends us to the top of our current bucket.
         */
        void sendToTop(void) const;
        
        /**
         * Sets our current workspace, updateMapStatus is automatically called
         * if workspace actually changes.
         */
        void setWorkspace(const unsigned int newWorkspace);
        
        /**
         * Sets whether window manager wants us shown, updateMapStatus is
         * automatically called if wmShown status actually changes.
         * (I.e: true to show, false to minimise).
         */
        void setWmShown(const bool newIsWmShown);
        
        /**
         * Sets us to be mapped/unmapped depending on current status. We'll
         * be mapped only if:
         *  - Client wants us mapped.
         *  - Window manager wants us shown (wmShown), i.e: not minimised.
         *  - We're on context's current workspace.
         * Must be called when context switches its current workspace!
         */
        void updateMapStatus(void);
        
        /**
         * Returns true if we're eligible to be shown on the list of windows of
         * the current workspace: I.e: If client wants us to be mapped, and
         * we're on the current workspace. Returns true anyway if minimised
         * (so we might not actually be mapped).
         */
        bool isClientMappedOnCurrentWorkspace(void) const;
        
        /**
         * Gets our current window title.
         */
        const std::string & getTitle(void);
    };
}

#endif
