/*
 * main.cpp
 *
 *  Created on: 20 September 2021
 *      Author: wilson
 */

#include <rascUIxcb/util/Connection.h>
#include <iostream>
#include <cstring>
#include <cstddef>
#include <cstdlib>
#include <chrono>
#include <thread>

#include "rascUIwm/utils/ProcessManager.h"
#include "rascUIwm/CustomContext.h"

int main(int argc, char * argv[]) {
    
    rascUIwm::ProcessManager processManager;
    
    // Deal with auto-launch of X server.
    if (argc == 2 && std::strcmp(argv[1], "auto") == 0) {

        // Set DISPLAY to :0 if no display has been set (overwite false).
        setenv("DISPLAY", ":0", false);

        // Start X process, (must wait for it to terminate, we're can't ever
        // kill the X process or unpleasant things like graphics driver freezes
        // seem to happen).
        const char * const xArgs[] = { "X", NULL };
        const pid_t xProcess = processManager.start("X", false, xArgs);

        // Wait until X server has started or process has died.
        while(true) {
            // We can connect: Successful!
            rascUIxcb::Connection testConnection;
            if (testConnection.connectionSuccessful()) {
                break;
            }

            // Update process manager (reap) so we know if X process has died.
            // If it has, give up.
            processManager.update();
            if (!processManager.isAlive(xProcess)) {
                std::cerr << "Auto launch: X process died before we were able to successfully connect, giving up.\n";
                return EXIT_FAILURE;
            }

            // Wait a bit before polling again.
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

    // Given some other args: Show help message.
    } else if (argc != 1) {
        std::cerr << "Either run with no args, or specify \"auto\" as a single arg to start X server automatically.\n";
        return EXIT_FAILURE;
    }

    // Create (custom) rascuixcb context (loads default theme internally).
    rascUIwm::CustomContext context(processManager);
    if (!context.wasSuccessful()) {
        std::cout << "Failed to setup context.\n";
        return EXIT_FAILURE;
    }
    
    context.mainLoop();

    return EXIT_SUCCESS;
}

