#include <cstdlib>
#include <iostream>
#include <rascUIfiles/CustomContext.h>
#include <rascUIfiles/MainWindow.h>
#include <unistd.h>
#include <cstddef>

int main(void) {
    // Create context, give up on failure.
    rascUIfiles::CustomContext context;
    if (!context.wasSuccessful()) {
        std::cerr << "Failed to create context.\n";
        return EXIT_FAILURE;
    }
    
    // Create a single window for now, registered with context, give it the
    // working directory. If getcwd fails default to root, also make sure we
    // free the returned string.
    char * const workingDir = getcwd(NULL, 0);
    rascUIfiles::MainWindow mainWindow(context, workingDir == NULL ? "/" : workingDir);
    free(workingDir);
    
    // Map it, run main loop then exit. That's it.
    mainWindow.setMapped(true);
    context.mainLoop();
    return EXIT_SUCCESS;
}
