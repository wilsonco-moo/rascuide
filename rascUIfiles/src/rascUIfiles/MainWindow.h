/*
 * MainWindow.h
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_MAINWINDOW_H_
#define RASCUIFILES_MAINWINDOW_H_

#include <rascUI/platform/WindowConfig.h>
#include <rascUIxcb/platform/Window.h>

#include "directoryTab/DirectoryTab.h"
#include "sideBar/SideBar.h"

namespace rascUIfiles {
    class CustomContext;

    /**
     * Represents a window which can contain one or more directory tabs, and
     * a side bar.
     */
    class MainWindow : public rascUIxcb::Window {
    private:
        SideBar sideBar;
        // For now just one tab.
        DirectoryTab directoryTab;

    public:
        MainWindow(CustomContext & context, const char * const initialPath);
        virtual ~MainWindow(void);
        
    private:
        // Internally used to generate window config.
        static rascUI::WindowConfig getWindowConfig(CustomContext & context, const char * const initialPath);
    };
}

#endif
