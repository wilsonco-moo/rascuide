/*
 * MainWindow.cpp
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#include "MainWindow.h"

#include <cmath>

#include "CustomContext.h"

namespace rascUIfiles {

    MainWindow::MainWindow(CustomContext & context, const char * const initialPath) :
        rascUIxcb::Window(&context, getWindowConfig(context, initialPath)),
        sideBar(context),
        directoryTab(context, context.defaultTheme, initialPath) {

        add(&sideBar);
        add(&directoryTab);
    }
    
    MainWindow::~MainWindow(void) {
    }
    
    rascUI::WindowConfig MainWindow::getWindowConfig(CustomContext & context, const char * const initialPath) {
        rascUI::WindowConfig config;
        config.title = "rascUIfiles: ";
        config.title += initialPath;
        config.screenWidth = std::floor(640.0f * (*context.defaultXScalePtr));
        config.screenHeight = std::floor(480.0f * (*context.defaultYScalePtr));
        return config;
    }
}
