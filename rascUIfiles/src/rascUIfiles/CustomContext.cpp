/*
 * CustomContext.cpp
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#include "CustomContext.h"

namespace rascUIfiles {

    CustomContext::CustomContext(void) :
        rascUIxcb::Context(),
        processReaper() {
    }
    
    CustomContext::~CustomContext(void) {
    }
    
    rascUIfileProcess::ProcessReaper & CustomContext::getProcessReaper(void) {
        return processReaper;
    }
}
