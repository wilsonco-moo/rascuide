/*
 * EntryProcess.h
 *
 *  Created on: 21 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_DIRECTORYTAB_ENTRYPROCESS_H_
#define RASCUIFILES_DIRECTORYTAB_ENTRYPROCESS_H_

#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUIfileProcess/processes/ListDirectory.h>
#include <rascUI/util/EmplaceVector.h>
#include <rascUI/base/Container.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Label.h>

#include "FileEntry.h"

namespace rascUIfiles {
    class CustomContext;
    class DirectoryTab;

    /**
     * Custom version of rascUI-fileProcess's ListDirectory process, which does
     * extra sorting.
     */
    class EntryProcess : public rascUIfileProcess::ListDirectory {
    private:
        // Entry in the scroll pane showing a heading.
        class Heading : public rascUI::Container {
        private:
            rascUI::FrontPanel frontPanel;
            rascUI::Label label;
        public:
            Heading(rascUI::Theme * theme, const char * const text);
            virtual ~Heading(void);
        };
    
        CustomContext & context;
        DirectoryTab & directoryTab;
        rascUI::Container & container;
        void * beforeEventToken, * afterEventToken;
        rascUI::ToggleButtonSeries series;
        
        // Headings.
        Heading failureHeading, waitingHeading, directoryHeading, fileHeading;
    
        std::vector<const rascUIfileProcess::DirEnt *> directories, files;
        rascUI::EmplaceVector<FileEntry> directoryFileEntries, fileFileEntries;
        
        // Currently selected file entry (or none).
        FileEntry * selectedFileEntry;

        bool showHiddenFiles;

    public:
        EntryProcess(CustomContext & context, DirectoryTab & directoryTab, rascUI::Theme * theme, rascUI::Container & container);
        virtual ~EntryProcess(void);
        
    protected:
        virtual bool consume(const char * const buffer, size_t length) override;
        virtual void onDone(void) override;
    
    public:
        /**
         * Overridden so we can do extra setup (clean up file entries) first.
         * Must be called from function queue (not UI event!!).
         */
        virtual void start(void) override;
        
        /**
         * Provides access to directory and file entries.
         * Will be empty unless sucessful.
         */
        rascUI::EmplaceVector<FileEntry> & getDirectoryFileEntries(void);
        rascUI::EmplaceVector<FileEntry> & getFileFileEntries(void);
        
        /**
         * Gets currently selected file entry (or null).
         */
        FileEntry * getSelectedFileEntry(void) const;
    };
}

#endif
