/*
 * DirectoryTab.cpp
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#include "DirectoryTab.h"

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <unistd.h>
#include <cstring>

#include "../CustomContext.h"
#include "../utils/Misc.h"
#include "../sideBar/SideBar.h"
#include "FileEntry.h"

#define LAYOUT_THEME theme

#define DIRECTORY_TAB_LOC \
    SUB_MARGIN_L(SideBar::WIDTH, \
     GEN_FILL \
    )

#define TOP_BAR_ROWS 2
#define TOP_BAR_HEIGHT \
    COUNT_OUTBORDER_Y(TOP_BAR_ROWS) + (UI_BORDER * 2.0f)

#define TOP_BAR_LOC \
    PIN_T(TOP_BAR_HEIGHT, \
     GEN_FILL \
    )

#define TOP_BAR_INNER_LOC \
    SUB_BORDER(TOP_BAR_LOC)

#define TOP_BAR_ROW(index) \
    BORDERTABLE_Y(index, TOP_BAR_ROWS, \
     SUB_BORDER( \
      TOP_BAR_INNER_LOC \
    ))

DEFINE_TABLE(Row1Buttons, 6, 11, 8, 4, 9, 10, 8)
#define TOP_BAR_ROW1_BUTTON_LOC(index) \
    DEFINED_BORDERTABLE_X(Row1Buttons, index, \
     TOP_BAR_ROW(1) \
    )

#define BOTTOM_BAR_LOC \
    SUB_MARGIN_T(TOP_BAR_HEIGHT, \
     GEN_FILL \
    )

#define SCROLL_PANE_LOC \
    SUB_BORDER_R( \
     BOTTOM_BAR_LOC \
    )

namespace rascUIfiles {

    DirectoryTab::DirectoryTab(CustomContext & context, rascUI::Theme * theme, const char * const initialPath) :
        rascUI::Container(rascUI::Location(DIRECTORY_TAB_LOC)),
        context(context),
        afterEventToken(context.afterEvent.createToken()),
        bottomBarBackPanel(rascUI::Location(BOTTOM_BAR_LOC)),
        scrollPane(theme, rascUI::Location(SCROLL_PANE_LOC), true, true, false),
        topBarBackPanel(rascUI::Location(TOP_BAR_LOC)),
        topBarFrontPanel(rascUI::Location(TOP_BAR_INNER_LOC)),
        pathBox(rascUI::Location(TOP_BAR_ROW(0)), initialPath,
            [this](const std::string & str) {
                // When changed, if possible, set entry process's directory (so
                // change our "working" directory) and start a listing.
                if (hasListing) {
                    entryProcess.setDirectory(str.c_str());
                    startDirectoryListing();
                }
            }
        ),
        openButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(0)), "Open",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                // Open the currently selected file.
                if (button == getMb()) {
                    openFileEntry();
                }
            }
        ),
        openWithButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(1)), "Open with"),
        optionsButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(2)), "Option"),
        upButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(3)), "Up",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                // Just navigate to parent directory.
                if (button == getMb()) {
                    openParentDirectory();
                }
            }
        ),
        refreshButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(4)), "Refresh",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                // Just start a directory listing if possible.
                if (button == getMb()) {
                    startDirectoryListing();
                }
            }
        ),
        terminalButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(5)), "Terminal",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                // Launch a terminal in the current directory.
                if (button == getMb() && hasListing && entryProcess.wasSuccessful()) {
                    const char * const args[] = { "xterm", NULL };
                    Misc::startDetach("xterm", entryProcess.getDirectory(), args);
                }
            }
        ),
        cancelButton(rascUI::Location(TOP_BAR_ROW1_BUTTON_LOC(6)), "Cancel",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                // If waiting for a directory listing, all we have to do is tell
                // entry process to kill.
                if (button == getMb() && !hasListing) {
                    entryProcess.kill();
                }
            }
        ),
        hasListing(false),
        entryProcess(context, *this, theme, scrollPane.contents) {
        
        // No smooth scrolling.
        scrollPane.contents.smoothMoveSpeed = 0.0f;
        
        add(&bottomBarBackPanel);
        add(&scrollPane);
        
        add(&topBarBackPanel);
        add(&topBarFrontPanel);
        add(&pathBox);
        add(&openButton);
        add(&openWithButton);
        add(&optionsButton);
        add(&upButton);
        add(&refreshButton);
        add(&terminalButton);
        add(&cancelButton);
        
        // Initially set active state.
        setButtonActiveState();
        
        // Start directory listing.
        entryProcess.setDirectory(initialPath);
        entryProcess.start();
    }
    
    DirectoryTab::~DirectoryTab(void) {
        context.afterEvent.deleteToken(afterEventToken);
    }
    
    void DirectoryTab::setButtonActiveState(void) {
        const FileEntry * const selectedFileEntry = entryProcess.getSelectedFileEntry();
        
        pathBox.setActive(hasListing);
        openButton.setActive(hasListing && selectedFileEntry != NULL);
        openWithButton.setActive(hasListing && selectedFileEntry != NULL && !S_ISDIR(selectedFileEntry->getEntry().getDestinationStat().st_mode));
        optionsButton.setActive(hasListing);
        upButton.setActive(hasListing && std::strcmp(entryProcess.getDirectory(), "/") != 0);
        refreshButton.setActive(hasListing);
        terminalButton.setActive(hasListing && entryProcess.wasSuccessful());
        cancelButton.setActive(!hasListing);
    }
    
    void DirectoryTab::openFileEntry(void) {
        // Get selected file entry, give up if invalid.
        const FileEntry * const selectedFileEntry = entryProcess.getSelectedFileEntry();
        if (!hasListing || selectedFileEntry == NULL) {
            return;
        }
        
        if (S_ISDIR(selectedFileEntry->getEntry().getDestinationStat().st_mode)) {
            // If we're opening a directory or symlink to one:
            // Get current directory, append filename of selected entry, switch
            // to the new directory, start listing.
            const std::string newDirectory = Misc::pathAppend(entryProcess.getDirectory(), selectedFileEntry->getEntry().name.c_str());
            entryProcess.setDirectory(newDirectory.c_str());
            startDirectoryListing();
            
        } else {
            // If we're opening a normal file:
            // todo.
        }
    }
    
    void DirectoryTab::openParentDirectory(void) {
        // Give up if no file listing.
        if (!hasListing) {
            return;
        }
        
        // Switch to the new directory, start listing.
        const std::string newDirectory = Misc::pathGetParent(entryProcess.getDirectory());
        entryProcess.setDirectory(newDirectory.c_str());
        startDirectoryListing();
    }
    
    void DirectoryTab::startDirectoryListing(void) {
        if (hasListing) {
            // We no longer have a valid directory listing.
            hasListing = false;
            
            // From a function queue, tell entry process to start.
            getTopLevel()->afterEvent->addFunction(afterEventToken, [this](void) {
                entryProcess.start();
                
                // Also update button active state.
                setButtonActiveState();
                
                // Stuff's been added to the scroll pane, so make sure it (and its back panel) repaints.
                // Also make sure buttons get repainted (those will have changed).
                bottomBarBackPanel.repaint();
                scrollPane.repaint();
                topBarFrontPanel.repaint();
            });
        }
    }
    
    void DirectoryTab::onCompleteDirectoryListing(void) {
        if (!hasListing) {
            // We now have a valid directory listing.
            hasListing = true;
            
            // Can update button active state now.
            setButtonActiveState();
            
            // Set keyboard selected: First directory or file if possible.
            if (!entryProcess.getDirectoryFileEntries().empty()) {
                getTopLevel()->setKeyboardSelected(&entryProcess.getDirectoryFileEntries().front());
            } else if (!entryProcess.getFileFileEntries().empty()) {
                getTopLevel()->setKeyboardSelected(&entryProcess.getFileFileEntries().front());
            }
            
            // Set path box's text to match completed path.
            pathBox.setText(entryProcess.getDirectory());
            
            // Stuff's been added to the scroll pane, so make sure it (and its back panel) repaints.
            // Also make sure buttons get repainted (those will have changed).
            bottomBarBackPanel.repaint();
            scrollPane.repaint();
            topBarFrontPanel.repaint();
        }
    }
    
    void DirectoryTab::onChangeSelectedEntry(void) {
        // Just update button active state.
        setButtonActiveState();
        
        // If a file has been selected, keyboard-select the open button for
        // convenience.
        if (entryProcess.getSelectedFileEntry() != NULL) {
            getTopLevel()->setKeyboardSelected(&openButton);
        }
    }
}
