/*
 * FileEntry.cpp
 *
 *  Created on: 21 Jan 2025
 *      Author: wilson
 */

#include "FileEntry.h"

#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#define LAYOUT_THEME theme

#define SCROLL_ENTRY_INNER_LOC \
    SUB_BORDER_X( \
     SUB_BORDER_B( \
      GEN_FILL \
    ))

namespace rascUIfiles {

    FileEntry::FileEntry(rascUI::ToggleButtonSeries * series, const unsigned long toggleButtonId, rascUI::Theme * theme, const rascUIfileProcess::DirEnt & entry) :
        rascUI::ToggleButton(series, toggleButtonId, rascUI::Location(SCROLL_ENTRY_INNER_LOC), entry.name),
        entry(entry) {
    }
    
    FileEntry::~FileEntry(void) {
    }
    
    const rascUIfileProcess::DirEnt & FileEntry::getEntry(void) const {
        return entry;
    }
}
