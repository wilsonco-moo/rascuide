/*
 * DirectoryTab.h
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_DIRECTORYTAB_DIRECTORYTAB_H_
#define RASCUIFILES_DIRECTORYTAB_DIRECTORYTAB_H_

#include <rascUI/base/Container.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/toggleButton/ToggleButtonSeries.h>

#include "EntryProcess.h"

namespace rascUI {
    class Theme;
}

namespace rascUIfiles {
    class CustomContext;
    class FileEntry;
    
    /**
     * Contains all the UI for a single directory instance.
     */
    class DirectoryTab : public rascUI::Container {
    private:
        CustomContext & context;
        void * afterEventToken;
    
        rascUI::BackPanel bottomBarBackPanel;
        rascUI::ScrollPane scrollPane;

        rascUI::BackPanel topBarBackPanel;
        rascUI::FrontPanel topBarFrontPanel;
        rascUI::BasicTextEntryBox pathBox;
        rascUI::Button openButton, openWithButton, optionsButton, upButton, refreshButton, terminalButton, cancelButton;
        
        // Whether we have the result of a directory listing (have active files).
        bool hasListing;
        
        // Background process for generating file listings.
        EntryProcess entryProcess;
        
    public:
        DirectoryTab(CustomContext & context, rascUI::Theme * theme, const char * const initialPath);
        virtual ~DirectoryTab(void);
    
    private:
        // Updates active state for all buttons.
        void setButtonActiveState(void);
        // Opens the currently selected file entry (if any).
        void openFileEntry(void);
        // Navigates to the parent directory.
        void openParentDirectory(void);
        // Starts a directory listing, only if we're not already waiting for one.
        void startDirectoryListing(void);
        
    public:
        /**
         * Called by EntryProcess once directory listing is finished.
         * Must be called from function queue (not worker thread!!).
         */
        void onCompleteDirectoryListing(void);
        
        /**
         * Called by EntryProcess when toggle button selection changes.
         * Must be called from function queue (not UI event!!).
         */
        void onChangeSelectedEntry(void);
    };
}

#endif
