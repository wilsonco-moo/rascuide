/*
 * FileEntry.h
 *
 *  Created on: 21 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_DIRECTORYTAB_FILEENTRY_H_
#define RASCUIFILES_DIRECTORYTAB_FILEENTRY_H_

#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUIfileProcess/processes/ListDirectory.h>

namespace rascUI {
    class Theme;
}

namespace rascUIfiles {

    /**
     * Entry in directory tab's scroll pane representing a file or directory.
     */
    class FileEntry : public rascUI::ToggleButton {
    private:
        const rascUIfileProcess::DirEnt & entry;
    
    public:
        FileEntry(rascUI::ToggleButtonSeries * series, const unsigned long toggleButtonId, rascUI::Theme * theme, const rascUIfileProcess::DirEnt & entry);
        virtual ~FileEntry(void);
        
        /**
         * Provides access to directory entry data.
         */
        const rascUIfileProcess::DirEnt & getEntry(void) const;
    };
}

#endif
