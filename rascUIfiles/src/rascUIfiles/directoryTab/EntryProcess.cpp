/*
 * EntryProcess.cpp
 *
 *  Created on: 21 Jan 2025
 *      Author: wilson
 */

#include "EntryProcess.h"

#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <rascUI/base/Container.h>
#include <unistd.h>
#include <algorithm>
#include <cstddef>

#include "../CustomContext.h"
#include "DirectoryTab.h"

#define LAYOUT_THEME theme

#define SCROLL_ENTRY_INNER_LOC \
    SUB_BORDER_X( \
     SUB_BORDER_B( \
      GEN_FILL \
    ))

namespace rascUIfiles {

    EntryProcess::Heading::Heading(rascUI::Theme * theme, const char * const text) :
        rascUI::Container(rascUI::Location(SCROLL_ENTRY_INNER_LOC)),
        frontPanel(),
        label(rascUI::Location(), text) {
        add(&frontPanel);
        add(&label);
    }

    EntryProcess::Heading::~Heading(void) {
    }

    EntryProcess::EntryProcess(CustomContext & context, DirectoryTab & directoryTab, rascUI::Theme * theme, rascUI::Container & container) :
        rascUIfileProcess::ListDirectory(context.getProcessReaper()),
        context(context),
        directoryTab(directoryTab),
        container(container),
        beforeEventToken(context.beforeEvent.createToken()),
        afterEventToken(context.afterEvent.createToken()),
        series(true,
            [this](unsigned long toggleButtonId, rascUI::ToggleButton * toggleButton) {
                // When selected toggle button changes, update selected entry and notify
                // directory tab (from function queue).
                selectedFileEntry = (FileEntry *)toggleButton;
                this->context.afterEvent.addFunction(afterEventToken, [this](void) {
                    this->directoryTab.onChangeSelectedEntry();
                });
            }
        ),
        failureHeading(theme, "Failed to list directory contents."),
        waitingHeading(theme, "Listing directory contents..."),
        directoryHeading(theme, "Directories:"),
        fileHeading(theme, "Files:"),
        selectedFileEntry(NULL),
        showHiddenFiles(true) {
    }
    
    EntryProcess::~EntryProcess(void) {
        // Subclasses must always call kill!
        kill();
        
        // Cope with the rare case that we're being destroyed before our before
        // event functions have ran - delete tokens.
        context.beforeEvent.deleteToken(beforeEventToken);
        context.afterEvent.deleteToken(afterEventToken);
    }
    
    bool EntryProcess::consume(const char * const buffer, size_t length) {
        // Call parent method, give up if that failed.
        if (!rascUIfileProcess::ListDirectory::consume(buffer, length)) {
            return false;
        }
        
        // Now, parent should've provided us with a valid list of directory
        // entries. Iterate through them, categorising them as directories or
        // files (valid links to directories count as directories).
        directories.clear();
        files.clear();
        for (const rascUIfileProcess::DirEnt & entry : getEntries()) {
            // Never show . or ..
            if (entry.name == "." || entry.name == "..") {
                continue;
            }
            // If not showing hidden files, don't show entry if name starts with dot.
            if (!showHiddenFiles && !entry.name.empty() && entry.name[0] == '.') {
                continue;
            }
            
            // Add to either directories or files depending on which one it is.
            if (S_ISDIR(entry.getDestinationStat().st_mode)) {
                directories.push_back(&entry);
            } else {
                files.push_back(&entry);
            }
        }
        
        // Comparator for sorting entries alphabetically.
        struct AlphabeticalComp {
            bool operator()(const rascUIfileProcess::DirEnt * entry0, const rascUIfileProcess::DirEnt * entry1) {
                return entry0->name < entry1->name;
            }
        };
        
        // Sort directories and files alphabetically. Nice that we're doing this
        // on the worker thread!
        std::sort(directories.begin(), directories.end(), AlphabeticalComp());
        std::sort(files.begin(), files.end(), AlphabeticalComp());
        
        // Must give each unique IDs in series.
        unsigned long toggleButtonIdUpto = 0;
        
        // Build file entries for directories:
        directoryFileEntries.resize(directories.size());
        for (const rascUIfileProcess::DirEnt * entry : directories) {
            directoryFileEntries.emplace(&series, toggleButtonIdUpto++, context.defaultTheme, *entry);
        }
        
        // Build file entries for files:
        fileFileEntries.resize(files.size());
        for (const rascUIfileProcess::DirEnt * entry : files) {
            fileFileEntries.emplace(&series, toggleButtonIdUpto++, context.defaultTheme, *entry);
        }
        
        return true;
    }
    
    void EntryProcess::onDone(void) {
        // Hand over to UI thread so we can do stuff, use token for safety.
        context.beforeEvent.addFunction(beforeEventToken, [this](void) {
            
            // Clear any interim "progress" heading(s).
            container.clear();
            
            if (wasSuccessful()) {
                // Add directory file entries.
                container.add(&directoryHeading);
                for (FileEntry & fileEntry : directoryFileEntries) {
                    container.add(&fileEntry);
                }
                
                // Add file file entries.
                container.add(&fileHeading);
                for (FileEntry & fileEntry : fileFileEntries) {
                    container.add(&fileEntry);
                }
                
            } else {
                // Failed to list directory contents, show failure.
                container.add(&failureHeading);
            }
            
            // Regardless of success status, notify directory tab.
            directoryTab.onCompleteDirectoryListing();
        });
    }
    
    void EntryProcess::start(void) {
        // Make definitely sure nothing is running async.
        kill();
        
        // No file entry is selected any more, we're starting.
        selectedFileEntry = NULL;
        
        // Remove all file entries and headings from container, then destroy
        // them all.
        container.clear();
        directoryFileEntries.clear();
        fileFileEntries.clear();
        
        // Add heading to say we're waiting.
        container.add(&waitingHeading);
        
        // Start parent.
        rascUIfileProcess::ListDirectory::start();
    }
    
    rascUI::EmplaceVector<FileEntry> & EntryProcess::getDirectoryFileEntries(void) {
        return directoryFileEntries;
    }
    
    rascUI::EmplaceVector<FileEntry> & EntryProcess::getFileFileEntries(void) {
        return fileFileEntries;
    }
    
    FileEntry * EntryProcess::getSelectedFileEntry(void) const {
        return selectedFileEntry;
    }
}
