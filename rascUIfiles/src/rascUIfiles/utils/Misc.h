/*
 * Misc.h
 *
 *  Created on: 22 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_UTILS_MISC_H_
#define RASCUIFILES_UTILS_MISC_H_

#include <string>

namespace rascUIfiles {

    /**
     * Collection of utility methods.
     */
    class Misc {
    public:
        /**
         * Gets the parent of the specified path.
         */
        static std::string pathGetParent(const char * const initialPath);
        
        /**
         * Appends the specified filename to the path.
         */
        static std::string pathAppend(const char * const initialPath, const char * const filename);
        
        /**
         * Starts a process at a specified working directory (or NULL for current!),
         * detaches and completely disassociates from the parent (so it isn't
         * killed when we are).
         * Args is a null terminated array.
         */
        static void startDetach(const char * const path, const char * const workingDirectory, const char * const * const args);
        
        /**
         * Same as above, but always uses NULL (current) working directory.
         */
        static void startDetach(const char * const path, const char * const * args);
    };
}

#endif
