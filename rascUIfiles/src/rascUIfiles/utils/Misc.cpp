/*
 * Misc.cpp
 *
 *  Created on: 22 Jan 2025
 *      Author: wilson
 */

#include "Misc.h"

#include <iostream>
#include <unistd.h>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>

namespace rascUIfiles {

    std::string Misc::pathGetParent(const char * const initialPath) {
        // Remove all trailing slashes.
        std::string newDirectory = initialPath;
        while (!newDirectory.empty() && newDirectory.back() == '/') {
            newDirectory.pop_back();
        }
        
        // Remove last slash character and everything after it.
        const size_t lastSlash = newDirectory.rfind('/');
        if (lastSlash != std::string::npos) {
            newDirectory.resize(lastSlash);
        }
        
        // Remove all trailing slashes again.
        while (!newDirectory.empty() && newDirectory.back() == '/') {
            newDirectory.pop_back();
        }
        
        // If we ended up with nothing, we want root instead.
        if (newDirectory.empty()) {
            newDirectory.push_back('/');
        }
        
        return newDirectory;
    }
    
    std::string Misc::pathAppend(const char * const initialPath, const char * const filename) {
        // Remove all trailing slashes (to get rid of duplicates).
        // For root, this will result in empty path.
        std::string newDirectory = initialPath;
        while (!newDirectory.empty() && newDirectory.back() == '/') {
            newDirectory.pop_back();
        }
        
        // Add a separator slash, append filename. Doing it this way avoids
        // slashes getting duplicated.
        newDirectory += '/';
        newDirectory += filename;
        
        return newDirectory;
    }
    
    void Misc::startDetach(const char * const path, const char * const workingDirectory, const char * const * const args) {
        // Fork the process.
        const pid_t pid = fork();
        if (pid == -1) {
            // Fork can fail, log it out.
            std::cerr << "Failed to start process \"" << path << "\", fork failed.\n";
        
        } else if (pid == 0) {
            // Zero: This is the child process.
            // Fork again, since we want to execute the process as a grandchild.
            const pid_t pid2 = fork();
            if (pid2 == 0) {
                // Zero: This is the grandchild process. Set working directory if one
                // was specified.
                if (workingDirectory != NULL) {
                    chdir(workingDirectory);
                }
                
                // Use execvp to replace it with the process we intend to run. Since
                // we're a grandchild (and child exits immediately), we're orphaned and
                // detached from the parent. Need a const cast because for some reason
                // exec wants non-const strings.
                execvp(path, const_cast<char * const *>(args));
                
                // Execv will only return if it fails, if so, just complain and end
                // the child process.
                const int execError = errno;
                std::cerr << "Cannot execute \"" << path << "\", ";
                for (const char * const * argsUpto = args; *argsUpto != NULL; argsUpto++) {
                    std::cerr << '"' << *argsUpto << "\", ";
                }
                std::cerr << "error: \"" << std::strerror(execError) << "\".\n";
                std::exit(EXIT_FAILURE);
                
            } else {
                // Non-zero: This is still the child process.
                // Again, log out failure if relevant.
                if (pid2 == -1) {
                    std::cerr << "Failed to start process \"" << path << "\", grandchild fork failed.\n";
                }
                
                // If sucessful, we've now spawned the grandchild process. Exit
                // immediately, since parent is waiting for us.
                // Doing this orphans the grandchild process, which detaches it
                // from the parent.
                std::exit(EXIT_SUCCESS);
            }
            
        } else {
            // Non-zero: This is the parent process. Wait for the child process
            // to complete, to clean up and avoid it becoming a zombie.
            // Repeat the call if it failed due to EINTR.
            pid_t result;
            do {
                result = waitpid(pid, NULL, 0);
            } while(result == -1 && errno == EINTR);
        }
    }
    
    void Misc::startDetach(const char * const path, const char * const * args) {
        startDetach(path, NULL, args);
    }
}
