/*
 * SideBar.cpp
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#include "SideBar.h"

#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>

#include "../CustomContext.h"

#define LAYOUT_THEME (context.defaultTheme)

#define SIDE_BAR_LOC \
    PIN_L(WIDTH, \
     GEN_FILL \
    )

namespace rascUIfiles {

    const float SideBar::WIDTH = 200;

    SideBar::SideBar(CustomContext & context) :
        rascUI::Container(rascUI::Location(SIDE_BAR_LOC)),
        backPanel(),
        scrollPane(context.defaultTheme) {
        
        // No smooth scrolling.
        scrollPane.contents.smoothMoveSpeed = 0.0f;
            
        add(&backPanel);
        add(&scrollPane);
    }
    
    SideBar::~SideBar(void) {
    }
}
