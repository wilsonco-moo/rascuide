/*
 * SideBar.h
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_SIDEBAR_SIDEBAR_H_
#define RASCUIFILES_SIDEBAR_SIDEBAR_H_

#include <rascUI/base/Container.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/generic/BackPanel.h>

namespace rascUIfiles {
    class CustomContext;

    /**
     * One per main window, shows common places.
     */
    class SideBar : public rascUI::Container {
    public:
        const static float WIDTH;
        
    private:
        rascUI::BackPanel backPanel;
        rascUI::ScrollPane scrollPane;

    public:
        SideBar(CustomContext & context);
        virtual ~SideBar(void);
    };
}

#endif
