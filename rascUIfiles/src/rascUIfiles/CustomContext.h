/*
 * CustomContext.h
 *
 *  Created on: 20 Jan 2025
 *      Author: wilson
 */

#ifndef RASCUIFILES_CUSTOMCONTEXT_H_
#define RASCUIFILES_CUSTOMCONTEXT_H_

#include <rascUIfileProcess/utils/ProcessReaper.h>
#include <rascUIxcb/platform/Context.h>

namespace rascUIfiles {

    /**
     * Context shared between windows: Provides any required config.
     */
    class CustomContext : public rascUIxcb::Context {
    private:
        rascUIfileProcess::ProcessReaper processReaper;
    
    public:
        CustomContext(void);
        virtual ~CustomContext(void);
        
        /**
         * Provides access to process reaper, separate to directory tabs so all
         * can share it.
         */
        rascUIfileProcess::ProcessReaper & getProcessReaper(void);
    };
}

#endif
